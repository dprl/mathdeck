
/* eslint-disable no-restricted-globals */
self.addEventListener('install', () => {
  console.log('Installed service worker');
  return self.skipWaiting();
});

self.addEventListener('activate', () => {
  // eslint-disable-next-line no-undef
  console.log('Ready to handle fetch');
  return self.clients.claim();
});


self.addEventListener('fetch', (event) => {
  const found = event.request.url.match(/\/tmpImages\//);
  if (found) {
    event.respondWith(new Promise((resolve) => {
      const requestUrl = event.request.url;
      const queryParam = requestUrl.slice(requestUrl.indexOf('?') + 1, requestUrl.length);
      const rawBlob = queryParam.split('=')[1];
      const domain = event.target.location.origin;
      const blobUrl = `blob:${domain}/${rawBlob}`;
      // console.log(`Request Data: ${event.request.url}`);
      // console.log(`Response: ${blobUrl}`);
      resolve(fetch(blobUrl));
    }));
  }
});
