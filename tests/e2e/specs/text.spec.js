describe('TextInput', () => {
  const textInputButtonSelector = '#text-input-activator';
  const textInputSelector = '.v-dialog';
  const speedDialSelector = '#speed-dial';
  // const logoSelector = '#mathseer-logo' // Arbitrary place to click outside the input dialog

  before((browser, done) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .windowSize('current', 1000, 1000, () => done());
  });

  after((browser, done) => {
    if (browser.sessionId) {
      browser.end(() => done());
    } else {
      done();
    }
  });

  beforeEach((browser, done) => {
    browser.refresh(() => done());
  });

  afterEach((_browser, done) => {
    done();
  });

  const clickSpeedDialButton = browser => browser.click(speedDialSelector);

  const clickTextInputButton = browser => browser.click(textInputButtonSelector);

  const textInputIsOpen = browser => browser.assert.cssClassPresent(textInputSelector, 'v-dialog--active');

  xit('is present onload', (browser) => {
    clickSpeedDialButton(browser);
    browser.expect.element('#script-editor').to.be.present.before(1000);
  });

  xit('opens up', (browser) => {
    clickSpeedDialButton(browser);
    clickTextInputButton(browser);
    textInputIsOpen(browser);
  });
});
