xdescribe('Menu', () => {
  const menuSelector = '#menu';

  before((browser, done) => {
    browser.url(process.env.VUE_DEV_SERVER_URL, () => done());
  });

  after((browser, done) => {
    if (browser.sessionId) {
      browser.end(() => done());
    } else {
      done();
    }
  });

  beforeEach((browser, done) => {
    browser.refresh(() => done());
  });

  afterEach((_browser, done) => {
    done();
  });

  // Define assertion for menu is open
  const menuIsOpen = browser => browser
    .assert.cssClassPresent(menuSelector, 'v-navigation-drawer--open');

  // Define assertion for menu is closed
  const menuIsClosed = browser => browser
    .assert.cssClassPresent(menuSelector, 'v-navigation-drawer--close');

  it('is present onload', (browser) => {
    browser.expect.element(menuSelector).to.be.present.before(1000);
  });

  it('is a navigation drawer', (browser) => {
    browser.assert.cssClassPresent(menuSelector, 'v-navigation-drawer');
  });

  it('is closed onload', menuIsClosed);

  describe('Menu Button', () => {
    const menuButtonSelector = '#menu-btn';

    it('is present onload', (browser) => {
      browser.expect.element(menuButtonSelector).to.be.present.before(1000);
    });

    // Define menu button click
    const clickMenuButton = browser => browser.click(menuButtonSelector);

    it('opens the menu on click when closed', (b) => {
      menuIsClosed(b);
      clickMenuButton(b);
      menuIsOpen(b);
    });

    it('closes the menu on click when open', (b) => {
      menuIsClosed(b);
      clickMenuButton(b);
      menuIsOpen(b);
      clickMenuButton(b);
      menuIsClosed(b);
    });

    it('can open and close multiple times', (b) => {
      menuIsClosed(b);
      clickMenuButton(b); // open
      clickMenuButton(b);
      clickMenuButton(b); // open
      menuIsOpen(b);
      clickMenuButton(b);
      menuIsClosed(b);
    });
  });
});
