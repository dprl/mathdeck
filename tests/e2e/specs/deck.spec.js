describe('MathDeck', () => {
  const deckSelector = '.v-dialog';
  const deckButtonSelector = '#math-deck-button';
  const logoSelector = '#mathseer-logo'; // Arbitrary place to click outside the deck

  before((browser, done) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .windowSize('current', 1000, 1000, () => done());
  });

  after((browser, done) => {
    if (browser.sessionId) {
      browser.end(() => done());
    } else {
      done();
    }
  });

  beforeEach((browser, done) => {
    browser.refresh(() => done());
  });

  afterEach((_browser, done) => {
    done();
  });

  // Define assertion for deck is open
  const deckIsOpen = browser => browser
    .assert.cssClassPresent(deckSelector, 'v-dialog--active');

  // Define click event
  const clickMathDeckButton = broswer => broswer.click(deckButtonSelector);

  it('is present onload', (browser) => {
    browser.expect.element(deckSelector).to.be.present.before(1000);
  });

  it('opens up', (browser) => {
    clickMathDeckButton(browser);
    deckIsOpen(browser);
  });

  it('can close', (browser) => {
    browser.click(logoSelector);
    browser.assert.cssClassNotPresent(deckSelector, 'v-dialog--active');
  });

  it('is a dialog box', (browser) => {
    browser.assert.cssClassPresent(deckSelector, 'v-dialog');
  });

  it('contains three tabs', (browser) => {
    clickMathDeckButton(browser);
    browser.assert.elementCount('.v-tabs__item', 3);
  });

  xit('[needs update] can toggle between tabs', (browser) => {
    clickMathDeckButton(browser);
    browser.expect.element('.recent-chips-window').to.have.css('display').which.equals('none');
    browser.expect.element('.favorite-chips-window').to.have.css('display').which.equals('block');

    browser.click('#recent-chips-button');
    browser.expect.element('.recent-chips-window').to.have.css('display').which.equals('block');
    browser.expect.element('.favorite-chips-window').to.have.css('display').which.equals('none');

    browser.click('#favorite-chips-button');
    browser.expect.element('.recent-chips-window').to.have.css('display').which.equals('none');
    browser.expect.element('.favorite-chips-window').to.have.css('display').which.equals('block');
  });
});
