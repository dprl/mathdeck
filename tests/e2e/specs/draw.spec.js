const rawStrokes42 = require('../data/raw_strokes_42.json');
const rawStrokesx2y2z2 = require('../data/raw_strokes_x2y2z2.json');

describe('script editor', () => {
  const selector = '#script-editor';
  const pathSelector = '#svg-strokes>path';

  before((browser, done) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .windowSize('current', 1000, 1000, () => done());
  });

  after((browser, done) => {
    if (browser.sessionId) {
      browser.end(() => done());
    } else {
      done();
    }
  });

  beforeEach((browser, done) => {
    browser.refresh(() => done());
  });

  afterEach((_browser, done) => {
    done();
  });

  it('is present onload', browser => browser.expect.element(selector).to.be.present.before(1000));

  it('can draw', (browser) => {
    browser.waitForElementVisible(selector)
      .moveToElement(selector, 10, 10)
      .mouseButtonDown()
      .moveToElement(selector, 100, 100)
      .moveToElement(selector, 200, 200)
      .moveToElement(selector, 400, 200)
      .moveToElement(selector, 400, 400)
      .mouseButtonUp();
  });

  // eslint-disable-next-line no-shadow
  function getDrawStroke(browser, selector) {
    return (stroke) => {
      const lengthX = stroke.x ? stroke.x.length : 0;
      const lengthY = stroke.y ? stroke.y.length : 0;
      const length = Math.min(lengthX, lengthY);
      if (length) {
        const x = i => stroke.x[i];
        const y = i => stroke.y[i];
        // move to first point and set button down
        browser
          .moveToElement(selector, x(0), y(0))
          .mouseButtonDown();
        // move along stroke path
        // eslint-disable-next-line no-plusplus
        for (let i = 1; i < length; i++) {
          browser.moveToElement(selector, x(i), y(i));
        }
        // lift button up
        browser.mouseButtonUp();
      }
    };
  }

  // eslint-disable-next-line no-shadow
  function getDrawStrokes(browser, selector) {
    const drawStroke = getDrawStroke(browser, selector);
    return (strokes) => {
      strokes.forEach(drawStroke);
    };
  }

  function drawStrokesFromJSON(browser, strokes) {
    browser.waitForElementVisible(selector);

    const drawStrokes = getDrawStrokes(browser, selector);
    drawStrokes(strokes);
  }

  it('can draw 42', (browser) => {
    browser.assert.elementCount(pathSelector, 0);
    drawStrokesFromJSON(browser, rawStrokes42);
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length);
  });

  it('can draw x^2 + y^2 = z^2', (browser) => {
    browser.assert.elementCount(pathSelector, 0);
    drawStrokesFromJSON(browser, rawStrokesx2y2z2);
    browser.assert
      .elementCount(pathSelector, rawStrokesx2y2z2.length);
  });

  it('can undo and redo a stroke', (browser) => {
    browser.assert.elementCount(pathSelector, 0);
    drawStrokesFromJSON(browser, rawStrokes42);
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length);

    // UNDO
    browser.click('#editor-undo');
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length - 1);

    // REDO
    browser.click('#editor-redo');
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length);
  });

  it('can clear after drawing', (browser) => {
    browser.assert.elementCount(pathSelector, 0);
    drawStrokesFromJSON(browser, rawStrokes42);
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length);

    // CLEAR
    browser.click('#editor-clear');
    browser.assert.elementCount(pathSelector, 0);

    // UNDO CLEAR
    browser.click('#editor-undo');
    browser.assert
      .elementCount(pathSelector, rawStrokes42.length);
  });
});
