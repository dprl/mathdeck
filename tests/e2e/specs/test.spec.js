// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

describe('Basic page properties', () => {
  before((browser, done) => {
    browser.url(process.env.VUE_DEV_SERVER_URL, () => done());
  });

  after((browser, done) => {
    if (browser.sessionId) {
      browser.end(() => done());
    } else {
      done();
    }
  });

  beforeEach((browser, done) => {
    browser.refresh(() => done());
  });

  afterEach((_browser, done) => {
    done();
  });

  it('has the mathseer logo', (browser) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .expect.element('#mathseer-logo').to.be.present.before(1000);
  });

  it('has MathSeer in the footer', (browser) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .expect.element('.v-footer').text.to.include('MathSeer').before(1000);
  });

  it('has at least 3 chips', (browser) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .assert.elementCountAtLeast('.chip', 3);
  });

  it('has at most 30 chips', (browser) => {
    browser.url(process.env.VUE_DEV_SERVER_URL)
      .assert.elementCountAtMost('.chip', 30);
  });
});
