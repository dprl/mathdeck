import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Footer from '@/components/Footer.vue';

describe('Footer.vue', () => {
  it('renders MathSeer (prototype)', () => {
    const msg = 'MathSeer (prototype)';
    const wrapper = shallowMount(Footer);
    expect(wrapper.text()).to.include(msg);
  });
});
