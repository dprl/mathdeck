export function saveImage(imageBlob, filename = 'chip.jpg', type = 'image/jpeg') {
  const a = document.createElement('a');
  const file = new File([imageBlob], filename, { type });
  a.href = URL.createObjectURL(file);
  a.download = filename;
  a.click();
}

export default saveImage;
