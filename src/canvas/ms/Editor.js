/* eslint-disable no-underscore-dangle */
import * as PointerEventGrabber from './grabber/PointerEventGrabber'
import * as CanvasRenderer from './renderer/canvas/CanvasRenderer'
import * as QuadraticCanvasStroker from './renderer/canvas/stroker/QuadraticCanvasStroker'
import * as DefaultConfiguration from './configuration/DefaultConfiguration'
import * as InkModel from './model/InkModel'

/**
 * Editor
 */
export class Editor {
  /**
   * @param {Element} element DOM element to attach this editor
   * @param {Configuration} [configuration] Configuration to apply
   */
  constructor (element, configuration) {
    /**
     * Inner reference to the DOM Element
     * @type {Element}
     */
    this.domElement = element
    this.domElement.classList.add('ms-editor')

    this.configuration = configuration
    this.renderer = CanvasRenderer
    /**
     * Current model
     * @type {Model}
     */
    this.model = InkModel.createModel(this.configuration)

    this.grabber = PointerEventGrabber
    this.stroker = QuadraticCanvasStroker
    /**
     * Current grabber context
     * @type {GrabberContext}
     */
    this.grabberContext = this.grabber.attach(this.domElement, this)

    this.domElement.editor = this
  }

  /**
   * Set the recognition parameters
   * WARNING : Need to fire a clear if user have already input some strokes.
   * @param {Configuration} configuration
   */
  set configuration (configuration) {
    /**
     * @private
     * @type {Configuration}
     */
    this.innerConfiguration = DefaultConfiguration.overrideDefaultConfiguration(configuration)
  }

  /**
   * Get the current recognition parameters
   * @return {Configuration}
   */
  get configuration () {
    return this.innerConfiguration
  }

  /**
   * Set the current renderer
   * @private
   * @param {Renderer} renderer
   */
  set renderer (renderer) {
    if (renderer) {
      if (this.innerRenderer) {
        this.innerRenderer.detach(this.domElement, this.rendererContext)
      }

      /**
       * @private
       * @type {Renderer}
       */
      this.innerRenderer = renderer
      if (this.innerRenderer) {
        /**
         * Current rendering context
         * @type {Object}
         */
        this.rendererContext = this.innerRenderer.attach(this.domElement, this.configuration.renderingParams.minHeight, this.configuration.renderingParams.minWidth)
      }
    }
  }

  /**
   * Get current renderer
   * @return {Renderer}
   */
  get renderer () {
    return this.innerRenderer
  }

  /**
   * Handle a pointer down
   * @param {{x: Number, y: Number, t: Number}} point Captured point coordinates
   * @param {String} [pointerType=mouse] Current pointer type
   * @param {String} [pointerId] Current pointer id
   */
  pointerDown (point, pointerType = 'pen', pointerId) {
    // logger.trace('Pointer down', point)
    this.model = InkModel.initPendingStroke(this.model, point, Object.assign({ pointerType, pointerId }, this.configuration.ink))
    this.renderer.drawCurrentStroke(this.rendererContext, this.model, this.stroker)
    // Currently no recognition on pointer down
  }

  /**
   * Handle a pointer move
   * @param {{x: Number, y: Number, t: Number}} point Captured point coordinates
   */
  pointerMove (point) {
    // logger.trace('Pointer move', point)
    this.model = InkModel.appendToPendingStroke(this.model, point)
    this.renderer.drawCurrentStroke(this.rendererContext, this.model, this.stroker)
    // Currently no recognition on pointer move
  }

  /**
   * Handle a pointer up
   * @param {{x: Number, y: Number, t: Number}} point Captured point coordinates
   */
  pointerUp (point) {
    // logger.trace('Pointer up', point)
    this.model = InkModel.endPendingStroke(this.model, point)
    this.renderer.drawModel(this.rendererContext, this.model, this.stroker)
  }

  /**
   * Function to call when the dom element link to the current ink paper has been resize.
   */
  resize () {
    // logger.debug('Resizing editor')
    this.renderer.resize(this.rendererContext, this.model, this.stroker, this.configuration.renderingParams.minHeight, this.configuration.renderingParams.minWidth)
  }
}
