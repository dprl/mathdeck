import { merge } from 'lodash'

/**
 * Default configuration
 * @type {Configuration}
 */
const defaultConfiguration = {
  listenerOptions: {
    capture: false,
    passive: true
  },
  xyFloatPrecision: 0,
  timestampFloatPrecision: 0,
  renderingParams: {
    minHeight: 100,
    minWidth: 100
  },
  ink: {
    color: '#000000',
    'pen-width': 1
  }
}

/**
 * Generate parameters
 * @param {Configuration} configuration Configuration to be used
 * @return {Configuration} Overridden configuration
 */
export function overrideDefaultConfiguration (configuration) {
  return merge({}, defaultConfiguration,
    configuration === undefined ? {} : configuration)
}

export default defaultConfiguration
