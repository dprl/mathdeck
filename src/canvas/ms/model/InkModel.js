import * as StrokeComponent from './StrokeComponent'
import { getSymbolsBounds } from './Symbol'

/**
 * Recognition positions
 * @typedef {Object} RecognitionPositions
 * @property {Number} [lastSentPosition=-1] Index of the last sent stroke.
 * @property {Number} [lastReceivedPosition=-1] Index of the last received stroke.
 * @property {Number} [lastRenderedPosition=-1] Last rendered recognized symbol position
 */

/**
 * Raw results
 * @typedef {Object} RawResults
 * @property {Object} exports=undefined The exports output as return by the recognition service.
 */

/**
 * Editor model
 * @typedef {Object} Model
 * @property {Stroke} currentStroke=undefined Stroke in building process.
 * @property {Array<Stroke>} rawStrokes=[] List of captured strokes.
 * @property {Array} strokeGroups=[] Group of strokes with same pen style.
 * @property {Array<Object>} defaultSymbols=[] Default symbols, relative to the current recognition type.
 * @property {Object} exports=undefined Result of the export (e.g. mathml, latex, text...).
 * @property {RawResults} rawResults The recognition output as return by the recognition service.
 * @property {Number} creationTime Date of creation timestamp.
 * @property {Number} modificationTime=undefined Date of lastModification.
 */

/**
 * Bounding box
 * @typedef {Object} Bounds
 * @property {Number} minX Minimal x coordinate
 * @property {Number} maxX Maximal x coordinate
 * @property {Number} minY Minimal y coordinate
 * @property {Number} maxY Maximal y coordinate
 */

/**
 * Create a new model
 * @return {Model} New model
 */
export function createModel () {
  // see @typedef documentation on top
  return {
    currentStroke: undefined,
    rawStrokes: [],
    strokeGroups: [],
    defaultSymbols: [],
    exports: undefined,
    rawResults: {
      exports: undefined
    },
    creationTime: new Date().getTime(),
    modificationTime: undefined
  }
}

/**
 * Clear the model.
 * @param {Model} model Current model
 * @return {Model} Cleared model
 */
export function clearModel (model) {
  const modelReference = model
  modelReference.currentStroke = undefined
  modelReference.rawStrokes = []
  modelReference.strokeGroups = []
  modelReference.exports = undefined
  modelReference.rawResults.exports = undefined
  return modelReference
}

/**
 * Mutate the model given in parameter by adding the new strokeToAdd.
 * @param {Model} model Current model
 * @param {Stroke} stroke Stroke to be added to pending ones
 * @return {Model} Updated model
 */
export function addStroke (model, stroke) {
  // We use a reference to the model. The purpose here is to update the pending stroke only.
  const modelReference = model
  // logger.debug('addStroke', stroke)
  modelReference.rawStrokes.push(stroke)
  return modelReference
}

/**
 * Mutate the model given in parameter by adding the new strokeToAdd and the penstyle. Used for iink REST.
 * @param {Model} model Current model
 * @param {Stroke} stroke Stroke to be added to pending ones
 * @return {Model} Updated model
 */
export function addStrokeToGroup (model, stroke) {
  // We use a reference to the model. The purpose here is to update the pending stroke only.
  const modelReference = model
  // logger.debug('addStroke', stroke)
  const lastGroup = modelReference.strokeGroups.length - 1
  if (modelReference.strokeGroups[lastGroup]) {
    modelReference.strokeGroups[lastGroup].strokes.push(stroke)
  } else {
    const newStrokeGroup = {
      strokes: []
    }
    const strokeCopy = {}
    Object.assign(strokeCopy, stroke)
    newStrokeGroup.strokes.push(strokeCopy)
    modelReference.strokeGroups.push(newStrokeGroup)
  }
  return modelReference
}

/**
 * Get the strokes that needs to be recognized
 * @param {Model} model Current model
 * @param {Number} [position=lastReceived] Index from where to extract strokes
 * @return {Array<Stroke>} Pending strokes
 */
export function extractPendingStrokes (model, position = 0) {
  return model.rawStrokes.slice(position)
}

/**
 * Mutate the model by adding a point and close the current stroke.
 * @param {Model} model Current model
 * @param {{x: Number, y: Number, t: Number}} point Captured point to create current stroke
 * @param {Object} properties Properties to be applied to the current stroke
 * @param {Number} [dpi=96] The screen dpi resolution
 * @return {Model} Updated model
 */
export function initPendingStroke (model, point, properties, dpi = 96) {
  if (properties && properties['pen-width']) {
    const pxWidth = (properties['pen-width'] * dpi) / 25.4
    Object.assign(properties, { width: pxWidth / 2 }) // FIXME hack to get better render
  }
  const modelReference = model
  // logger.trace('initPendingStroke', point)
  // Setting the current stroke to an empty one
  modelReference.currentStroke = StrokeComponent.createStrokeComponent(properties)
  modelReference.currentStroke = StrokeComponent.addPoint(modelReference.currentStroke, point)
  return modelReference
}

/**
 * Mutate the model by adding a point to the current pending stroke.
 * @param {Model} model Current model
 * @param {{x: Number, y: Number, t: Number}} point Captured point to be append to the current stroke
 * @return {Model} Updated model
 */
export function appendToPendingStroke (model, point) {
  const modelReference = model
  if (modelReference.currentStroke) {
    // logger.trace('appendToPendingStroke', point)
    modelReference.currentStroke = StrokeComponent.addPoint(modelReference.currentStroke, point)
  }
  return modelReference
}

/**
 * Mutate the model by adding the new point on a initPendingStroke.
 * @param {Model} model Current model
 * @param {{x: Number, y: Number, t: Number}} point Captured point to be append to the current stroke
 * @return {Model} Updated model
 */
export function endPendingStroke (model, point) {
  const modelReference = model
  if (modelReference.currentStroke) {
    // logger.trace('endPendingStroke', point)
    const currentStroke = StrokeComponent.addPoint(modelReference.currentStroke, point)
    // Mutating pending strokes
    addStroke(modelReference, currentStroke)
    addStrokeToGroup(modelReference, currentStroke)
    // Resetting the current stroke to an undefined one
    delete modelReference.currentStroke
  }
  return modelReference
}

/**
 * Get the bounds of the current model.
 * @param {Model} model Current model
 * @return {Bounds} Bounding box enclosing the current drawn model
 */
export function getBorderCoordinates (model) {
  let modelBounds = { minX: Number.MAX_VALUE, maxX: Number.MIN_VALUE, minY: Number.MAX_VALUE, maxY: Number.MIN_VALUE }

  // Default symbols
  if (model.defaultSymbols && model.defaultSymbols.length > 0) {
    modelBounds = getSymbolsBounds(model.defaultSymbols, modelBounds)
  }
  modelBounds = getSymbolsBounds(model.rawStrokes, modelBounds)
  return modelBounds
}

/**
 * Extract strokes from an ink range
 * @param {Model} model Current model
 * @param {Number} firstStroke First stroke index to extract
 * @param {Number} lastStroke Last stroke index to extract
 * @param {Number} firstPoint First point index to extract
 * @param {Number} lastPoint Last point index to extract
 * @return {Array<Stroke>} The extracted strokes
 */
export function extractStrokesFromInkRange (model, firstStroke, lastStroke, firstPoint, lastPoint) {
  return model.rawStrokes.slice(firstStroke, lastStroke + 1).map((stroke, index, slicedStrokes) => {
    if (slicedStrokes.length < 2) {
      return StrokeComponent.slice(stroke, firstPoint, lastPoint + 1)
    }
    if (index === 0) {
      return StrokeComponent.slice(stroke, firstPoint)
    }
    if (index === (slicedStrokes.length - 1)) {
      return StrokeComponent.slice(stroke, 0, lastPoint + 1)
    }
    return stroke
  })
}

/**
 * Clone model
 * @param {Model} model Current model
 * @return {Model} Clone of the current model
 */
export function cloneModel (model) {
  const clonedModel = Object.assign({}, model)
  // We clone the properties that need to be. Take care of arrays.
  clonedModel.defaultSymbols = [...model.defaultSymbols]
  clonedModel.currentStroke = model.currentStroke ? Object.assign({}, model.currentStroke) : undefined
  clonedModel.rawStrokes = [...model.rawStrokes]
  clonedModel.strokeGroups = JSON.parse(JSON.stringify(model.strokeGroups))
  clonedModel.exports = model.exports ? Object.assign({}, model.exports) : undefined
  clonedModel.rawResults = Object.assign({}, model.rawResults)
  return clonedModel
}

/**
 * Merge models
 * @param {...Model} models Models to merge (ordered)
 * @return {Model} Updated model
 */
export function mergeModels (...models) {
  return models.reduce((a, b) => {
    const modelRef = a
    modelRef.rawResults = b.rawResults
    modelRef.exports = b.exports
    return modelRef
  })
}
