import { saveJSON } from '@/saveJSON';
import config from '@/client-config';

const $ = require('jquery');

export function getPath() {
  const hash = (window.location.hash || '#').slice(1);
  // backup option for temporary backwards compatibility
  const path = (window.location.pathname || '/').slice(1);

  // Update urls to use hash
  if (path && !hash) {
    window.location.hash = path;
    // Don't redirect to / since this might be cached
    // window.location.pathname = '';
  }
  return (hash || path);
}

export function remoteReq(requestData) {
  return new Promise((resolve, reject) => {
    const url = config.saveStateURL;
    const failure = () => {
      reject(Error('Remote request failed'));
    };
    const success = (response) => {
      if (response.error) {
        reject(Error(response.message));
      } else {
        resolve(response);
      }
    };
    $.post(url, JSON.stringify(requestData), success, 'json')
      .fail(failure);
  });
}

export function remoteSave(stateStr) {
  const sessionId = getPath();
  console.log('Saving main application state to remote storage.');
  const requestData = {
    sessionId,
    request: 'SET_STATE',
    state: stateStr,
  };
  remoteReq(requestData);
}

export function remoteGetState(sessionId) {
  console.log('Requesting saved main application state from remote storage.');
  const requestData = {
    sessionId,
    request: 'GET_STATE',
  };
  return remoteReq(requestData)
    .then(response => JSON.parse(response.state));
}

export function remoteGetSessionId() {
  console.log('Requesting remote storage sessionId.');
  const requestData = {
    request: 'GET_SESSION_ID',
  };
  return remoteReq(requestData)
    .then(response => response.sessionId);
}

export function saveState(state, sessionId = 'state') {
  console.log('Saving main application state to local storage.');
  const stateStr = JSON.stringify(state);
  const localStr = JSON.stringify(Object.assign(
    {},
    state,
    {
      timestamp: Date.now(),
      isMathSeerStateSave: true,
    },
  ));
  try {
    localStorage.setItem(sessionId, localStr);
  } catch {
    // // localStorage.clear(); // this will clear all sessions
    // localStorage.setItem(sessionId, localStr);
    console.warn('Failed to save session to local storage.');
  }

  if (config.remoteSave && navigator.onLine) {
    remoteSave(stateStr);
  }
}

// Removes saved state from local storage if they are older than N days
// ExpireTime: The expire time is by days. (i.e 7 = 7 days)
export function purgeLocalStorage(expireTime = 30) {
  // day => ms: 1000 ms/s * 60 s/m * 60 m/h * 24 h/d * days
  const ms = 1000 * 60 * 60 * 24 * expireTime;
  const tDaysAgo = Date.now() - ms;

  for (let i = 0; i < localStorage.length; i += 1) {
    const key = localStorage.key(i);

    // Ensure that what is being purged is actual mathseer data
    try {
      const localStorageVal = JSON.parse(localStorage.getItem(key));
      // Do not consider if the timestamp does not exist
      if (!!localStorageVal
        && localStorageVal.isMathSeerStateSave
        && localStorageVal.timestamp
        && parseInt(localStorageVal.timestamp, 10) <= tDaysAgo) {
        // Remove item if older than expireTime
        localStorage.removeItem(key);
      }
    } catch (e) {
      console.log(`Skipping invalid json: ${key}.`);
    }
  }
}

export function clearDefaultState() {
  localStorage.removeItem('state');
}

export function getState(sessionId) {
  const key = sessionId || 'state';
  const stateStr = localStorage.getItem(key);
  try {
    return stateStr ? JSON.parse(stateStr) : null;
  } catch {
    // Reset localstorage if it is not readable
    clearDefaultState();
    return null;
  }
}

function getDateString() {
  const date = new Date();
  const zeroPrefix = n => (n > 9 ? n : `0${n}`);
  const dateStr = [
    date.getFullYear(),
    date.getMonth() + 1,
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
  ].map(zeroPrefix).join('');
  return dateStr;
}

export function downloadState(state) {
  const fileName = `mathseer_${getDateString()}.json`;
  saveJSON(JSON.stringify(state), fileName);
}

export default {
  saveState,
  purgeLocalStorage,
  getState,
  clearDefaultState,
  downloadState,
};
