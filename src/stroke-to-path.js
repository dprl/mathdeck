import fitCurve from 'fit-curve';

export default function strokeToPathD(stroke) {
  // convert a stroke into a SVG path element string
  const x = index => stroke.x[index];
  const y = index => stroke.y[index];
  let pathPts = '';

  // Extract stroke coordinates into points array [[x0,y0],...]
  const points = stroke.x.map((v, i) => [v, y(i)]);

  // Bezier curves fit to the set of points
  // params: points, Error tolerance (higher => more simplification)
  const bCurves = fitCurve(points, 20);

  // convert bCurves into SVG string
  bCurves.forEach((b, i) => {
    if (i === 0) {
      // Move to first point if first bezier curve
      pathPts += `M ${b[0][0]} ${b[0][1]} `;
    }
    // Add bezier control points and final point
    pathPts += `C ${b[1][0]} ${b[1][1]} ${b[2][0]} ${b[2][1]}`
      + ` ${b[3][0]} ${b[3][1]} `;
  });

  if (!pathPts) {
    // pathPts can be empty on a non-moving click
    const radius = stroke.width / 2;
    // draw circle point
    pathPts = `M ${x(0)} ${y(0)} m -${radius}, 0 `
      + `a ${radius},${radius} 0 1,0 ${2 * radius},0 `
      + `a ${radius},${radius} 0 1,0 -${2 * radius},0 `;
  }

  // return path d string
  return pathPts;
}
