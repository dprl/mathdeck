

export const experimentURL = process.env.NODE_ENV === 'production'
  ? 'https://mathdeck.cs.rit.edu/__experiment'
  : 'http://localhost:5021';

export default {
  experimentURL,
};
