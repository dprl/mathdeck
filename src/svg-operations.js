import _ from 'lodash';
import piexif from 'piexifjs';
import {
  render,
  flattenVisibleMathjaxSVG,
  getSVGBoundingClientRect,
} from '@/tex-render';
import {
  // multiPartSymbolUnicodes,
  multiPartUnicodeSymbols,
} from '@/unicode-converter';
// import { mergeMultiparts } from './multipart';

const $ = require('jquery');
// const blueIcon = require('./assets/thumbnail_bluechips.jpg');
// const greyIcon = require('./assets/thumbnail_greychips.jpg');

export const SvgOps = {
  getDefaultHeight() {
    return 20;
  },
  getRenderContainer() {
    return document.getElementById('mathjax-render-container');
  },
  makeSVG() {
    return document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  },
  makeTempRenderer() {
    const tempRenderer = SvgOps.makeSVG();
    // tempRenderer.setAttribute('visibility', 'hidden');
    SvgOps.getRenderContainer().appendChild(tempRenderer);
    return tempRenderer;
  },
  makeContainer(x, y, width, height) {
    const container = SvgOps.makeSVG();
    container.setAttribute('x', x);
    container.setAttribute('y', y);
    container.setAttribute('width', width);
    container.setAttribute('height', height);
    return container;
  },
  async doWithRenderer(operation) {
    /**
     * Perform an operation using a temporary svg renderer
     */
    const tempRenderer = SvgOps.makeTempRenderer();
    const result = await operation(tempRenderer);
    tempRenderer.remove();
    return result;
  },
  convertCoordinate(svgContext, x, y) {
    const point = svgContext.createSVGPoint();
    point.x = x;
    point.y = y;
    return point.matrixTransform(svgContext.getCTM());
  },
  fromElement(el) {
    /**
     * Constructs an object from a svg element
     */
    // Create the object structure
    const object = {
      tagName: el.tagName,
      attributes: {},
    };
    // Add all element attributes as object attributes
    const attrs = el.attributes;
    for (let i = 0; i < attrs.length; i += 1) {
      const key = attrs[i].name;
      let { value } = attrs[i];
      if (['x', 'y', 'width', 'height'].indexOf(key) !== -1) {
        value = parseFloat(value);
      }
      object.attributes[key] = value;
    }
    // Recursively construct and add children to object
    object.children = [];
    const childEls = el.children;
    for (let i = 0; i < childEls.length; i += 1) {
      object.children.push(SvgOps.fromElement(childEls[i]));
    }
    return object;
  },
  toElementString(object) {
    /**
     * Generates an element XML string from the object
     */
    if (!object || !object.attributes) return '';
    const attributesStr = Object.keys(object.attributes)
      .map(key => `${key}="${object.attributes[key]}"`)
      .join(' ');
    const innerStr = object.children.map(SvgOps.toElementString).join('\n');
    return [
      `<${object.tagName} ${attributesStr}>`,
      innerStr,
      `</${object.tagName}>`,
    ].join(!innerStr ? '' : '\n');
  },
  toElementInnerString(object) {
    /**
     * Generates an element XML string from the object
     */
    if (!object.children) return '';
    return object.children.map(SvgOps.toElementString).join('\n');
  },
  toElement(object) {
    /**
     * Returns an element corresponding to the provided object
     */
    // Create the element in a svg context
    return $(`<svg>${SvgOps.toElementString(object)}</svg>`)
      .children().first()[0];
  },
  symbolsToElement(symbols) {
    const bbox = SvgOps.getBBox(...symbols);
    return $([
      `<svg width="${bbox.width}" height="${bbox.height}">`,
      ...symbols.map(s => [
        [
          '<svg',
          `x="${s.attributes.x}"`,
          `y="${s.attributes.y}"`,
          `width="${s.attributes.width}"`,
          `height="${s.attributes.height}">`,
        ].join(' '),
        `<svg viewBox="0 0 ${s.symbolWidth} ${s.symbolHeight}" preserveAspectRatio="none">`,
        SvgOps.toElementInnerString(s),
        '</svg>',
        '</svg>',
      ].join('\n')),
      '</svg>',
    ].join('\n'))[0];
  },
  scaleElement(svgEl, factorX = 1, factorY = 1, originalWidth, originalHeight) {
    _.map(svgEl.children, (cur) => {
      // define a reference to the transform
      const transformList = cur.transform.baseVal;
      const origSvgTO = svgEl.createSVGTransformFromMatrix(cur.getCTM());
      const scale = svgEl.createSVGTransform();
      scale.setScale(factorX, factorY);
      // Clear the transform
      transformList.clear();
      // Apply the scale transform
      transformList.appendItem(scale);
      // Apply and merge the original transform
      transformList.appendItem(origSvgTO);
      transformList.consolidate();
    });

    svgEl.setAttribute('width', originalWidth * factorX);
    svgEl.setAttribute('height', originalHeight * factorY);
    return svgEl;
  },
  scale(svgObj, factorX, factorY) {
    /**
     * Returns an object corresponding to the svg of svgObj,
     * scaled by a factor of factor.
     */
    return SvgOps.doWithRenderer((renderer) => {
      // Record the original location offset
      const {
        x, y, width, height,
      } = svgObj.attributes;
      const copy = _.cloneDeep(svgObj);
      // Set x, y to 0 so the scale is relative to the svgObj itself
      // rather than the container of the svgObj
      copy.attributes.x = 0;
      copy.attributes.y = 0;
      // Convert to element and add to renderer
      let el = SvgOps.toElement(copy);
      renderer.append(el);
      // Perfrom the scaling and convert back to an object
      el = SvgOps.scaleElement(el, factorX, factorY, width, height);
      const result = SvgOps.fromElement(el);
      // Restore the original location offset
      result.attributes.x = x;
      result.attributes.y = y;
      return result;
    });
  },
  resizeElement(svgEl, newWidth = 0, newHeight = 0) {
    const { width, height } = svgEl.getBoundingClientRect();
    const scaleX = newWidth / width;
    const scaleY = newHeight / height;
    return SvgOps.scaleElement(svgEl, scaleX, scaleY, width, height);
  },
  resize(svgObj, newWidth, newHeight) {
    /**
     * Returns an object corresponding to the svg of svgObj,
     * translated by dx, dy.
     */
    return SvgOps.doWithRenderer((renderer) => {
      let el = SvgOps.toElement(svgObj);
      const { width, height } = svgObj.attributes;
      renderer.append(el);
      el = SvgOps.scaleElement(el, newWidth / width, newHeight / height, width, height);
      return SvgOps.fromElement(el);
    });
  },
  scaleSymbol(symbol, factorX, factorY) {
    const {
      x, y, width, height,
    } = symbol.attributes;
    const copy = _.cloneDeep(symbol);
    copy.attributes.x = x * factorX;
    copy.attributes.y = y * factorY;
    copy.attributes.width = width * factorX;
    copy.attributes.height = height * factorY;
    return copy;
  },
  resizeSymbols(symbols, newBBox) {
    const newX = newBBox.x;
    const newY = newBBox.y;
    const newWidth = newBBox.width;
    const newHeight = newBBox.height;
    const bbox = SvgOps.getBBox(...symbols);
    const {
      x, y, width, height,
    } = bbox;

    const widthRatio = newWidth / width;
    const heightRatio = newHeight / height;
    // Scale the svg to fit
    return symbols
      .map(s => ({
        ...s,
        attributes: {
          ...s.attributes,
          x: s.attributes.x - x,
          y: s.attributes.y - y,
        },
      }))
      .map(
        s => SvgOps.scaleSymbol(s, widthRatio, heightRatio),
      )
      .map(s => ({
        ...s,
        attributes: {
          ...s.attributes,
          x: s.attributes.x + newX,
          y: s.attributes.y + newY,
        },
      }));
  },
  translateElement(svgEl, dx = 0, dy = 0, context) {
    // define a reference to the transform
    const transformList = svgEl.transform.baseVal;
    const origSvgTO = context.createSVGTransformFromMatrix(svgEl.getCTM());
    const translate = context.createSVGTransform();
    translate.setTranslate(dx, dy);
    // Clear the transform
    transformList.clear();
    // Apply the scale transform
    transformList.appendItem(translate);
    // Apply and merge the original transform
    transformList.appendItem(origSvgTO);
    transformList.consolidate();
    return svgEl;
  },
  translateInnerElement(svgEl, dx = 0, dy = 0) {
    _.map(svgEl.children, (cur) => {
      SvgOps.translateElement(cur, dx, dy, svgEl);
    });
    return svgEl;
  },
  translateInner(svgObj, dx = 0, dy = 0) {
    /**
     * Returns an object corresponding to the svg of svgObj,
     * translated by dx, dy.
     */
    return SvgOps.doWithRenderer((renderer) => {
      let el = SvgOps.toElement(svgObj);
      renderer.append(el);
      el = SvgOps.translateInnerElement(el, dx, dy);
      return SvgOps.fromElement(el);
    });
  },
  translateOuterElement(svgEl, dx = 0, dy = 0) {
    const newX = parseFloat($(svgEl).attr('x')) + dx;
    const newY = parseFloat($(svgEl).attr('y')) + dy;
    $(svgEl).attr('x', newX).attr('y', newY);

    return svgEl;
  },
  translateOuter(svgObj, dx = 0, dy = 0) {
    /**
     * Returns an object corresponding to the svg of svgObj,
     * translated by dx, dy.
     */
    const translated = _.cloneDeep(svgObj);
    const attrs = translated.attributes;
    attrs.x += dx;
    attrs.y += dy;
    return translated;
  },
  computeElementOffset(svgEl, context) {
    const boundingRect = context.getBoundingClientRect();
    const svgRect = getSVGBoundingClientRect(svgEl);
    const offsetLeft = boundingRect.left - svgRect.left;
    const offsetTop = boundingRect.top - svgRect.top;
    return { offsetLeft, offsetTop };
  },
  normalizeMathJaxSVG(svgEl, renderContext) {
    const flattened = flattenVisibleMathjaxSVG(svgEl);
    const { offsetLeft, offsetTop } = SvgOps.computeElementOffset(flattened, renderContext);
    SvgOps.translateInnerElement(flattened, offsetLeft, offsetTop);
    return flattened;
  },
  renderMathToElement(mathStr) {
    /**
     * Renders the provided mathStr as SVG using mathjax
     * and returns the element
     */
    return SvgOps.doWithRenderer(async (renderer) => {
      const mathEl = $(`<span>${mathStr}</span>`)[0];
      renderer.appendChild(mathEl);

      await render(mathEl);
      const svgEl = $(mathEl).find('.MathJax_SVG>svg')[0];
      // if render fails to create svg return null
      if (!svgEl || svgEl.children[0].childElementCount === 0) return null;
      $(renderer).empty().append(svgEl);
      const flattened = SvgOps.normalizeMathJaxSVG(svgEl, renderer);
      return flattened;
    });
  },
  renderMath(mathStr) {
    return SvgOps.renderMathToElement(mathStr)
      .then(element => (element ? SvgOps.fromElement(element) : null));
  },
  async getMathSVGSymbols(mathStr, color = 'currentColor') {
    let math = {
      mml: '',
      slt: null,
    };
    const svgs = await SvgOps.doWithRenderer(async (renderer) => {
      const mathEl = $(`<span>${mathStr}</span>`)[0];
      renderer.appendChild(mathEl);
      math = await render(mathEl);
      const svgEl = $(mathEl).find('.MathJax_SVG>svg')[0];
      // if render fails to create svg return null
      if (!svgEl || svgEl.children[0].childElementCount === 0) {
        return null;
      }
      $(renderer).empty().append(svgEl);
      const flattened = SvgOps.normalizeMathJaxSVG(svgEl, renderer);
      if (!flattened) {
        return null;
      }
      return SvgOps.splitSvgs(flattened);
    });
    if (!svgs) {
      return null;
    }
    const getColor = svg => svg.children[0].attributes.fill;
    const colors = {};
    svgs.forEach((svg) => {
      const c = getColor(svg);
      if (!colors[c]) {
        colors[c] = [svg];
      } else {
        colors[c].push(svg);
      }
    });
    // console.log(colors);

    const merged = await Promise.all(
      _.map(colors, (svgSet) => {
        const symbol = SvgOps.mergeSvgObjects(...svgSet);
        return symbol;
      }),
    );

    const symbols = merged.map(s => ({
      ...s,
      children: s.children.map(c => ({
        ...c,
        attributes: {
          ...c.attributes,
          fill: getColor(s) === 'gray' ? getColor(s) : color,
        },
      })),
      color: getColor(s),
      symbolWidth: s.attributes.width,
      symbolHeight: s.attributes.height,
    }));

    // const multiParts = await Promise.all(
    //   mergeMultiparts(
    //     multiPartSymbolUnicodes,
    //     svgs,
    //     svg => svg.children[0].attributes.unicodehex,
    //   )
    //     .map(mp => SvgOps.mergeSvgObjects(...mp)),
    // );
    // const symbols = [...svgs, ...multiParts].map(s => ({
    //   ...s,
    //   symbolWidth: s.attributes.width,
    //   symbolHeight: s.attributes.height,
    // }));
    return {
      symbols,
      ...math,
    };
  },
  async makeChipData(latex, isMath = true) {
    // Wrap in \textrm{} if not math
    const str = isMath ? `${latex}` : `\\text{${latex}}`;
    const svgData = await SvgOps.renderMath(`[imath]${str}[/imath]`);
    if (!svgData) return null;
    svgData.attributes.x = 0;
    svgData.attributes.y = 0;
    const chipData = {
      str: latex,
      svgData,
    };
    return chipData;
  },
  getCorners(svgObj, all = false) {
    if (!svgObj || !svgObj.attributes) {
      return [];
    }
    const attrs = svgObj.attributes;
    return [
      // return array with top left and bottom right corners
      [attrs.x, attrs.y],
      ...(all ? [
        // Include bottom left and top right if all is set
        [attrs.x + attrs.width, attrs.y],
        [attrs.x, attrs.y + attrs.height],
      ] : []),
      [attrs.x + attrs.width, attrs.y + attrs.height],
    ];
  },
  getBBox(...objects) {
    // Collect coordinates
    const { xs, ys } = objects
      .map(obj => SvgOps.getCorners(obj)
        .reduce((prev, cur) => ({
          xs: [...prev.xs, cur[0]],
          ys: [...prev.ys, cur[1]],
        }), { xs: [], ys: [] }))
      .reduce((prev, cur) => ({
        xs: [...prev.xs, ...cur.xs],
        ys: [...prev.ys, ...cur.ys],
      }), { xs: [], ys: [] });

    // Compute min and max x and y coordinates
    const minX = _.min(xs);
    const maxX = _.max(xs);
    const minY = _.min(ys);
    const maxY = _.max(ys);

    // Return the bounding box data
    return {
      x: minX,
      y: minY,
      width: maxX - minX,
      height: maxY - minY,
    };
  },
  async mergeSvgObjects(...objects) {
    // eslint-disable-next-line no-param-reassign
    objects = _.compact(objects);
    // Get the bounding box for all objects
    const bbox = SvgOps.getBBox(...objects);
    // Create a svg container for the new merged svg
    const container = SvgOps.makeContainer(bbox.x, bbox.y, bbox.width, bbox.height);
    const containerObj = SvgOps.fromElement(container);
    // Place each object into the new container
    await Promise.all(objects.map((o) => {
      const { x, y } = o.attributes;
      // Reset outer position to (0, 0)
      const translated = SvgOps.translateOuter(o, -x, -y);
      // Update inner positions to account for the offset
      return SvgOps.translateInner(translated, x - bbox.x, y - bbox.y)
      // Copy all updated inner objects into the container
        .then(obj => containerObj.children.push(...obj.children));
    }));
    return containerObj;
  },
  async splitSvgs(svgEl) {
    return SvgOps.doWithRenderer((renderer) => {
      renderer.append(svgEl);
      /**
       * Currently the svgEl is constructed as below.
       *
       * renderer
       * +------------------------+
       * |   svgEl     outerTop   |
       * |   +-------------------+|
       * |   |   c(i)  innerTop  ||
       * |   |   +--------------+||
       * |   |   |              |||
       * |   |   |              |||
       * |   |   +--------------+||
       * |   | ^--innerLeft      ||
       * |   +-------------------+|
       * | ^--outerLeft           |
       * +------------------------+
       *
       * Need to extract each child element and give it
       * its own svg container with the outer offset equal to
       * the outer + its inner, and remove the inner offset
       */

      // Compute the svgElement coordinates
      const sRect = getSVGBoundingClientRect(svgEl);
      const rRect = renderer.getBoundingClientRect();
      // Compute the outer offset of the svgEl group
      const outerLeft = sRect.x - rRect.x;
      const outerTop = sRect.y - rRect.y;

      // For each child svg element convert the inner
      // translation (svg transform)
      // into an outer translation (svg.x, svg.y)
      const svgs = _.map(svgEl.children, (c) => {
        // Compute the child element coordinates
        const cRect = c.getBoundingClientRect();

        // Compute inner offset relative to the group (svgEl)
        const innerLeft = cRect.x - sRect.x;
        const innerTop = cRect.y - sRect.y;

        // Remove the inner offset
        SvgOps.translateElement(
          c,
          -innerLeft,
          -innerTop,
          svgEl,
        );

        // Create a svg container element to hold the position offset
        // which now uses both the outer and inner offsets
        const container = SvgOps.makeContainer(
          outerLeft + innerLeft,
          outerTop + innerTop,
          cRect.width,
          cRect.height,
        );

        container.append(c.cloneNode());
        // Return a new svg object for the processed child
        return SvgOps.fromElement(container);
      });
      // Return the list of svg objects
      return svgs;
    });
  },
  async splitSvgObject(svgObj) {
    if (!svgObj) return [];
    /**
     * Split the svg object into individual svg objects,
     * one for each of its children.
     */
    // Convert the svgObj to a svg element
    const svgEl = SvgOps.toElement(svgObj);
    return SvgOps.splitSvgs(svgEl);
  },
  async getSymbolBBoxInfo(svgObj) {
    const symbolSvgs = await SvgOps.splitSvgObject(svgObj);
    return SvgOps.getSymbolsBBoxInfo(symbolSvgs);
  },
  async getSymbolsBBoxInfo(symbolSvgs, includeSvg = false) {
    return _.compact(symbolSvgs.map((s) => {
      try {
        const symbolUnicode = parseInt(s.children[0].attributes.unicodehex, 16);
        const data = {
          symbol: {
            value: String.fromCharCode(symbolUnicode),
            unicodehex: symbolUnicode,
          },
          boundingRect: {
            x: s.attributes.x,
            y: s.attributes.y,
            width: s.attributes.width,
            height: s.attributes.height,
          },
        };
        if (includeSvg) {
          data.svgObj = s;
        }
        return data;
      } catch {
        return null;
      }
    }));
  },
  getSymbolsInfo(symbols) {
    return _.compact(symbols.map((s) => {
      try {
        const symbolUnicodes = _.map(
          s.children,
          c => c.attributes.unicodehex,
        );
        let symbolValue = symbolUnicodes
          .map(uch => parseInt(uch, 16))
          .map(n => String.fromCharCode(n))
          .join('');

        Object.values(multiPartUnicodeSymbols).forEach((v) => {
          if (_.isEqual(v.unicodeHex, symbolUnicodes)) {
            // Use multipart latex for value if it exists
            symbolValue = v.latex;
          }
        });

        return {
          symbol: {
            value: symbolValue,
            unicodehex: parseInt(symbolUnicodes[0], 16),
            data: s,
          },
          boundingRect: {
            x: s.attributes.x,
            y: s.attributes.y,
            width: s.attributes.width,
            height: s.attributes.height,
          },
        };
      } catch {
        return null;
      }
    }));
  },
  // chipType : blueIcon, greyIcon
  getThumbnail(iconType) {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.open('GET', iconType);
      request.responseType = 'blob';
      request.onload = async function onload(e) {
        if (e.target.status !== 200) reject(new Error('Thumbnails do not exist'));
        resolve(e.target.response);
      };
      request.send();
    });
  },
  blobToBase64(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = function onload() {
        if (reader.error) reject(reader.error);
        const encodedImage = reader.result;
        const prefixKeyEnd = 'base64';
        const prefixEnd = encodedImage.indexOf(prefixKeyEnd) + prefixKeyEnd.length;
        const slicedEncodedImage = encodedImage.slice(prefixEnd, encodedImage.length);
        resolve(slicedEncodedImage);
      };
      reader.readAsDataURL(blob);
    });
  },
  buildImageURL(blob, filename) {
    const blobUrl = window.URL.createObjectURL(blob);
    const blobData = blobUrl.slice(blobUrl.lastIndexOf('/') + 1);
    const imageURL = new URL(`${window.location.origin}/tmpImages/${filename}?blob=${blobData}`);
    return imageURL;
  },
  removeBase64Prefix(base64) {
    const prefixKeyEnd = 'base64';
    const prefixEnd = base64.indexOf(prefixKeyEnd) + prefixKeyEnd.length;
    const slicedEncodedImage = base64.slice(prefixEnd, base64.length);
    return slicedEncodedImage.slice(5);
  },
  async toBase64Image(
    symbols,
    format = 'image/png',
    chipData = null,
    filename = 'test.jpg',
    cardTitle = '',
    cardDescription = '',
  ) {
    const xml = await SvgOps.doWithRenderer(async (renderer) => {
      const el = SvgOps.symbolsToElement(symbols);
      renderer.append(el);
      return new XMLSerializer().serializeToString(el);
    });
    const { width, height } = SvgOps.getBBox(...symbols);
    return new Promise((resolve) => {
      const imgStr = `data:image/svg+xml;utf8,${xml}`;
      const imgEl = new Image();
      imgEl.width = width;
      imgEl.height = height;
      imgEl.src = imgStr;
      const canvas = document.createElement('canvas');
      canvas.width = width;
      canvas.height = height;
      const context = canvas.getContext('2d');
      // context.clearRect(0, 0, canvas.width, canvas.height);
      context.fillStyle = 'white';
      context.lineWidth = 10;

      context.fillRect(0, 0, canvas.width, canvas.height);
      imgEl.onload = async () => {
        context.fillStyle = 'black';
        context.drawImage(imgEl, 0, 0);

        const imageData = canvas.toDataURL(format);
        let result;

        if (chipData) {
          const modifiedUrl = await this.addMetadata(imageData, chipData, symbols,
            cardTitle, cardDescription);
          const imageBlob = await fetch(modifiedUrl)
            .then(res => res.blob());
          const imageURL = this.buildImageURL(imageBlob, filename);

          result = { imageURL, imageBlob, filename };
        } else {
          result = { imageData, xml };
        }

        canvas.remove();
        imgEl.remove();
        resolve(result);
      };
      SvgOps.getRenderContainer().append(canvas);
      SvgOps.getRenderContainer().append(imgEl);
    });
  },
  async addMetadata(base64Url, chipData, symbols, cardTitle, cardDescription) {
    const metadata = {
      isMathSeerImage: true,
      str: chipData.str,
      isFresh: chipData.isFresh,
      cardTitle,
      cardDescription,
    };

    if (!chipData.isFresh) {
      metadata.symbols = symbols;
    }

    const zeroth = {
      270: JSON.stringify(metadata),
    };

    const exifObj = {
      '0th': zeroth, Exif: {}, GPS: {}, '1st': {},
    };

    // Code for adding thumbnail images
    // let thumb = null;
    // if (chipData.isFresh) {
    //   thumb = await this.getThumbnail(blueIcon);
    // } else {
    //   thumb = await this.getThumbnail(greyIcon);
    // }

    // const thumbBase64 = await this.blobToBase64(thumb);
    // exifObj.thumbnail = atob(thumbBase64.split(',')[1]);
    try {
      const exifByte = piexif.dump(exifObj);
      return piexif.insert(exifByte, base64Url);
    } catch {
      return base64Url;
    }
  },
  async buildSVGRequest(svgObj, imgWidth = 300) {
    if (!svgObj.children.length) return null;
    const {
      x, y, width, height,
    } = svgObj.attributes;
    let positioned = await SvgOps.translateInner(svgObj, -x, -y);
    positioned = await SvgOps.translateOuter(positioned, -x, -y);
    const scaled = await SvgOps.resize(positioned,
      imgWidth,
      height * imgWidth / width);
    const symbolInfo = await SvgOps.getSymbolBBoxInfo(scaled);
    const png = await SvgOps.toBase64Image(scaled);
    const svgString = SvgOps.toElementString(scaled);
    const prefixKeyEnd = 'base64,';
    const prefixEnd = png.indexOf(prefixKeyEnd) + prefixKeyEnd.length;
    const slicedEncodedImage = png.slice(prefixEnd, png.length);
    return {
      symbols: symbolInfo,
      image: slicedEncodedImage,
      svg: svgString,
    };
  },
  async buildSymbolsRequest(symbols, imgWidth = 300) {
    if (!symbols.length) return null;
    const bbox = SvgOps.getBBox(...symbols);
    const { width, height } = bbox;

    const imgHeight = height * imgWidth / width;

    const resized = SvgOps.resizeSymbols(symbols, {
      x: 0, y: 0, width: imgWidth, height: imgHeight,
    });
    const symbolsInfo = SvgOps.getSymbolsInfo(resized);
    const { imageData, xml } = await SvgOps.toBase64Image(resized);
    const svgString = xml;
    const prefixKeyEnd = 'base64,';
    const prefixEnd = imageData.indexOf(prefixKeyEnd) + prefixKeyEnd.length;
    const slicedEncodedImage = imageData.slice(prefixEnd, imageData.length);
    return {
      symbols: symbolsInfo,
      image: slicedEncodedImage,
      svg: svgString,
    };
  },
};

export default {
  SvgOps,
};
