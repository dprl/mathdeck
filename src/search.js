import Vue from 'vue';

function doSearch(engine, ...terms) {
  const query = encodeURIComponent(terms
    .map(t => (engine.wrapLeft || '') + t + (engine.wrapRight || ''))
    .join(engine.separator || ' '));
  window.open(engine.webApi + query, '_blank');
}

async function doAutocomplete(engine, ...terms) {
  const query = encodeURIComponent(terms
    .join(engine.separator || ' '));

  // Setting the no-cors mode { mode: 'no-cors' })
  const result = await fetch(engine.jsonApi + query)
    .then(async (response) => {
      const searchResult = await response.json();
      if (searchResult.items && searchResult.items.length === 0) {
        return 'No search results';
      }
      return searchResult;
    })
    .catch(err => `Unable to get autocomplete result: ${err}`);
  return result;
}


export const Approach0 = {
  jsonApi: 'https://approach0.xyz/search/search-relay.php?p=1&q=',
  webApi: 'https://approach0.xyz/search/?p=1&q=',
  separator: ', ',
  wrapLeft: '$',
  wrapRight: '$',
};

export const Bing = {
  webApi: 'https://www.bing.com/search?q=',
};

export const DLMF = {
  webApi: 'https://dlmf.nist.gov/search/search?q=',
  separator: ' and ',
};

export const DuckDuckGo = {
  webApi: 'https://www.duckduckgo.com/?q=',
  instantAnswer: 'https://api.duckduckgo.com/?format=json&q=',
  // suggestions: 'https://api.duckduckgo.com/ac/?q=',
};

export const Google = {
  webApi: 'https://www.google.com/search?q=',
  // suggestions: 'https://suggestqueries.google.com/complete/search?output=toolbar&hl=en&q=',
};

export const SearchOnMath = {
  webApi: 'https://www.searchonmath.com/result?query=',
  wrapLeft: '${',
  wrapRight: '}$',
};

export const SpringerLink = {
  webApi: 'https://link.springer.com/search?query=',
};

export const Wikipedia = {
  webApi: 'https://wikipedia.org/wiki/Special:Search?search=',
};

export const Wolfram = {
  webApi: 'https://www.wolframalpha.com/input/?i=',
};

export const Yahoo = {
  webApi: 'https://search.yahoo.com/search?p=',
};

export const StackExchange = {
  webApi: 'https://math.stackexchange.com/search?q=',
  jsonApi: 'https://api.stackexchange.com/2.2/search/excerpts?sort=relevance&answers=0&site=math&title=',
};

export const SearchEngineManager = new Vue({
  data: {
    engines: {
      Approach0,
      Bing,
      DLMF,
      DuckDuckGo,
      Google,
      SearchOnMath,
      SpringerLink,
      Wikipedia,
      Wolfram,
      Yahoo,
      StackExchange,
    },
  },
  methods: {
    hasEngine(engine) {
      return {}.hasOwnProperty.call(this.engines, engine);
    },
    search(selectedEngine, ...terms) {
      // Only search if there are search terms
      if (terms.length > 0) {
        // Search for the terms using the selected search engine
        const engine = this.hasEngine(selectedEngine)
          ? this.engines[selectedEngine]
          : Google;
        doSearch(engine, ...terms);
      }
    },
    autocomplete(selectedEngine, ...terms) {
      // Only search if there are search terms
      if (terms.length > 0) {
        // Search for the terms using the selected search engine
        // Use StackExchange as default if the selected Engine
        // does not have autocomplete available
        const engine = this.hasEngine(selectedEngine)
                       && this.engines[selectedEngine].jsonApi
          ? this.engines[selectedEngine]
          : StackExchange;


        // Autocomplete engine does not exist
        if (engine) {
          return doAutocomplete(engine, ...terms);
        }
        return `Autocomplete is not available for ${selectedEngine}`;
      }
      return 'No search results';
    },
  },
});

export default {
  SearchEngineManager,
};
