import _ from 'lodash';
import config from '@/client-config';

const $ = require('jquery');

function reformatStroke(stroke, id) {
  const x = index => stroke.x[index];
  const y = index => stroke.y[index];

  // Safety check: at least length elements in x and y
  const length = Math.min(stroke.x.length, stroke.y.length);

  const pointsString = _.range(length) // [0:length)
    .map(i => `${x(i)} ${y(i)}`) // point format
    .join(', '); // point separator

  return {
    points: pointsString,
    id,
  };
}

function buildRequest(strokes) {
  // Reformat Strokes
  const inputStrokes = strokes.map(({ stroke }, id) => reformatStroke(stroke, id));

  // Format Request Time
  const now = new Date();
  now.setMilliseconds(0);
  const isoNow = new Date(now.getTime() - now.getTimezoneOffset() * 60000).toISOString();
  return {
    request_time: isoNow.replace(/T/, ' ').replace(/.000Z/, ''),
    file_id: 'mathdeck-request',
    input_type: 'strokes',
    input_strokes: inputStrokes,
    annotation: 'string',
  };
}

function cleanResult(data) {
  const result = data;
  result.latex = result.latex.substring(2, result.latex.length - 2);
  return result;
}

function recognize(strokes) {
  const url = config.recognizerURL;
  return new Promise((resolve, reject) => {
    function fail() {
      reject(Error('Recognition request failed.'));
    }

    function success(data) {
      const result = cleanResult(data);
      resolve(result);
    }

    console.log('Attempting recognition request');
    const request = buildRequest(strokes);
    $.post(url, JSON.stringify(request), success, 'json').fail(fail);
  });
}

export default {
  reformatStroke,
  buildRequest,
  recognize,
};
