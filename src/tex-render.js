import _ from 'lodash';
import { ScriptEditor as SE } from '@/editor';
import {
  RecognizerEditor as RE,
} from '@/recognizer-editor';
import mathml from './mathml';

const $ = require('jquery');

const xmlParser = new DOMParser();
const parseXML = xmlStr => xmlParser.parseFromString(xmlStr, 'application/xml');
const randColor = () => Math.floor(Math.random() * 205);
const randRGB = () => `rgb(${randColor()},${randColor()},${randColor()})`;

function addColor(node, overwriteColor = '') {
  if (!node || typeof node !== 'object') return;
  const { data } = node;
  let newColor = '';

  if (overwriteColor) {
    newColor = overwriteColor;
    // eslint-disable-next-line no-param-reassign
    node.mathcolor = newColor;
  } else if (!node.mathcolor) {
    // eslint-disable-next-line no-param-reassign
    node.mathcolor = randRGB();
  } else {
    newColor = node.mathcolor;
  }
  for (let i = 0; i < data?.length; i += 1) {
    addColor(data[i], newColor);
  }
}


// eslint-disable-next-line no-undef
MathJax.Hub.Register.StartupHook('onLoad', () => {
  // Once MathJax is loaded,

  // // eslint-disable-next-line
  // Object.defineProperty(MathJax.InputJax.TeX.__proto__.Parse.prototype, 'i', {
  //   get() {
  //     // eslint-disable-next-line no-underscore-dangle
  //     return this._i;
  //   },
  //   set(v) {
  //     console.log(`${v}:${this.string[v]}:${this.string}`);
  //     // eslint-disable-next-line no-underscore-dangle
  //     this._i = v;
  //   },
  // });

  function convertTex(latex) {
    // eslint-disable-next-line no-undef
    const p = MathJax.InputJax.TeX.Parse(latex);

    // console.log(p);
    // p.i = 0;
    // let a;
    // let lastI = 0;
    // // eslint-disable-next-line
    // while ((a = p.GetArgument("",true))) {
    //   console.log(`(${lastI},${p.i}):${a}`);
    //   lastI = p.i;
    // }
    // p.stack.data.map(i => console.log(i.toString()));

    const m = p.mml();
    console.log(m);
    // eslint-disable-next-line no-undef
    const mm = MathJax.ElementJax.mml(m);
    console.log(mm);
    const r = mm.root;
    const mathmlStr = r.toMathML();
    console.log(mathmlStr);
    const svg = r.toSVG();
    console.log(svg);
    const svgStr = svg.element.outerHTML;
    console.log(svgStr);
  }

  window.convertTex = convertTex;
  // eslint-disable-next-line no-undef
  MathJax.InputJax.TeX.postfilterHooks.Add((data) => {
    // Add hook to set color on each node
    try {
      addColor(data.math.root);
    } catch (e) {
      console.error(e);
    }
    // console.log(data.math.root.toMathML());
  });

  // // eslint-disable-next-line no-undef
  // MathJax.Hub.signal.Interest((m) => {
  //   console.log(m);
  // });
});

function mathjaxQueue(...queueFns) {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-undef
    MathJax.Hub.Queue(...queueFns, [resolve]);
  });
}

export function render(element) {
  const parent = element.parentNode;
  return mathjaxQueue(
    // eslint-disable-next-line no-undef
    ['Typeset', MathJax.Hub, parent],
  ).then(() => {
    // Return mathml for rendered element
    try {
      // eslint-disable-next-line no-undef
      const jax = MathJax.Hub.getJaxFor($(parent).find('svg')[0]);
      const mml = jax.root.toMathML();
      const mmlTree = parseXML(mml).firstChild;
      const slt = mathml.toSLT(mmlTree);
      // console.log(slt);
      return { mml, slt };
    } catch {
      return null;
    }
  });
}

const isUse = el => el && el.tagName === 'use';

// Reduce [{xs: [x0, ..],ys:[y0, ..]}, ..] into a a single { xs: [x0, ..], ys: [y0, ..] }
const xyReducer = [
  ({ xs, ys }, cur) => ({ xs: [...xs, ...cur.xs], ys: [...ys, ...cur.ys] }),
  { xs: [], ys: [] },
];

function getBounds(xs, ys) {
  const minX = _.min(xs);
  const maxX = _.max(xs);
  const minY = _.min(ys);
  const maxY = _.max(ys);

  // Compute the x, y, width, and height for the SVG
  return {
    x: minX,
    y: minY,
    left: minX,
    top: minY,
    width: (maxX - minX) || 2, // min width = 2
    height: (maxY - minY) || 2, // min height = 2
  };
}

export function getStrokesBBox(strokesJSON) {
  const { xs, ys } = strokesJSON
    .map(s => ({ xs: s.x, ys: s.y }))
    .reduce(...xyReducer);

  return getBounds(xs, ys);
}

export function getChip(chipId) {
  return $(`#chip-id-${chipId}`).parent().clone();
}

export function getEditorCenter(recognizerWindowOpen) {
  const editorRect = recognizerWindowOpen ? RE.rect : SE.rect;
  const x = editorRect.width / 2;
  const y = editorRect.height / 2;
  return { x, y };
}

export function iterLeaf(nodes, iterFn) {
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < nodes.length; i++) {
    const cur = nodes[i];
    if (cur.children.length === 0) {
      iterFn(cur, i);
    } else {
      iterLeaf(cur.children, iterFn);
    }
  }
}

export function getAllLeafNodes(el) {
  const leafNodes = [];
  iterLeaf(el.children, leaf => leafNodes.push(leaf));
  return leafNodes;
}

export function getSVGBoundingClientRect(svgEl) {
  /**
   * el.getBoundingClientRect works as expected
   * on parent svgs in chrome but not firefox.
   * In firefox it only works on the primitives
   */
  const { xs, ys } = getAllLeafNodes(svgEl)
    .map(leaf => leaf.getBoundingClientRect())
    .map(rect => ({
      xs: [rect.x, rect.x + rect.width],
      ys: [rect.y, rect.y + rect.height],
    }))
    .reduce(...xyReducer);

  const minX = _.min(xs);
  const maxX = _.max(xs);
  const minY = _.min(ys);
  const maxY = _.max(ys);

  return {
    x: minX,
    y: minY,
    left: minX,
    top: minY,
    width: maxX - minX,
    height: maxY - minY,
  };
}

export function flattenVisibleMathjaxSVG(svg) {
  if (svg.flattened) {
    // skip svg if already flattened
    return svg;
  }
  const newSvg = $(
    ['<svg',
      `x="${svg.getAttribute('x') || 0}"`,
      `y="${svg.getAttribute('y') || 0}"`,
      `width="${svg.getBoundingClientRect().width}"`,
      `height="${svg.getBoundingClientRect().height}"`,
      // `style="${svg.getAttribute('style') || ''}"`,
      '></svg>',
    ].join(' '),
  )[0];
  // svg.parentElement.appendChild(newSvg)
  svg.parentNode.insertBefore(newSvg, svg.nextSibling);

  function moveEl(el) {
    // Record the x, y and transforms of el
    const originalCTM = el.getCTM();
    const x = el.getAttribute('x') || 0;
    const y = el.getAttribute('y') || 0;

    let elToMove = el;
    // If el is a use element
    if (isUse(el)) {
      const href = el.getAttribute('href');
      const path = $(href).clone()[0];
      // and we can access the raw path
      if (path) {
        // then use a copy of the path instead of el
        el.remove();
        path.setAttribute('unicodehex', href.split('-')[1]);
        path.removeAttribute('id');
        elToMove = path;
      }
    }
    if (el.tagName === 'rect') {
      el.setAttribute('unicodehex', '2212');
    }

    // define a reference to the transform
    const transformList = elToMove.transform.baseVal;

    // move the element to the new svg
    newSvg.appendChild(elToMove);

    // elToMove.setAttribute('transform', '')
    elToMove.removeAttribute('transform');
    elToMove.removeAttribute('x');
    elToMove.removeAttribute('y');

    // Apply and merge the original transform
    const origSvgTO = svg.createSVGTransformFromMatrix(originalCTM);
    transformList.appendItem(origSvgTO);
    transformList.consolidate();

    // Apply and merge the x, y translation
    const translate = svg.createSVGTransform();
    translate.setTranslate(x, y);
    transformList.appendItem(translate);
    transformList.consolidate();

    // elToMove.setAttribute('fill', 'green') // visibility check
    return elToMove;
  }

  // Depth-first traverse nodes list
  // Moving leaf nodes, removing non-leaf nodes
  function moveLeaf(nodes, color) {
    while (nodes.length > 0) {
      const cur = nodes[0];

      // Check the color of the current node
      let curColor = cur.getAttribute('fill');
      if (curColor === 'currentColor') curColor = null;

      // Compute color to use for drawing (if any)
      const drawColor = curColor || color;

      if (cur.children.length === 0) {
        if (['g', 'svg'].indexOf(cur.tagName) === -1) {
          const movedEl = moveEl(cur);

          // Set fill color
          if (movedEl && drawColor) {
            movedEl.setAttribute('fill', drawColor);
          }
        } else {
          cur.remove();
        }
      } else {
        moveLeaf(cur.children, drawColor);
      }
    }
  }

  // move all of the svg leaf elements to the new svg
  moveLeaf(svg.children);
  // then remove the old svg
  svg.remove();
  // set flattened attribute on svg
  newSvg.flattened = true;
  return newSvg;
}
