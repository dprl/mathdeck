import _ from 'lodash';
import * as mutation from './mutation_types';
import * as action from './action_types';
import GetIdFactory from './id_factory';
// import { SvgOps } from '@/svg-operations';
import Factory from './factory';

// Utility function to create an action function for a mutation
const actionMutate = name => (context, ...values) => context.commit(name, ...values);
// Utility function for basic error logging
const logError = name => error => console.error(
  `Error ${name}:${error}`,
);

// generator for unique chip ids
const chipIdFactory = GetIdFactory();
const svgIdFactory = GetIdFactory();
const strokeIdFactory = GetIdFactory();
const cardIdFactory = GetIdFactory();
const symbolIdFactory = GetIdFactory();

export default {
  ...Factory.createActions({
    facetedSearchMode: '',
    replacementOptions: [],
    recognizedFormula: '',
    alternateResults: [],
    recognitionSearchFilters: [],
    recognitionSearchOpen: false,
    canvasTransitionTrigger: false,
    showContext: '',
    insertionContext: '',
    individualSymbolAlternatives: {},
    activeRecognizedFormula: '',
    newUndoStack: {},
    newRedoStack: {},
    canUndo: false,
    canRedo: false,
    recognizerWindowOpen: false,
    activeTabs: [],
    recognizingCounter: 0,
    activeHistory: null,
    autoRecPercent: 100,
    mode: 'draw',
    controlsActive: false,
    entityActiveChip: null,
    showSearch: false,
    dialogIsOpen: false,
    selectionActive: false,
    activeChip: null,
    draggingChip: null,
    selection: null,
    treeSelection: null,
    selectingOpArg: false,
    searchEngine: 'Google',
    duplicateChipId: null,
    activeFormulaId: null,
    searchBarChipsOrder: [],
    favoriteChips: [],
    canvasChipsOrder: [],
    canvasSVGsOrder: [],
    wikiCards: [],
    myCards: [],
    selectedSymbols: [],
    cardsExpanded: true,
    focusedEntityType: null,
    focusedCardId: null,
    deckExpanded: 1,
    canvasWidth: 0,
    canvasHeight: 0,
    texCursorStart: -1,
    texCursorEnd: 0,
    texString: '',
    texValid: true,
    paletteSymbols: null,
    canvasChipData: null,
    editorStateUpdate: 0,
    targetFormula: null,
    editingMode: 'LaTeX + Visual',
    editingPanelExpanded: true,
    wikicardsAutoUpdate: true,
  }),
  [action.QUEUE_ERROR_MESSAGE]: actionMutate(mutation.QUEUE_ERROR_MESSAGE),
  [action.ADD_SVG]: ({ commit }, svgData) => {
    const newId = svgIdFactory();
    commit(mutation.ADD_SVGS, [{ ...svgData, id: newId }]);
    return newId;
  },
  [action.COPY_SVG]: ({ dispatch, getters }, svgId) => {
    const copy = _.cloneDeep(getters.getSVG(svgId));
    delete copy.ignoreUndo;
    return dispatch(action.ADD_SVG, copy);
  },
  addToCanvasSVGsOrder: (({ commit, dispatch }, data) => {
    let newId;
    // Create svg in store
    return dispatch(action.ADD_SVG, data)
    // Add svg to canvas
      .then((id) => {
        newId = id;
        commit('addToCanvasSVGsOrder', [newId]);
      })
    // Log error in case of failure
      .catch(logError('addToCanvasSVGsOrder'))
      .then(() => newId); // resolve with newId
  }),
  [action.ADD_SYMBOLS]: (({ dispatch }, symbols) => Promise.all([
    ...symbols.map(symbol => dispatch('addToCanvasSVGsOrder', symbol)),
  ])),
  [action.EDIT_SVG]: (actionMutate(mutation.EDIT_SVG)),
  [action.DELETE_SVGS]: (actionMutate(mutation.DELETE_SVGS)),
  copySymbols({ dispatch, getters }, { symbolIds, ignoreUndo }) {
    const symbols = _.cloneDeep(_.compact(symbolIds.map(getters.getSymbol)));
    symbols.forEach((s) => {
      // eslint-disable-next-line no-param-reassign
      delete s.id;
      // Remove ignoreUndo if copying from history
      // eslint-disable-next-line no-param-reassign
      delete s.ignoreUndo;
      if (ignoreUndo) {
        // eslint-disable-next-line no-param-reassign
        s.ignoreUndo = true;
      }
    });
    return dispatch('addToSymbols', symbols);
  },
  updateWikiChips: ({ commit, dispatch }, chipsData) => {
    // Create chipIds for each of the new chips
    const ids = chipsData.map(() => chipIdFactory());
    // Collect all sets of new symbols from the chipsData
    const allSymbols = chipsData.map(c => c.symbols);

    // Add all the new symbols at once
    dispatch('addToSymbols', _.flatten(allSymbols))
      .then((allSymbolIds) => {
        // Then compute start and end indicies for each chips' symbols
        const startEndIndicies = allSymbols
          .reduce((prev, curr) => {
            const l = prev.length;
            const start = prev[l - 1].end;
            return [...prev, {
              start,
              end: start + (curr ? curr.length : 0),
            }];
          }, [{ start: 0, end: 0 }]);

        // Create data for each new chip
        const newChipsData = chipsData.map((chip, i) => {
          // Get the start and end indicies for this chip's symbols
          const ind = startEndIndicies[i + 1];
          // Create the chip data
          return {
            ...chip,
            id: ids[i],
            // include all of the chip's symbolIds
            symbolIds: (ind.start !== ind.end)
              ? allSymbolIds.slice(ind.start, ind.end)
              : [],
            historyId: null,
          };
        });

        // Then create all the chips
        commit('addToChips', newChipsData);
      });
    // return the new card ids
    return ids;
  },
  // Adds a chip to the chips object, and generates a
  // history for it.
  [action.ADD_CHIP]: (async ({ commit, dispatch }, data) => {
    const chipId = chipIdFactory();
    const chipData = data;

    // // Store svgData separately from chipData
    // const { svgData } = chipData;
    // delete chipData.svgData;

    // Store / update symbols separately
    const { symbols, isCopy, noHistory } = chipData;
    delete chipData.symbols;
    delete chipData.isCopy;
    delete chipData.noHistory;

    // // Copy from existing svg or create a new svg
    // const svgId = (chipData.svgId)
    //   ? await dispatch(action.COPY_SVG, chipData.svgId)
    //   : await dispatch(action.ADD_SVG, svgData);

    // Copy from existing symbols
    const newSymbols = (!chipData.symbolIds && symbols)
      ? await dispatch('addToSymbols', symbols)
      : [];
    const symbolIds = (chipData.symbolIds)
      ? await dispatch('copySymbols', { symbolIds: chipData.symbolIds })
      : newSymbols;

    // // Copy other svg if it exists
    // const otherSvgId = chipData.otherSvgId
    //   ? await dispatch(action.COPY_SVG, chipData.otherSvgId)
    //   : undefined;

    // Create a new id for the history of this chip
    const historyId = noHistory ? null : chipIdFactory();

    // Create timestamp
    const now = Date.now();

    // Then create the chip
    commit('addToChips', [{
      ...chipData,
      id: chipId,
      created: now,
      modified: now,
      symbolIds,
      // svgId,
      // otherSvgId,
      historyId,
    }]);

    // Only create a new chip group entry
    // if it was not a copy of a chip
    if (!isCopy && !chipData.ignoreUndo) {
      commit(mutation.ADD_TO_CHIP_GROUPS,
        {
          favoriteId: null,
          newFavoriteId: null,
          str: data.str,
        });
    }
    if (!noHistory) {
      setTimeout(async () => {
        // Create the history data without making the add chip action wait
      // let newSvgData = _.cloneDeep(getters.getSVG(svgId));
      // newSvgData = await SvgOps.resize(
      //   newSvgData,
      //   SvgOps.getDefaultHeight()
      //     * (newSvgData.attributes.width / newSvgData.attributes.height),
      //   SvgOps.getDefaultHeight(),
      // );
      // newSvgData.ignoreUndo = true;
      // const historySvgId = await dispatch(action.ADD_SVG, newSvgData);

        // Copy from existing symbols
        const histSymbols = (symbolIds)
          ? await dispatch('copySymbols', { symbolIds, ignoreUndo: true })
          : [];

        // Create a new id for the copy of this chip
        const historyChipId = chipIdFactory();
        const historyChipData = {
          id: historyChipId,
          str: data.str,
          mml: data.mml,
          ignoreUndo: true,
          isHistory: true,
          // noHistory: true,
          historyId,
          symbolIds: histSymbols,
          // svgId: historySvgId,
          // isTemplate: !!chipData.isTemplate,
          isFresh: !!chipData.isFresh,
          isFavorite: chipData.isFavorite,
        };

        commit('addToChips', [historyChipData]);
        commit(mutation.CREATE_CHIP_HISTORY, { historyId });
        commit(mutation.ADD_TO_CHIP_HISTORY, { historyId, historyChipId });
      }, 0);
    }

    // Return the new chip id
    return chipId;
  }),
  [action.EDIT_CHIP]: (async ({ commit, dispatch, getters }, data) => {
    const chipData = getters.getChip(data.id);
    if (!chipData) return;
    const { historyId, noHistory } = chipData;
    const { symbols } = data;

    const oldSymbolIds = chipData.symbolIds || [];
    // Copy from existing symbols
    const symbolIds = symbols
      ? await dispatch('addToSymbols', symbols)
      : (data.symbolIds || chipData.symbolIds);

    const now = Date.now();

    const updatedChipData = {
      ...data,
      modified: now,
      symbolIds,
    };

    delete updatedChipData.svgData;
    delete updatedChipData.symbols;
    const favoriteId = (getters.getChipGroup(data.str) || {}).newFavoriteId;

    // Only unfavorite a string if the latex string has changed.
    if (data.str !== chipData.str && favoriteId) {
      commit(mutation.REMOVE_FROM_FAVORITE_CHIPS, favoriteId);
      commit(mutation.ADD_TO_CHIP_GROUPS,
        {
          favoriteId: null,
          newFavoriteId: null,
          str: data.str,
        });
    }

    // Determine when blue chip turns into a grey chip
    if ('isFresh' in data && !data.isFresh
          && chipData.isFresh) {
      let keySymbols = symbolIds.map(getters.getSymbol);
      keySymbols = keySymbols.map(s => ({
        ...s,
        id: undefined,
      }));

      // // Since it is a grey chip, the latex string should
      // // be empty
      // updatedChipData.str = '';

      // Register this grey chip using the symbol object as string
      // to chipGroups
      commit(mutation.ADD_TO_CHIP_GROUPS,
        {
          favoriteId: null,
          newFavoriteId: null,
          str: JSON.stringify(keySymbols),
        });
    }

    // if (svgData) {
    //   await dispatch(action.EDIT_SVG, {
    //     ...svgData,
    //     id: svgId,
    //   });
    // }

    if (!noHistory) {
      // Handle chip history if string set to a new string value
      if ((!_.isNil(data.str) && data.str !== chipData.str)
        || (symbolIds.length !== chipData.symbolIds.length)) {
        setTimeout(async () => {
          // Create the history data without making the main action wait

          const historyChipId = chipIdFactory();
          // let historySvgData = _.cloneDeep(getters.getSVG(svgId));
          // historySvgData = await SvgOps.resize(
          //   historySvgData,
          //   20 * (historySvgData.attributes.width / historySvgData.attributes.height),
          //   20,
          // );
          // const historySvgId = await dispatch(action.ADD_SVG, {
          //   ...historySvgData, ignoreUndo: true,
          // });

          // Copy from existing symbols
          const histSymbols = (symbolIds)
            ? await dispatch('copySymbols', { symbolIds, ignoreUndo: true })
            : [];

          const historyChipData = {
            id: historyChipId,
            str: updatedChipData.str,
            mml: data.mml,
            // svgId: historySvgId,
            symbolIds: histSymbols,
            // isTemplate: !!updatedChipData.isTemplate,
            isFresh: !!updatedChipData.isFresh,
            ignoreUndo: true,
            isHistory: true,
            // noHistory: true,
            historyId,
            isFavorite: chipData.isFavorite,
          };

          commit('addToChips', [historyChipData]);

          // If there is a difference between the original and copy
          // of the chip, create a new history record for it
          // otherwise we append to the current record
          commit(mutation.ADD_TO_CHIP_HISTORY, { historyId, historyChipId });
        }, 0);
      }
    }

    // If the chip is favorited, remove the state and remove it from the list of observers
    const favId = chipData.favoriteId;
    if (favId) {
      commit(mutation.REMOVE_CHIP_OBSERVER, {
        id: favId, observerId: data.id,
      });
      updatedChipData.favoriteId = null;
    }

    // Then create the chip with the svgId
    commit(mutation.EDIT_CHIP, updatedChipData);

    // Remove old symbols
    const oldToRemove = oldSymbolIds.filter(id => symbolIds.indexOf(id) === -1);
    if (oldToRemove.length > 0) {
      commit('removeFromSymbols', oldToRemove);
    }
  }),

  addToSearchBarChipsOrder: (({ commit, dispatch }, chipData) => {
    let newId;
    // Create chip in store
    return dispatch(action.ADD_CHIP, chipData)
      // Add chip to search bar
      .then((id) => {
        newId = id;
        commit('addToSearchBarChipsOrder', [newId]);
      })
      // Log error in case of failure
      .catch(logError('addToSearchBarChipsOrder'))
      .then(() => newId);
  }),
  [action.COPY_CHIP_TO_SEARCHBAR]: (({ dispatch, getters }, id) => {
    const chipData = getters.getChip(id);
    return dispatch(
      'addToSearchBarChipsOrder',
      {
        str: chipData.str,
        mml: chipData.mml,
        symbols: chipData.symbols,
        tree: _.cloneDeep(chipData.tree),
        symbolIds: chipData.symbolIds,
        isFresh: chipData.isFresh,
        historyId: chipData.historyId,
        isCopy: true,
        isFavorite: chipData.isFavorite,
        // svgId: chipData.svgId,
        // otherSvgId: chipData.otherSvgId,
        isTemplate: !!chipData.isTemplate,
      },
    )
      .catch(logError(action.COPY_CHIP_TO_SEARCHBAR));
  }),
  removeFromChips: (({ getters, commit }, chipIds) => {
    // Remove each chip from it's favorite chip observer list
    // and collect all of it's symbolIds to delete
    const symbolsToRemove = _.flatten(chipIds.map((id) => {
      const { favoriteId, symbolIds } = getters.getChip(id);
      if (favoriteId) {
        commit(mutation.REMOVE_CHIP_OBSERVER, {
          id: favoriteId, observerId: id,
        });
      }
      // return the chip's symbolIds to delete
      return (symbolIds && symbolIds.length > 0)
        ? [...symbolIds]
        : [];
    }));
    // Delete all the symbols
    commit('removeFromSymbols', [...symbolsToRemove]);
    // Delete the chips
    commit('removeFromChips', chipIds);
  }),
  [action.COPY_CHIP_TO_CANVAS]: (({ getters, dispatch, commit }, { id, x, y }) => {
    const chip = getters.getChip(id);
    const chipCopy = {
      str: chip.str,
      mml: chip.mml,
      // svgId: chip.svgId,
      symbolIds: chip.symbolIds,
      // otherSvgId: chip.otherSvgId,
      tree: _.cloneDeep(chip.tree),
      x,
      y,
      historyId: chip.historyId,
      height: chip.height,
      isCopy: true,
      isFavorite: chip.isFavorite,
      isFresh: !!chip.isFresh,
      isTemplate: !!chip.isTemplate,
    };
    // Create chip in store
    return dispatch(action.ADD_CHIP, chipCopy)
    // Add chip to canvas
      .then(newId => commit('addToCanvasChipsOrder', [newId]))
    // Log error in case of failure
      .catch(logError(action.COPY_CHIP_TO_CANVAS));
  }),
  addToCanvasChipsOrder: (({ dispatch, commit }, chipData) =>
    // Create chip in store
    // eslint-disable-next-line implicit-arrow-linebreak
    dispatch(action.ADD_CHIP, chipData)
    // Add chip to canvas
      .then(newId => commit('addToCanvasChipsOrder', [newId]))
    // Log error in case of failure
      .catch(logError('addToCanvasChipsOrder'))),
  removeFromCanvasSVGsOrder: (({ dispatch, commit }, svgIds) => {
    // make a copy of the ids to delete
    const toDelete = [...svgIds];
    // Remove svgs from canvas
    commit('removeFromCanvasSVGsOrder', toDelete);
    // Remove svgs from store
    return dispatch(action.DELETE_SVGS, toDelete);
  }),
  emptyCanvasSVGsOrder:
    // override default with undoable
    ({ commit, dispatch, getters }) => {
      const svgIds = [...getters.canvasSVGsOrder];
      commit('emptyCanvasSVGsOrder');
      // Remove chips from store
      return dispatch(action.DELETE_SVGS, svgIds);
    },
  removeFromCanvasChipsOrder: (({ dispatch, commit }, chipIds) => {
    // Remove chips from search bar
    commit('removeFromCanvasChipsOrder', chipIds);
    // Remove chips from store
    return dispatch('removeFromChips', chipIds);
  }),
  removeFromSearchBarChipsOrder:
    // override default with undoable
    ({ dispatch, commit }, chipIds) => {
      commit('removeFromSearchBarChipsOrder', chipIds);
      // Remove chips from store
      return dispatch('removeFromChips', chipIds);
    },
  emptySearchBarChipsOrder:
    // override default with undoable
    ({ commit, dispatch, state }) => {
      const chipIds = [...state.searchBarChipsOrder];
      commit('emptySearchBarChipsOrder');
      // Remove chips from store
      return dispatch('removeFromChips', chipIds);
    },
  addToFavoriteChips:
    async ({ commit, dispatch }, { favChipData, favChipStr }) =>
      // eslint-disable-next-line implicit-arrow-linebreak
      dispatch(action.ADD_CHIP,
        {
          ...favChipData,
          isHistory: false,
          height: undefined,
        })
        .then((newId) => {
          commit(mutation.ADD_TO_CHIP_GROUPS, {
            newFavoriteId: newId,
            favoriteId: favChipData.id,
            str: favChipStr,
          });
          commit(mutation.ADD_TO_FAVORITE_CHIPS, newId);
        }),
  removeFromFavoriteChips:
    (async ({ commit, state }, { favChipStr }) => {
      const favChipId = state.chipGroups[favChipStr].newFavoriteId;

      if (favChipId) {
        commit(mutation.REMOVE_FROM_FAVORITE_CHIPS, favChipId);
      }
      commit(mutation.ADD_TO_CHIP_GROUPS, {
        favoriteId: null,
        newFavoriteId: null,
        str: favChipStr,
      });
    }),
  [action.ADD_STROKE](context, stroke) {
    const id = strokeIdFactory();
    context.commit(mutation.CREATE_STROKE, { id, stroke });
    return (({ commit }) => {
      commit(mutation.ADD_STROKE, id);
    })(context);
  },
  [action.CLEAR_STROKES]: (({ commit }, strokeIds) => {
    commit(mutation.CLEAR_STROKES, strokeIds);
  }),
  setRecognizingCounter: (actionMutate('setRecognizingCounter')),
  [action.CLEAR_CHIP_HISTORY]:
    ({ dispatch, commit, getters }, historyId) => {
      const { history } = getters.getHistory(historyId);
      dispatch('removeFromChips', history);
      commit(mutation.CLEAR_CHIP_HISTORY, historyId);
    },
  [action.DELETE_FROM_CHIP_HISTORY]:
    ({ dispatch, commit }, { historyId, historyChipId }) => {
      commit(mutation.DELETE_FROM_CHIP_HISTORY, { historyId, historyChipId });
      dispatch('removeFromChips', [historyChipId]);
    },
  [action.DEQUEUE_ERROR_MESSAGE]: ({ state, commit }) => {
    if (state.errorMessages.length === 0) return undefined;
    const message = state.errorMessages[0];
    commit(mutation.DEQUEUE_ERROR_MESSAGE);
    return message;
  },
  // CARDS actions
  setCard: (({ commit }, ...args) => {
    commit('setCard', ...args);
  }),
  addToCards:
    ({ commit }, values) => {
      const valuesWithIds = values.map(value => ({
        ...value,
        id: cardIdFactory(),
      }));
      commit('addToCards', valuesWithIds);
      return valuesWithIds.map(v => v.id);
    },
  removeFromCards:
    ({ commit }, ...args) => {
      commit('removeFromCards', ...args);
    },
  emptyCards:
    ({ commit }, ...args) => {
      commit('emptyCards', ...args);
    },
  // Symbols actions
  setSymbol: (({ commit }, ...args) => {
    commit('setSymbol', ...args);
  }),
  addToSymbols:
    ({ commit }, values) => {
      const valuesWithIds = values.map(value => ({
        ...value,
        id: symbolIdFactory(),
      }));
      commit('addToSymbols', valuesWithIds);
      return valuesWithIds.map(v => v.id);
    },
  removeFromSymbols:
    ({ commit }, ...args) => {
      commit('removeFromSymbols', ...args);
    },
  emptySymbols:
    ({ commit }, ...args) => {
      commit('emptySymbols', ...args);
    },
  addSymbolAlternatives: ({ commit }, symbol, alternatives) => {
    commit('addIndividualSymbolAlternatives', symbol, alternatives);
  },
  deleteSymbolAlternatives: ({ commit }, symbol) => {
    commit('deleteIndividualSymbolAlternatives', symbol);
  },
  clearIndividualSymbolAlternatives: ({ commit }) => {
    commit('clearIndividualSymbolAlternatives');
  },
};
