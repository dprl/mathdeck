import { SvgOps } from '@/svg-operations';

export default {
  getChip: state => id => state.chips[id],
  getSVG: state => id => state.svgs[id],
  getChipSVG: (state, getters) => (id) => {
    const chip = getters.getChip(id);
    return (chip && chip.svgId)
      ? getters.getSVG(chip.svgId)
      : undefined;
  },
  getSymbol: state => id => state.symbols[id],
  activeFormulaId: state => state.activeFormulaId,
  selectionId: state => state.selection,
  selectionSVG: (state, getters) => getters.getSVG(getters.selectionId),
  canvasSVGsOrder: (state, getters) => state.canvasSVGsOrder
    .filter(id => id !== getters.selectionId),
  searchBarChipsOrder: state => state.searchBarChipsOrder,
  canvasChipsOrder: state => state.canvasChipsOrder,
  chips: state => state.chips,
  duplicateChipId: state => state.duplicateChipId,
  getStroke: state => id => state.strokes[id],
  strokeOrder: state => state.strokeOrder,
  strokes: state => state.strokeOrder.map(id => state.strokes[id]),
  canClearStrokes: state => state.strokeOrder.length > 0,
  getHistory: state => id => state.histories[id],
  getChipGroup: state => str => state.chipGroups[str],
  getSymbolData: (state, getters) => (id) => {
    const symbol = getters.getSymbol(id);
    if (!symbol) {
      return {
        x: 0, y: 0, width: 1, height: 1,
      };
    }
    return {
      id: symbol.id,
      x: symbol.attributes.x,
      y: symbol.attributes.y,
      width: symbol.attributes.width,
      height: symbol.attributes.height,
      symbolWidth: symbol.symbolWidth,
      symbolHeight: symbol.symbolHeight,
      svgContent: SvgOps.toElementInnerString(symbol),
    };
  },
};
