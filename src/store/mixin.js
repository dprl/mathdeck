import { mapGetters, mapState } from 'vuex';
import _ from 'lodash';
import * as AT from '@/store/action_types';
import * as MT from '@/store/mutation_types';
// import { undoableInProgress } from '@/store/undoable';
import {
  ScriptEditor as SE,
  EditorModes as EM,
} from '@/editor';
import {
  RecognizerEditor as RE,
} from '@/recognizer-editor';
import {
// ? leave for webpack build order ??
  getStrokesBBox,
} from '@/tex-render';
// import {
//   charToUnicode,
//   multiPartSymbolStrings,
// } from '@/unicode-converter';
import { SvgOps } from '@/svg-operations';
import { handleDrop } from '@/image-uploader';
import UnicodeLatexMap from '@/unicode-to-latex-map';
import { makeSVGRequest } from '@/recognition';
import * as saveState from '@/save-state';
import { compress } from '@/compress-json';
import { SearchEngineManager as SEM } from '@/search';
import Factory from './factory';
import style from '@/style';
// import mathml from '../mathml';
// import { mergeMultiparts } from '../multipart';
import {
  Bbox,
  Selection,
  Node,
  OpManager,
} from '../vst';

export const mixin = {
  computed: {
    style() {
      return style;
    },
    ...Factory.createGetterSetters(
      'facetedSearchMode',
      'replacementOptions',
      'recognizedFormula',
      'alternateResults',
      'recognitionSearchFilters',
      'recognitionSearchOpen',
      'canvasTransitionTrigger',
      'showContext',
      'insertionContext',
      'individualSymbolAlternatives',
      'activeRecognizedFormula',
      'newUndoStack',
      'newRedoStack',
      'canUndo',
      'canRedo',
      'recognizerWindowOpen',
      'activeTabs',
      'recognizingCounter',
      'controlsActive',
      'entityActiveChip',
      'showSearch',
      'dialogIsOpen',
      'selectionActive',
      'activeHistory',
      'activeChip',
      'draggingChip',
      'treeSelection',
      'selectingOpArg',
      'mode',
      'autoRecPercent',
      'activeFormulaId',
      'searchBarChipsOrder',
      'favoriteChips',
      'duplicateChipId',
      'canvasChipsOrder',
      'canvasSVGsOrder',
      'selectedSymbols',
      'cardsExpanded',
      'focusedEntityType',
      'focusedCardId',
      'wikiCards',
      'myCards',
      'deckExpanded',
      'canvasWidth',
      'canvasHeight',
      'texCursorStart',
      'texCursorEnd',
      'texString',
      'texValid',
      'paletteSymbols',
      'canvasChipData',
      'editorStateUpdate',
      'targetFormula',
      'editingMode',
      'editingPanelExpanded',
      'wikicardsAutoUpdate',
    ),
    ...mapState([
      'histories',
      'errorMessages',
      'searchEngine',
      'redoStack',
      'chipGroups',
    ]),
    ...mapGetters([
      'strokeOrder',
      'canvasSVGsOrder',
      'selectionId',
      'selectionSVG',
      'getChip',
      'getChipSVG',
      'getSVG',
      'getStroke',
      'canClearStrokes',
    ]),
    alternateResultsSet() {
      return new Set(this.alternateResults);
    },
    structureOpsEnabled() {
      return true;
    },
    latexEditingEnabled() {
      return true;
    },
    drawMode() {
      return this.mode === EM.draw;
    },
    selectMode() {
      return this.mode === EM.select;
    },
    textMode() {
      return this.mode === EM.text;
    },
    readOnlyMode() {
      return this.activeFormula.noHistory || this.focusedEntityType === 'wikiCard';
    },
    canClearCanvas() {
      return !this.canvasSVGsOrder.length && !this.strokeOrder.length;
    },
    hasCanvasSVGs() {
      return !!this.canvasSVGsOrder.length;
    },
    activeFormula() {
      if (this.activeFormulaId) {
        return this.getChip(this.activeFormulaId) || {};
      }
      return {};
    },
    activeSymbolIds() {
      return (
        this.activeFormula
        && this.activeFormula.symbolIds
      ) || [];
    },
    activeSymbols() {
      return this.activeSymbolIds.map(this.getSymbol);
    },
    canvasSymbols() {
      return this.canvasChipData?.symbols || [];
    },
    hasCanvasSymbols() {
      return !!this.activeSymbolIds.length;
    },
    activeFormulaState() {
      return this.activeFormula && !!this.activeFormula.isFresh;
    },
    activeFormulaText() {
      return this.activeFormula ? this.activeFormula.str : '';
    },
    activeFormulaMML() {
      return this.activeFormula ? this.activeFormula.mml : '';
    },
    mergeHistories() {
      const mergedHistories = {};

      Object.keys(this.histories).forEach((historyId) => {
        const historyIdNum = parseInt(historyId, 10);
        const chipHistoryRecord = this.histories[historyIdNum].history;
        const hasHistory = chipHistoryRecord.length > 1;

        // If the chip does not have a history record, don't consider it
        const currentChipId = chipHistoryRecord.length > 0 ? chipHistoryRecord[0] : null;
        let currentChipStr = null;
        if (currentChipId) {
          const currentChip = this.getChip(currentChipId);
          currentChipStr = !currentChip.isFresh
            ? this.toSymbolStr(currentChipId)
            : currentChip.str;
        }

        // The historyId is store as most recent at the last index
        // and the oldest at the first index
        if (currentChipStr !== null) {
          if (currentChipStr in mergedHistories) {
            const mergeHistory = mergedHistories[currentChipStr];
            mergeHistory.historyIds.push(historyIdNum);
            // If any branch has history, the merge has history
            mergeHistory.hasHistory = mergeHistory.hasHistory || hasHistory;
          } else {
            mergedHistories[currentChipStr] = { historyIds: [historyIdNum], hasHistory };
          }
        }
      });
      return mergedHistories;
    },
    focusedCard() {
      if (this.focusedCardId) {
        return this.getCard(this.focusedCardId) || {};
      }
      return {};
    },
    symbolBoxMap() {
      return _.merge({}, ...this.canvasSymbols
        .map(s => ({
          [s.color]: {
            loc: {
              x: s.attributes.x,
              y: s.attributes.y,
            },
            size: {
              width: s.attributes.width,
              height: s.attributes.height,
            },
          },
        })));
    },
  },
  methods: {
    // ...Factory.createCollectionMethods('symbols'),
    ...Factory.createCollectionMethods('wikiCards'),
    ...Factory.createCollectionMethods('myCards'),
    addSymbolAlternatives(symbol, alternatives) {
      return this.$store.dispatch('addSymbolAlternatives', { symbol, alternatives });
    },
    deleteSymbolAlternatives(symbol) {
      return this.$store.dispatch('deleteSymbolAlternatives', symbol);
    },
    clearSymbolAlternatives() {
      return this.$store.dispatch('clearIndividualSymbolAlternatives');
    },
    addToSymbols(values) {
      return this.$store.dispatch('addToSymbols', values);
    },
    removeFromSymbols(values) {
      return this.$store.dispatch('removeFromSymbols', values);
    },
    getSymbol(id) {
      return this.$store.getters.getSymbol(id);
    },
    setSymbol(id, value) {
      return this.$store.dispatch('setSymbol', {
        ...value,
        id,
      });
    },
    addToCards(values) {
      return this.$store.dispatch('addToCards', values);
    },
    removeFromCards(values) {
      return this.$store.dispatch('removeFromCards', values);
    },
    getCard(id) {
      return this.$store.state.cards[id];
    },
    setCard(id, value) {
      return this.$store.dispatch('setCard', {
        ...value,
        id,
      });
    },
    toSelectMode() {
      this.mode = EM.select;
    },
    toDrawMode() {
      this.mode = EM.draw;
    },
    toTextMode() {
      this.mode = EM.text;
    },
    expandDeck() {
      // record deck width and height for scaling renders
      // before expanding the deck (and hiding the canvas)
      const { width, height } = SE.rect;
      this.canvasWidth = width;
      this.canvasHeight = height;
      if (this.deckExpanded === 0) {
        this.deckExpanded = 1;
      } else if (this.deckExpanded === 1) {
        this.deckExpanded = 2;
      }
    },
    collapseDeck() {
      if (this.deckExpanded === 2) {
        this.deckExpanded = 1;
      } else if (this.deckExpanded === 1) {
        this.deckExpanded = 0;
      }
    },
    async selectChip(chipId, cardId) {
      if (chipId) {
        const operation = OpManager.getOp();
        if (this.selectingOpArg && operation) {
          OpManager.setOp(null);
          this.selectingOpArg = false;
          await this.performOperationWithChip(operation, chipId);
        } else {
          if (!this.activeTabs.includes(chipId)) {
            this.activeTabs = this.activeTabs.concat([chipId]);
          }
          this.setActiveFormula(chipId);
          this.focusedCardId = cardId;
          this.focusedEntityType = 'chip';
        }
      }
    },
    async newChip(newTab = true) {
      const chipId = await this.addChip({});
      if (newTab) {
        this.activeTabs = this.activeTabs.concat([chipId]);
        this.selectChip(chipId);
      }
      return chipId;
    },
    async copyActiveChip() {
      const chipId = await this.addChip({ ...this.activeFormula, noHistory: false });
      this.selectChip(chipId);
    },
    selectCard(cardId) {
      this.focusedCardId = cardId;
      if (cardId) {
        // const card = this.getCard(cardId);
        // if (card) {
        //   this.setActiveFormula(card.chipId);
        // }
        this.focusedEntityType = 'card';
      }
    },
    // CARDS
    copyFocusedCard() {
      const card = this.getCard(this.focusedCardId);
      return this.newCard(card.chipId, card.title, card.description);
    },
    newCardFromActiveFormula() {
      return this.newCard(this.activeFormulaId);
    },
    newEmptyCard() {
      const chipId = this.addChip({});
      return this.newCard(chipId);
    },
    async newCard(fromChipId, title = '', description = '') {
      const chip = this.getChip(fromChipId);
      const newChipData = (chip)
        ? { ...chip, noHistory: false }
        : { str: '', isFresh: true };
      const chipId = await this.addChip(newChipData);

      // Make a new card with the chip id
      const [cardId] = await this.addToCards([{
        title,
        chipId,
        description,
      }]);
      // Add the cardId to myCards index
      await this.addToMyCards([cardId]);

      this.selectCard(cardId);
    },
    cardClick(cardId) {
      this.selectCard(cardId);
    },

    wikiCardClick(cardId) {
      const card = this.getCard(cardId);
      // Make a copy when selecting
      return this.newCard(card.chipId, card.title, card.description);
    },

    cardChipClick(e, cardId, insertionMode, wikicard = false) {
      e.preventDefault();
      e.stopPropagation();
      const card = this.getCard(cardId);
      if (card) {
        const { chipId } = card;
        if (!!insertionMode && chipId) {
          const chip = this.getChip(chipId);
          if (chip && chip.str) {
            this.insertText(chip.str);
          }
        } else if (wikicard) {
          this.copyAndFocusChip(e, chipId);
        } else {
          this.selectChip(chipId, cardId);
        }
      }
    },
    async copyAndFocusChip(e, chipId) {
      e.preventDefault();
      e.stopPropagation();
      if (chipId) {
        const formula = this.getChip(chipId);
        const copyChipId = await this.addChip({ ...formula, noHistory: false });
        this.selectChip(copyChipId);
      }
    },
    async setWikiCards(cards) {
      // Create chips data
      const chipsToAdd = _.map(cards, (card) => {
        const { str, symbols } = card.formula;
        return {
          // ignoreUndo: true,
          noHistory: true,
          isFresh: true,
          str,
          symbols: symbols || [],
        };
      });

      // Add chips and collect their chipIds
      const cardChipIds = await this.$store
        .dispatch('updateWikiChips', chipsToAdd);

      // Create cards data
      const cardsWithChips = _.map(cards, (card, i) => {
        const chipId = cardChipIds[i];

        const {
          title,
          confidence,
          description,
          sourceLink,
          sourceName,
        } = card;

        return {
          // ignoreUndo: true,
          isWikiCard: true,
          title,
          confidence,
          description,
          sourceLink,
          sourceName,
          chipId,
        };
      });

      // Add cards add collect their cardIds
      const cardIds = await this.addToCards(cardsWithChips);

      // Collect the chipIds from the old wiki cards
      const chipIds = this.wikiCards
        .map(this.getCard)
        .map(c => c.chipId);

      // Update the wikiCards to the new cardIds
      this.wikiCards = [...cardIds];

      // Delete the old chips from the old wiki cards
      this.removeFromChips(chipIds);
    },
    async setActiveFormula(chipId) {
      if (this.selectedSymbols && this.selectedSymbols.length > 0) {
        // Drop selection before switching active formula
        await this.createTemplateFromSymbols(this.selectedSymbols);
        this.makeSelection(null);
      }
      const chipData = this.getChip(chipId);
      const text = chipData.str;

      const formulaData = await this.renderForCanvas(text);
      if (formulaData) {
        this.texValid = true;
        await this.editChip({
          ...chipData,
          ...formulaData,
          str: text,
          isFresh: true,
        });
      }
      this.resetDuplicateObserver();
      this.activeFormulaId = chipId;
    },
    nextActiveFormula() {
      if (this.searchBarChipsOrder.length > 0) {
        [this.activeFormulaId] = this.activeTabs;
      } else {
        this.addChipToSearchBar({
          str: '',
          symbolIds: [],
        }).then((id) => {
          this.selectChip(id);
        });
      }
    },
    setActiveStale() {
      if (this.activeFormulaId) {
        this.editChip({
          ...this.activeFormula,
          isFresh: false,
        });
      }
    },
    debRenderFormula: _.throttle(function r(id) {
      const f = this.getChip(id);
      this.renderForCanvas(f.str)
        .then((formulaData) => {
          this.texValid = !!formulaData;
          if (formulaData) {
            return this.editChip({
              ...f,
              ...formulaData,
              str: f.str,
              isFresh: true,
            });
          }
          return null;
        });
    }, 300),
    // Inserts text into text area at cursor location
    async insertText(newText) {
      if (!newText) return;
      // Update text
      const start = this.texCursorStart === -1 ? 0 : this.texCursorStart;
      const end = this.texCursorEnd || 0;
      let text = this.activeFormulaText;
      const prefix = text.slice(0, start);
      const suffix = text.slice(end);
      text = `${prefix + newText} ${suffix}`;
      await this.updateEditorState('text', text);

      // Update the cursor position
      const newCursorPosition = start + newText.length;
      this.texCursorStart = newCursorPosition;
      this.texCursorEnd = newCursorPosition;
    },
    makeSelection(selectedElements) {
      if (this.selectedSymbols.length === 0
        && (!selectedElements || selectedElements.length === 0)
      ) {
        // Return since there are no changes to make
        return;
      }
      const extractSymbolId = el => parseInt(el.dataset.symbolId, 10);
      const selectedIds = selectedElements
        ? _.compact(selectedElements.map(extractSymbolId))
        : [];
      this.makeSelectionFromIds(selectedIds);
    },
    async makeSelectionFromIds(selectedIds) {
      const droppedIds = _.difference(
        this.selectedSymbols,
        selectedIds,
      );
      if (droppedIds && droppedIds.length > 0) {
        // Note any dropped ids not already part of the active formula
        const newDropIds = _.difference(
          droppedIds,
          this.activeFormula.symbolIds,
        );

        if (newDropIds && newDropIds.length > 0) {
          const newSymbols = _.cloneDeep(
            newDropIds.map(id => this.getSymbol(id)),
          );

          // Clear symbol ids
          // eslint-disable-next-line no-param-reassign
          newSymbols.forEach((s) => { delete s.id; });

          // Add them as symbols in the store
          const ids = await this.addToSymbols(newSymbols);
          // Add them to the active formula
          this.updateEditorState('formula', {
            symbolIds: [...this.activeFormula.symbolIds, ...ids],
            isFresh: false,
          });
        }
      }

      this.selectedSymbols = [...selectedIds];
    },
    scaleForCanvas(inputSymbols) {
      if (!(inputSymbols && inputSymbols.length > 0)) {
        return null;
      }

      let { width, height } = this.recognizerWindowOpen ? RE.rect : SE.rect;
      let bbox = SvgOps.getBBox(...inputSymbols);

      if (width === 0 || height === 0) {
        width = this.canvasWidth;
        height = this.canvasHeight;
      }
      // Compute the scaling ratio to fit within the preview area
      const widthRatio = width / bbox.width;
      const heightRatio = height / bbox.height;
      const ratio = 0.8 * (
        widthRatio <= heightRatio
          ? widthRatio
          : heightRatio);
        // Scale the svg to fit
      let symbols = inputSymbols.map(
        s => SvgOps.scaleSymbol(s, ratio, ratio),
      );
      bbox = SvgOps.getBBox(...symbols);
      symbols = symbols.map(s => ({
        ...s,
        attributes: {
          ...s.attributes,
          x: s.attributes.x + (width - bbox.width) / 2,
          y: s.attributes.y + (height - bbox.height) / 2,
        },
      }));

      // const symbolIds = await this.addToSymbols(
      // );
      return {
        symbols,
        bbox,
      };
    },
    async recognizeCanvasSymbols() {
      this.recognitionInProgress = true;
      const activeId = this.activeFormulaId;
      if (!this.hasCanvasSymbols || !activeId) {
        this.recognitionInProgress = false;
        return;
      }
      const oldSymbolIds = [...this.activeSymbolIds];
      const oldSymbols = oldSymbolIds.map(this.getSymbol);

      const formulaData = await this.recognizeSymbolsIntoChip(oldSymbols);

      this.updateEditorState('formula', {
        ...formulaData,
        isFresh: true,
      })
        .then((async () => {
          await this.emptySelectedSymbols();
          await this.removeFromSymbols(oldSymbolIds);
        }))
        .catch(() => console.log('Failed to recognize canvas symbols'))
        .finally(() => {
          this.recognitionInProgress = false;
        });
    },
    emptySelectedSymbols() {
      return this.$store.dispatch('emptySelectedSymbols');
    },
    async deleteSelectedSymbols() {
      // Remove selected symbols from active formula
      await this.updateEditorState('formula', {
        symbolIds: [ // = current - (newly selected) + old selected
          ..._.difference(
            this.activeFormula.symbolIds,
            this.selectedSymbols,
          ),
        ],
        isFresh: false,
      });
      // Remove selected symbols from selection
      await this.emptySelectedSymbols();
      // Remove selected symbols data
      await this.removeFromSymbols([...this.selectedSymbols]);
    },
    favorite(chipId) {
      this.resetDuplicateObserver();
      const favChipData = this.getChip(chipId);
      const favChipStr = favChipData.str === '' && favChipData.symbolIds.length > 0
        ? this.toSymbolStr(favChipData.id) : favChipData.str;
      this.$store.dispatch('addToFavoriteChips', { favChipData, favChipStr });
    },
    unfavorite(chipId) {
      this.resetDuplicateObserver();
      const favChipData = this.getChip(chipId);
      const favChipStr = favChipData.str === '' && favChipData.symbolIds.length > 0
        ? this.toSymbolStr(favChipData.id) : favChipData.str;
      this.$store.dispatch('removeFromFavoriteChips', { favChipStr });
    },
    // modify duplicate observer. If chipId is null, and status if false
    // we are saying no duplicates are found
    resetDuplicateObserver() {
      this.duplicateChipId = null;
    },
    copyChipToSearchBar(id) {
      return this.$store.dispatch(AT.COPY_CHIP_TO_SEARCHBAR, id);
    },
    copyChipToCanvas(id, x, y) {
      return this.$store.dispatch(AT.COPY_CHIP_TO_CANVAS, { id, x, y });
    },
    chipInArray(chipId, array) {
      return !!chipId && array.indexOf(chipId) !== -1;
    },
    isChipInQuery(chipId) {
      return this.chipInArray(chipId, this.searchBarChipsOrder);
    },
    isChipInFavorites(chipId) {
      return this.chipInArray(chipId, this.favoriteChips);
    },
    isHistoryChip(chipId) {
      const chipData = this.getChip(chipId);
      return !!('isHistory' in chipData && chipData.isHistory);
    },
    isChipOnCanvas(chipId) {
      return this.chipInArray(chipId, this.canvasChipsOrder);
    },
    getChipEl(chipId) {
      return document.getElementById(`chip-id-${chipId}`);
    },
    getChipContainerEl(chipId) {
      if (!chipId) return null;
      const chipEl = this.getChipEl(chipId);
      if (!chipEl) return null;
      return chipEl.closest('.chip-container');
    },
    getElementIndexIn(el, container) {
      return Array.prototype.indexOf.call(container.children, el);
    },
    doChipDragOver: _.throttle(function doChipDragOver(
      ev, chipArray, arrayEl,
    ) {
      const overContainer = ev.target.closest('.chip-container');
      const dragging = this.getChipContainerEl(this.draggingChip);
      // Only preview for chip movement within searchbar
      if (dragging && overContainer
        && this.chipInArray(this.draggingChip, chipArray)) {
        // Check that the dragging chip is different
        // from the chip being dragged over
        if (dragging === overContainer) return;
        const ocBCR = overContainer.getBoundingClientRect();
        const isOnLeftHalf = ev.clientX <= (ocBCR.x + ocBCR.width / 2);
        // Move the chip DOM node into position to 'preview'
        // the drop operation reposition
        dragging.classList.add('dragged-chip');
        arrayEl.insertBefore(
          dragging,
          (isOnLeftHalf) ? overContainer : overContainer.nextSibling,
        );
      }
    }, 200),
    reorderChipInCollection(chipId, collection, collectionEl) {
      const oldIndex = collection.indexOf(chipId);
      const chipEl = this.getChipContainerEl(chipId);
      const newIndex = this.getElementIndexIn(chipEl, collectionEl);
      if (oldIndex === -1 || newIndex === -1) return undefined;
      // The following assumes the DOM preview reorder has occurred
      // Remove chip from old position
      const newOrder = [...collection];
      newOrder.splice(oldIndex, 1);
      // Place chip in new position
      newOrder.splice(newIndex, 0, chipId);
      // Remove dragged chip class
      _.forEach(collectionEl.children,
        el => el.classList.remove('dragged-chip'));
      return newOrder;
    },
    setSearchEngine(engine) {
      if ({}.hasOwnProperty.call(SEM.engines, engine)) {
        this.$store.dispatch('setSearchEngine', engine);
      }
    },
    queueErrorMessage(message) {
      return this.$store.dispatch(AT.QUEUE_ERROR_MESSAGE, message);
    },
    dequeueErrorMessage() {
      return this.$store.dispatch(AT.DEQUEUE_ERROR_MESSAGE);
    },
    getStrokes() {
      return this.$store.getters.strokes;
    },
    deleteSelection() {
      // const oldId = this.selectionId;
      // return this.setSelection(null).then(
      //   () => this.deleteSVGFromCanvas(oldId),
      // );
    },
    deleteChip(id) {
      if (this.isChipInQuery(id)) {
        this.deleteChipFromQuery(id);
      } else if (this.isChipOnCanvas(id)) {
        this.deleteCanvasChip(id);
      } else if (this.isChipInFavorites(id)) {
        this.unfavorite(id);
      }
    },
    removeFromChips(ids) {
      return this.$store.dispatch('removeFromChips', ids);
    },
    deleteCanvasChip(id) {
      return this.deleteCanvasChips([id]);
    },
    deleteCanvasChips(ids) {
      return this.$store.dispatch('removeFromCanvasChipsOrder', ids);
    },
    clearChipHistory(historyId) {
      return this.$store.dispatch(AT.CLEAR_CHIP_HISTORY, historyId);
    },
    deleteChipFromHistory(historyId, historyChipId) {
      return this.$store.dispatch(AT.DELETE_FROM_CHIP_HISTORY, { historyId, historyChipId });
    },
    deleteChipFromQuery(id) {
      this.$store.dispatch('removeFromSearchBarChipsOrder', [id]);
    },
    addStroke(stroke) {
      return this.$store.dispatch(AT.ADD_STROKE, stroke);
    },
    editChip(chipData) {
      this.resetDuplicateObserver();
      this.saveToUndoStack();
      return this.$store.dispatch(AT.EDIT_CHIP, chipData);
    },
    editCard(cardData) {
      const cardId = cardData.id;
      return cardId
        ? this.setCard(cardId, cardData)
        : null;
    },
    updateCardTitle: _.debounce(function updateCardTitle(value) {
      if (this.focusedCardId) {
        this.editCard({
          ...this.focusedCard,
          title: value || '',
        });
      }
    }, 300),
    updateCardDescription: _.debounce(function updateCardDescription(value) {
      if (this.focusedCardId) {
        this.editCard({
          ...this.focusedCard,
          description: value || '',
        });
      }
    }, 300),
    addSVGToCanvas(svgData) {
      return this.$store.dispatch('addToCanvasSVGsOrder', svgData);
      //   {
      //   svgContent, x, y, width, height, selected,
      // });
    },
    async createTemplateFromSymbols(symbolIds) {
      if (!symbolIds || symbolIds.length === 0) return;
      // Copy symbols
      let symbols = _.cloneDeep(symbolIds.map(this.getSymbol));

      // Compute Bounding box
      const bbox = SvgOps.getBBox(...symbols);

      // Compute the new x and y values
      // relative to the bounding box
      symbols = symbols.map(s => ({
        ...s,
        id: undefined,
        attributes: {
          ...s.attributes,
          x: s.attributes.x - bbox.x,
          y: s.attributes.y - bbox.y,
        },
      }));

      // Make new canvas chip
      // Scale the chip so it is center
      // to the canvas
      await this.addChipToCanvas({
        str: '',
        x: bbox.x,
        y: bbox.y,
        height: bbox.height,
        symbols: this.scaleForCanvas(symbols).symbols,
        isFresh: false,
      });
    },
    async createTemplateFromSelectedSymbols() {
      if (this.selectedSymbols.length === 0) return;
      // Make template from symbols
      await this.createTemplateFromSymbols(this.selectedSymbols);
      // Remove symbols
      await this.deleteSelectedSymbols();
    },
    async duplicateSelectedSymbols() {
      if (this.selectedSymbols.length === 0) return;
      // Make the duplicates
      const duplicates = _.cloneDeep(
        this.selectedSymbols.map(this.getSymbol),
      ).map(s => ({
        ...s,
        // Clear id
        id: undefined,
        // Offset position of duplicates
        attributes: {
          ...s.attributes,
          x: (s.attributes.x || 10) - 10,
          y: (s.attributes.y || 10) - 10,
        },
      }));
      // Add them as symbols in the store
      const ids = await this.addToSymbols(duplicates);
      // Add them to the active formula
      this.updateEditorState('formula', {
        symbolIds: [...this.activeFormula.symbolIds, ...ids],
        isFresh: false,
      });
    },
    deleteSVGFromCanvas(id) {
      return this.$store.dispatch('removeFromCanvasSVGsOrder', [id]);
    },
    addSVG(svgData) {
      return this.$store.dispatch(AT.ADD_SVG, svgData);
    },
    editSVG(svgData) {
      if (!svgData.id) {
        return this.addSVG(svgData);
      }
      return this.$store.dispatch(AT.EDIT_SVG, {
        ...svgData,
      });
    },
    moveSymbol(id, x, y) {
      if (!id) {
        return null;
      }
      const symbolData = this.getSymbol(id);
      return this.setSymbol(id, {
        ...symbolData,
        attributes: {
          ...symbolData.attributes,
          x,
          y,
        },
      });
    },
    setSelection(svgId) {
      if (this.selectionId !== svgId) {
        return this.$store.dispatch('setSelection', svgId);
      }
      return Promise.resolve();
    },
    scaleSelectionSVG(...args) {
      return this.scaleSVG(this.selectionId, ...args);
    },
    scaleChipSVG(chipId, factor) {
      const chipData = { ...this.getChip(chipId) };
      const svgData = this.getSVG(chipData.svgId);
      if (this.isChipOnCanvas(chipId)) {
        const { width, height } = svgData.attributes;
        const newWidth = width * factor;
        const newHeight = height * factor;

        chipData.x -= (newWidth - width) / 2;
        chipData.y -= (newHeight - height);
      }

      return this.scaleSVG(chipData.svgId, factor, false)
        .then(() => this.editChip(chipData));
    },
    scaleChipHeight(chipId, factor) {
      const chipData = { ...this.getChip(chipId) };
      const oldHeight = (chipData.height || 30);
      const newHeight = oldHeight * factor;
      chipData.height = newHeight;
      if (chipData.x && chipData.y) {
        const symbolBBox = SvgOps.getBBox(
          ...chipData.symbolIds.map(this.getSymbol),
        );
        const deltaY = newHeight - oldHeight;
        const aspectRatio = symbolBBox.width / symbolBBox.height;
        const newWidth = newHeight * aspectRatio;
        const oldWidth = oldHeight * aspectRatio;
        const deltaX = newWidth - oldWidth;
        chipData.y -= deltaY;
        chipData.x -= deltaX / 2;
      }
      return this.editChip(chipData);
    },
    scaleSVG(svgId, factor, reposition = true) {
      if (!svgId) {
        return Promise.resolve();
      }
      const svg = this.getSVG(svgId);
      const originalWidth = svg.attributes.width;
      const originalHeight = svg.attributes.height;
      return SvgOps.scale(svg, factor, factor)
        .then((scaled) => {
          const newWidth = scaled.attributes.width;
          const newHeight = scaled.attributes.height;
          const { x, y } = scaled.attributes;
          return this.$store.dispatch(AT.EDIT_SVG, {
            ...scaled,
            id: svgId,
            attributes: {
              ...scaled.attributes,
              x: reposition ? x - (newWidth - originalWidth) / 2 : x,
              y: reposition ? y - (newHeight - originalHeight) : y,
            },
          });
        });
    },
    addChip(chipData) {
      return this.$store.dispatch(AT.ADD_CHIP, chipData);
    },
    scrollQueryBar() {
      const queryBar = document.getElementById('query-bar');
      const afterChips = document.getElementById('after-chips');
      // scroll to the point after all chips
      queryBar.scrollLeft = afterChips.offsetLeft;
    },
    // Scroll the query bar to the point
    // where the chipId is
    scrollToChip(chipId) {
      if (chipId === null) return;
      const duplicateChip = document.getElementById(`chip-id-${chipId}`);

      // scroll to the point where the chip exist
      duplicateChip.scrollIntoView({ inline: 'center' });
    },
    addChipToSearchBar(chipData) {
      return this.$store.dispatch('addToSearchBarChipsOrder', {
        ...chipData,
        x: undefined,
        y: undefined,
      }).then((result) => {
        this.scrollQueryBar();
        return result;
      });
    },
    // Clear the formula input area, and preview popper (draft input)
    // contents
    clearDraftInput() {
      const inputArea = document.getElementsByClassName('formula-input-area')[0];
      const inputContainer = document.getElementsByClassName('draft-chip')[0];

      inputArea.value = '';

      // The popper will always be the sibling element of draft-chip.
      // The popper should disappear when a user clears the draft input
      const closestPopper = inputContainer.nextElementSibling;
      closestPopper.style.display = 'none';
    },
    emptySearchBarChipsOrder() {
      if (this.searchBarChipsOrder.indexOf(this.activeFormulaId) !== -1) {
        this.activeFormulaId = null;
      }
      this.clearDraftInput();
      return this.$store.dispatch('emptySearchBarChipsOrder');
    },
    addChipToCanvas(chipData) {
      return this.$store.dispatch('addToCanvasChipsOrder', chipData);
    },
    // Search location indicates where do we want
    // to checking for duplicate chips. (i.e searchbar, favorite, history, canvas etc..)
    // By default, it compares current chip/latex with chips in the searchbar.
    checkDuplicate(chipId, latex, checkLoc = 'searchbar') {
      const chipData = this.getChip(chipId);
      let currSymbols = null;

      const chipStr = chipId ? this.getChip(chipId).str : latex.replace(/\s/g, '');

      let searchArr = this.searchBarChipsOrder;

      // Searching for duplicate chip in favorites
      if (checkLoc === 'fav') {
        searchArr = this.favoriteChips;
      }

      if (chipData) {
        currSymbols = _.cloneDeep(chipData.symbolIds.map(this.getSymbol));
        currSymbols.forEach((s) => {
          // eslint-disable-next-line no-param-reassign
          delete s.id;
          // eslint-disable-next-line no-param-reassign
          delete s.ignoreUndo;
        });
      }

      for (let i = 0; i < searchArr.length; i += 1) {
        if (searchArr[i]) {
          const oldChipData = this.getChip(searchArr[i]);
          const oldSymbols = _.cloneDeep(oldChipData.symbolIds.map(this.getSymbol));
          oldSymbols.forEach((s) => {
            // eslint-disable-next-line no-param-reassign
            delete s.id;
            // eslint-disable-next-line no-param-reassign
            delete s.ignoreUndo;
          });
          // If we want to check against the active formula
          // oldChipData.id !== this.activeFormulaId
          // Need to make sure that oldChipData is not undefined
          // and oldChipData has str in field
          if (oldChipData && oldChipData.str) {
            if ((oldChipData.str.replace(/\s/g, '') === chipStr
                && oldChipData.str.replace(/\s/g, '') !== '')
                || (currSymbols
                    && JSON.stringify(currSymbols) === JSON.stringify(oldSymbols))) {
              return oldChipData.id;
            }
          }
        }
      }
      return null;
    },
    // Converts the symbols to a string
    // containing the symbols object, excluding their
    // unique id
    toSymbolStr(chipId) {
      // Return null if no chipId
      if (!chipId) return null;
      const chipData = this.getChip(chipId);

      // Return null if chipData is empty
      if (chipData) {
        const clonedSymbols = _.cloneDeep(chipData.symbolIds.map(this.getSymbol));
        clonedSymbols.forEach((s) => {
        // eslint-disable-next-line no-param-reassign
          delete s.id;
          // eslint-disable-next-line no-param-reassign
          delete s.ignoreUndo;
        });
        return JSON.stringify(clonedSymbols);
      }
      return null;
    },
    async showDuplicate(chipId, chipStr) {
      const duplicateChipId = await this.checkDuplicate(chipId, chipStr);

      // If you are dropping to the query bar, you need to reset
      // the state of any duplicates. In case no duplicates are
      // are found
      this.resetDuplicateObserver();

      // There was a duplicate found, so you set
      // the global duplicateChipId to the current chipId
      // and set the flag status of that chipId to tru
      if (duplicateChipId !== null) {
        this.duplicateChipId = duplicateChipId;
        // this.scrollToChip(duplicateChipId);
      }
      return duplicateChipId;
    },
    addSymbols(svgs) {
      return this.$store.dispatch(AT.ADD_SYMBOLS, svgs);
    },
    undo: _.throttle(function undo() {
      return new Promise((resolve) => {
        if (this.canUndo) {
          this.$store.commit(MT.APPLY_UNDO);
        }
        resolve();
      });
    }, 100),
    redo: _.throttle(function redo() {
      return new Promise((resolve) => {
        if (this.canRedo) {
          this.$store.commit(MT.APPLY_REDO);
        }
        resolve();
      });
    }, 100),
    clearStrokes(strokeIds) {
      return (this.canClearStrokes)
        ? this.$store.dispatch(AT.CLEAR_STROKES, strokeIds)
        : Promise.resolve();
    },
    clearCanvasChipSVGs() {
      return this.$store.dispatch('emptyCanvasSVGsOrder');
    },
    imageDrop(e) {
      if (!_.get(e, 'dataTransfer.files.length', 0) > 0) {
        // Only execute for file drops
        return undefined;
      }
      return this.recognitionRequest(
        handleDrop(e)
          .then(this.handleImageResults)
          .then(chips => chips
            .map(chipData => this.addChipToCanvas({
              ...chipData,
              x: e.clientX - SE.rect.x,
              y: e.clientY - SE.rect.y,
            }))),
      )
        .catch(console.error);
    },
    async handleImageResults(results) {
      if (!results) return Promise.reject(results);
      return Promise.all(results.map(
        // eslint-disable-next-line implicit-arrow-linebreak
        async (result) => {
          let chipData = result;
          if (result.isMathSeerImage) {
            if (result.isFresh) {
              // MathSeer image drop
              chipData = await this.makeFormulaData(result.str, true);
              // const chipId = await this.addChipToSearchBar(chipData);
              if (result.cardTitle !== '' || result.cardDescription !== '') {
                this.newCard(chipData.id, result.cardTitle, result.cardDescription);
              }
            }
          } else {
            // Image drop
            chipData = await this.makeFormulaData(result.latex, true);
            // this.addChipToSearchBar(chipData);
          }
          if (!chipData) return Promise.resolve();
          return Promise.resolve(chipData);
        },
      ));
    },
    async recognitionRequest(promise) {
      this.recognizingCounter += 1;
      try {
        return await promise;
      } catch (e) {
        // Intercept error to display message to user
        this.queueErrorMessage(e.message || 'Recognition request failed.');
        // then pass it along
        throw e;
      } finally {
        // Decrement counter on success or fail
        this.recognizingCounter -= 1;
      }
    },
    makeStale(chipId) {
      return this.editChip({
        ...this.getChip(chipId),
        isFresh: false,
      });
    },
    async dropSVGToCanvas(svgId) {
      const svgData = _.cloneDeep(this.getSVG(svgId));
      if (!svgData) return Promise.resolve([]);
      const { x, y } = svgData.attributes;
      svgData.attributes.x = 0;
      svgData.attributes.y = 0;
      await this.deleteSVGFromCanvas(svgId);
      this.makeStale(this.activeFormulaId);
      const svgs = await SvgOps.splitSvgObject(svgData);
      return Promise.all(svgs.map((svg) => {
        const positioned = SvgOps.translateOuter(svg, x, y);
        return this.addSVGToCanvas(positioned);
      }));
    },
    async dropChipSymbolsToCanvas(chipId) {
      const chipData = this.getChip(chipId);
      const symbolIds = _.cloneDeep(chipData.symbolIds || []);
      const symbols = _.cloneDeep(symbolIds.map(this.getSymbol));
      const bbox = SvgOps.getBBox(...symbols);
      // Resize symbols to match chip size
      const newHeight = chipData.height || 30;
      const resized = SvgOps.resizeSymbols(symbols, {
        x: chipData.x,
        y: chipData.y,
        width: newHeight * bbox.width / bbox.height,
        height: newHeight,
      });
      await this.deleteCanvasChip(chipId);
      await Promise.all(resized.map(s => this.setSymbol(s.id, s)));

      this.updateFormulaState('formula', {
        symbolIds: [
          ...(this.activeFormula.symbolIds || []),
          ...symbolIds,
        ],
        isFresh: false,
      });
    },
    async dropChipSVGToCanvas(chipId) {
      const chipData = this.getChip(chipId);
      const svgData = _.cloneDeep(this.getSVG(chipData.svgId));
      await this.deleteCanvasChip(chipId);
      this.makeStale(this.activeFormulaId);
      const svgs = await SvgOps.splitSvgObject(svgData);
      return Promise.all(svgs.map((svg) => {
        const positioned = SvgOps.translateOuter(svg, chipData.x, chipData.y);
        return this.addSVGToCanvas(positioned);
      }));
    },
    dropSelection() {
      const svgId = this.selectionId;
      return this.setSelection(null)
        .then(() => this.dropSVGToCanvas(svgId));
    },
    async recognizeSVG(svg) {
      const svgRequestData = await SvgOps.buildSVGRequest(svg);
      try {
        return await this.recognitionRequest(
          makeSVGRequest(svgRequestData),
        );
      } catch (e) {
        console.error(e);
        return null;
      }
    },
    async recognizeSVGIntoChip(svg) {
      return new Promise(async (resolve, reject) => {
        const response = await this.recognizeSVG(svg);
        if (!response) {
          reject();
        } else {
          const chipData = await this.makeFormulaData(response.latex);
          if (!chipData) {
            reject(Error('Failed to render LaTeX string'));
          } else {
            resolve(chipData);
          }
        }
      });
    },
    async recognizeSymbols(symbols) {
      const requestData = await SvgOps.buildSymbolsRequest(symbols);
      try {
        return await this.recognitionRequest(
          makeSVGRequest(requestData),
        );
      } catch (e) {
        console.error(e);
        return null;
      }
    },
    async recognizeSymbolsIntoChip(symbols) {
      return new Promise(async (resolve, reject) => {
        const response = await this.recognizeSymbols(symbols);
        if (!response) {
          reject();
        } else {
          // const recognizedSymbols = await SvgOps
          //   .getMathSVGSymbols(`[imath]${response.latex}[/imath]`);

          // const result = {
          //   str: response.latex,
          //   recognizedSymbols,
          // };
          const result = await this.makeFormulaData(response.latex, true);
          if (!result.symbols || result.symbols.length === 0) {
            reject(Error('Failed to render LaTeX string'));
          } else {
            resolve(result);
          }
        }
      });
    },
    createTemplateFromSVG(svg) {
      if (!svg) {
        return Promise.resolve();
      }
      const svgData = _.cloneDeep(svg);
      const { x, y } = svgData.attributes;

      // Create template from SVG data
      return this.addChipToCanvas({
        x,
        y,
        isTemplate: true,
        svgData: {
          ...svgData,
          attributes: {
            ...svgData.attributes,
            x: 0,
            y: 0,
          },
        },
      });
    },
    getStoreState() {
      const fieldsToSave = [
        'canvasChipsOrder',
        'searchBarChipsOrder',
        'favoriteChips',
        // 'duplicateChipId',
        'histories',
        'chipGroups',
        'cards',
        'chips',
        'myCards',
        'svgs',
        'strokeOrder',
        'strokes',
        'mode',
        'searchEngine',
        'dialogIsOpen',
        'activeFormulaId',
        'symbols',
        'selectedSymbols',
        'cardsExpanded',
      ];
      const state = {};
      fieldsToSave.forEach((field) => {
        state[field] = _.cloneDeep(this.$store.state[field]);
      });
      return state;
    },
    loadState(state) {
      this.$store.commit(MT.INITIALIZE_WITH_STATE, state);
    },
    downloadState() {
      const compressed = compress(this.getStoreState());
      saveState.downloadState(compressed);
    },
    async afterRecognition(data, strokesJSON) {
      // Add recognition result to chip in searchbar
      const chipData = await SvgOps.makeChipData(data.latex);
      if (!chipData) return Promise.reject(Error('Failed to render LaTeX string'));
      let { svgData } = chipData;
      const bbox = getStrokesBBox(strokesJSON);
      await this.addChipToSearchBar(chipData);
      svgData = await SvgOps.resize(svgData, bbox.width, bbox.height);
      svgData = await SvgOps.translateOuter(svgData, bbox.left, bbox.top);
      return this.addSVGToCanvas(svgData);
    },
    getSymbolData(latex, addTag = true) {
      const latexStr = addTag ? `[imath]${latex}[/imath]` : latex;
      return SvgOps.getMathSVGSymbols(latexStr);
    },
    async symbolsToChip(data, scaleForCanvas = false) {
      let symbols = null;

      if (data) {
        let scaled = null;
        if (scaleForCanvas) {
          scaled = await this.scaleForCanvas(data.symbols);
        }
        symbols = scaled ? scaled.symbols : data && data.symbols;
      }

      const mml = data && data.mml ? data.mml : null;
      return {
        str: '',
        symbols,
        mml,
        isFresh: false,
      };
    },
    async makeFormulaData(latex, scaleForCanvas = false) {
      let symbols = null;
      let data = null;
      const str = latex && latex.trim();
      if (str) {
        data = await this.getSymbolData(str);
        if (data && data.symbols) {
          let scaled = null;
          if (scaleForCanvas) {
            scaled = await this.scaleForCanvas(data.symbols);
          }
          symbols = scaled ? scaled.symbols : data && data.symbols;
          return {
            str: latex,
            symbols,
            mml: data.mml,
            tree: data.slt,
            isFresh: true,
          };
        }
      }
      return null;
    },
    async renderForCanvas(latex) {
      return this.makeFormulaData(latex, true);
    },
    // async afterNewRecognition(data, strokesJSON) {
    //   console.log(strokesJSON);
    //   const symbolData = await this.getSymbolData(data.latex);
    //   if (!symbolData) return null;
    //   const { symbols, mml } = symbolData;
    //   const symbolsInfo = SvgOps.getSymbolsInfo(symbols);
    // },
    async afterRecognitionIndividualSymbols(data, strokesJSON) {
      const recognizedSymbols = data.classificationResults.objects;
      // console.log(recognizedSymbols);
      const symbolBBoxes = _.merge({}, ...recognizedSymbols
        .map(s => (
          // convert to symbol + strokes form
          {
            symbol: s.symbol,
            strokesJSON: s.strokes
              .map(i => strokesJSON[parseInt(i, 10)]),
          }))
        .map((entry, i) => ({
          // convert to symbol + bbox form
          [entry.symbol]: {
            [i]: {
              symbol: entry.symbol,
              boundingRect: getStrokesBBox(entry.strokesJSON),
            },
          },
        })));

      const bbox = getStrokesBBox(strokesJSON);

      Object.keys(symbolBBoxes).forEach((key) => {
        // Convert symbol object format to array format
        const arr = Object.keys(symbolBBoxes[key])
          .map(i => symbolBBoxes[key][i]);
        symbolBBoxes[key] = arr;
      });

      const leftOf = (a, b) => a.x + a.width < b.x;
      const above = (a, b) => a.y < b.y;
      // const yMid = bbox => (bbox.y + bbox.height) / 2;
      const sortBBoxesLRTD = (a, b) => {
        if (leftOf(a, b)) {
          // { A } { B }
          return -1;
        } if (leftOf(b, a)) {
          // { B } { A }
          return 1;
        } // A and B overlap horizontally
        if (above(a, b)) {
          // A top above B top
          return -1;
        } // B top not as high as A top
        return 1;
      };
        // Add recognition result to chip in searchbar
      const symbolData = await this.getSymbolData(data.latex);
      if (!symbolData) return null;
      const { symbols, mml } = symbolData;
      // const chipData = await SvgOps.makeChipData(data.latex);
      // if (!chipData) return Promise.reject(Error('Failed to render LaTeX string'));
      // const { svgData } = chipData;
      // const svgObjects = await SvgOps.splitSvgObject(svgData);
      // let svgsInfo = await SvgOps.getSymbolsBBoxInfo(svgObjects, true);

      // const multiPartSymbols = multiPartSymbolStrings
      //   .map(s => s.split('').map(charToUnicode));

      // const multiParts = mergeMultiparts(
      //   multiPartSymbols,
      //   svgsInfo,
      //   svg => svg.symbol.unicodehex,
      // );

      // const multiPartSvgs = [];
      // await Promise.all(multiParts.map(async (mps) => {
      //   const str = mps.map(s => s.symbol.value).join('');
      //   const svgs = mps.map(s => s.svgObj);
      //   multiPartSvgs.push([
      //     `\\${str}`, await SvgOps.mergeSvgObjects(...svgs),
      //   ]);
      // }));
      // const multiPartSvgsInfo = multiPartSvgs
      //   .map(o => ({
      //     symbol: o[0],
      //     svgObj: o[1],
      //     boundingRect: SvgOps.getBBox(o[1]),
      //   }));
      const symbolsInfo = SvgOps.getSymbolsInfo(symbols);

      // svgsInfo = _.compact(await svgsInfo.reduce(async (rPromise, cur) => {
      //   const result = await rPromise;
      //   const prev = result.pop();
      //   if (prev && !prev.merged
      //   && prev.symbol.unicodehex === 8730 // ‎√
      //   && cur.symbol.unicodehex === 8722) { // -
      //     const merged = await SvgOps.mergeSvgObjects(prev.svgObj, cur.svgObj);
      //     return [...result, {
      //       merged: true,
      //       symbol: prev.symbol,
      //       svgObj: merged,
      //       boundingRect: SvgOps.getBBox(merged),
      //     }];
      //   }
      //   return [...result, prev, cur];
      // }, []));

      // // Add in multipart symbols
      // svgsInfo = svgsInfo.concat(multiPartSvgsInfo);

      const [symbolMatches, noMatches] = _.partition(symbolsInfo,
        info => symbolBBoxes[info.symbol.value]);

      const symbolsToAdd = await Promise.all(symbolMatches
        .map((info) => {
          const symbolMatch = symbolBBoxes[info.symbol.value].pop();
          if (symbolBBoxes[info.symbol.value].length === 0) {
            delete symbolBBoxes[info.symbol.value];
          }

          const [symbol] = SvgOps.resizeSymbols(
            [info.symbol.data],
            symbolMatch.boundingRect,
          );

          return symbol;
        // const svgObj = SvgOps.translateOuter(
        //   svg,
        //   -svg.attributes.x,
        //   -svg.attributes.y,
        // );
        // return SvgOps.resize(svgObj, width, height)
        //   .then(obj => SvgOps.translateOuter(obj, x, y))
        //   .then(this.addSVGToCanvas);
        }));

      const remainingSymbols = _.flatten(Object.keys(symbolBBoxes)
        .map(key => symbolBBoxes[key]))
        .sort((a, b) => sortBBoxesLRTD(a.boundingRect, b.boundingRect));

      const otherSymbolsToAdd = await Promise.all(noMatches
        .sort((a, b) => sortBBoxesLRTD(a.boundingRect, b.boundingRect))
        .map(async (info, i) => {
          const symbolBBox = remainingSymbols[i].boundingRect;
          const [symbol] = SvgOps.resizeSymbols(
            [info.symbol.data],
            symbolBBox,
          );

          return symbol;
        // let svgObj = SvgOps.translateOuter(
        //   svg,
        //   -svg.attributes.x,
        //   -svg.attributes.y,
        // );
        // svgObj = await SvgOps.resize(svgObj, symbolBBox.width, symbolBBox.height);
        // svgObj = await SvgOps.translateOuter(svgObj, symbolBBox.x, symbolBBox.y);
        // return this.addSVGToCanvas(svgObj);
        }));

      const allSymbolIds = await this.addToSymbols([
        ...symbolsToAdd,
        ...otherSymbolsToAdd,
      ]);

      return this.updateEditorState('formula', {
        str: !this.activeFormula.str
          ? data.latex
          : this.activeFormula.str,
        mml,
        symbolIds: [
          ...(this.activeFormula.symbolIds || []),
          ...allSymbolIds,
        ],
        bbox,
        isFresh: !this.activeFormula.str,
      });
    },
    symbolGetBox(symbolColor) {
      return this.symbolBoxMap[symbolColor] || Bbox.emptyBbox;
    },
    nodeGetBox(n) {
      return this.symbolGetBox(n.id);
    },
    treeSelect(start, end) {
      if (!this.canvasChipData) return [undefined, []];
      const tree = _.cloneDeep(this.canvasChipData.tree);
      if (tree) {
        const ptA = { x: start.x, y: start.y };
        const ptB = { x: end.x, y: end.y };
        return Selection.getSelection(ptA, ptB, 50, this.nodeGetBox, tree);
      }
      return [undefined, []];
    },
    async handleSelection(start, end) {
      const selection = this.treeSelect(start, end);
      const [selectedTree] = selection;
      const operation = OpManager.getOp();
      if (this.selectingOpArg && operation && selectedTree) {
        OpManager.setOp(null);
        this.selectingOpArg = false;
        const rootedTree = Node.rootSubtree(selectedTree);
        await this.performOperation(operation, rootedTree);
      } else {
        this.treeSelection = selection;
        OpManager.setOp(null);
        this.selectingOpArg = false;
      }
    },
    stringOfNode(n) {
      try {
        return Node.string_of_node(v => v, n);
      } catch {
        return '';
      }
    },
    editorStateUpdateCanvas(data) {
      this.canvasChipData = {
        ...this.canvasChipData,
        ...data,
      };
    },
    async updateEditorState(type, data) {
      const now = Date.now();
      // const last = this.editorStateUpdate;
      this.editorStateUpdate = now;
      const text = data;
      let chipData;
      const dataOrDefault = d => d || {
        symbols: [],
        symbolIds: [],
        tree: null,
        str: text,
        isFresh: false,
      };
      const updateCanvas = () => {
        this.texValid = !text || !!chipData;
        this.editorStateUpdateCanvas(dataOrDefault(chipData));
      };
      const updateChipFromText = () => {
        if (this.editorStateUpdate !== now) return;

        this.editChip({
          ...this.activeFormula,
          ...dataOrDefault(chipData),
          str: text,
          isFresh: !!text,
        });
      };
      const setChipData = (v) => {
        chipData = v;
        // activeFormula
        setTimeout(updateChipFromText, 0);
      };
      switch (type) {
        case 'text':
          // texString
          this.texString = text;
          // canvasChipData
          // if (now - last > 100) {
          //   setChipData(await this.renderForCanvas(text));
          //   updateCanvas();
          // } else {
          setTimeout(async () => {
            if (this.editorStateUpdate !== now) return;

            setChipData(await this.renderForCanvas(text));
            if (this.editorStateUpdate !== now) return;

            updateCanvas();
          }, 0);
          // }


          break;

        case 'formula':
          this.texString = data.str || '';
          setTimeout(() => {
            if (this.editorStateUpdate !== now) return;
            const canvasData = {
              ...data,
              symbols: data.symbolIds?.map(this.getSymbol) || [],
            };
            this.editorStateUpdateCanvas(canvasData);
          }, 0);
          setTimeout(() => {
            if (this.editorStateUpdate !== now) return;
            this.editChip({
              ...this.activeFormula,
              ...data,
            });
          }, 0);
          break;

        default:
          // texString
          this.texString = data.str;
      }
      this.treeSelection = null;
      OpManager.setOp(null);
      this.selectingOpArg = false;
    },
    filterBoxes(boxes) {
      return (boxes || []).filter(
        // filter out boxes @ origin (default if no symbol box)
        box => box.loc.x || box.loc.y,
      );
    },
    async performOperation(op, argument, withSelection) {
      const tree = _.cloneDeep(this.canvasChipData?.tree);
      if (tree) {
        const timer = setTimeout(() => {
          throw new Error('Operation timed out.');
        }, 5000);
        try {
          const [primary, floating] = op.op(argument, !!withSelection);
          clearTimeout(timer);

          const newString = this.stringOfNode(primary);
          if (floating) {
            const floatingWithBoxes = floating
              .map(n => [
                n,
                Bbox.boxesBox(this.filterBoxes(
                  Selection.getSubtreeBoxes(n, this.nodeGetBox),
                )),
              ]);

            Promise.all(
              floatingWithBoxes.map(
                ([fTree, box]) => this.makeFormulaData(this.stringOfNode(fTree))
                  .then(formulaData => ((formulaData)
                    ? this.addChipToCanvas({
                      ...formulaData,
                      tree: Node.rootSubtree(fTree),
                      x: box.loc.x,
                      y: box.loc.y,
                      height: box.size.height,
                    })
                    : null)),
              ),
            );
          }
          this.updateEditorState('text', newString);
        } catch (e) {
          console.error(e);
          console.log('Operation timed out.');
          if (timer) clearTimeout(timer);
        }
      }
    },
    async performOperationWithTex(operation, latex) {
      if (operation.name === 'Start formula') {
        return this.updateEditorState('text', latex);
      }
      const { slt } = await this.getSymbolData(latex);

      // if unable to get tree, fall back to dummy node
      const arg = slt || Node.make('arg', `{${latex}}`);
      // console.log(arg);
      return this.performOperation(operation, arg);
    },
    async performOperationWithChip(operation, chipId) {
      const chip = this.getChip(chipId);
      if (!chip) return null;

      return (chip.tree)
        ? this.performOperation(operation, chip.tree)
        : this.performOperationWithTex(operation, chip.str);
    },
    saveToUndoStack() {
      const currentFormula = this.activeFormula.str?.trim();

      if (currentFormula) {
        let texVersion = currentFormula;
        Object.keys(UnicodeLatexMap.unicodeToLatexMap).forEach((unicode) => {
          const index = texVersion.indexOf(unicode);
          if (index !== -1) {
            texVersion = texVersion.replaceAll(unicode, UnicodeLatexMap.unicodeToLatexMap[unicode]);
          }
        });

        if (texVersion !== currentFormula) return;
      }

      if (!Object.keys(this.newUndoStack).includes(this.activeFormulaId.toString())) {
        this.newUndoStack[this.activeFormulaId] = [currentFormula];
      } else {
        // Check for duplicates in Undo stack
        const chipStack = this.newUndoStack[this.activeFormulaId];
        const latestChip = chipStack[chipStack.length - 1];
        if (latestChip === currentFormula) {
          return;
        }

        // Check for duplicates in Redo stack
        const chipRedoStack = this.newRedoStack[this.activeFormulaId];
        if (chipRedoStack) {
          const latestRedoChip = chipRedoStack[chipRedoStack.length - 1];
          if (latestRedoChip === currentFormula) {
            return;
          }
        }

        const newStack = this.newUndoStack;
        newStack[this.activeFormulaId] = this.newUndoStack[this.activeFormulaId]
          .concat([currentFormula]);
        this.newUndoStack = newStack;
        // delete this.newUndoStrack.undefined;
      }
      this.newUndoStack = {};
      this.canRedo = false;
      this.canUndo = true;
      delete this.newUndoStack.undefined;
    },
    saveToRedoStack() {
      if (!Object.keys(this.newRedoStack).includes(this.activeFormulaId.toString())) {
        this.newRedoStack[this.activeFormulaId] = [this.activeFormula.str];
      } else {
        // Check for duplicates
        const chipStack = this.newRedoStack[this.activeFormulaId];
        const latestChip = chipStack[chipStack.length - 1];
        if (latestChip === this.activeFormula?.str) {
          return;
        }

        const newStack = this.newRedoStack;
        newStack[this.activeFormulaId] = this.newRedoStack[this.activeFormulaId]
          .concat([this.activeFormula.str]);
        this.newUndoStack = newStack;
      }
      this.canRedo = true;
      delete this.newRedoStack.undefined;
    },
    undoOperation() {
      if (this.recognizerWindowOpen && this.recognizedFormula === '') {
        const strokes = [...this.getStrokes()];
        strokes.pop();
        const strokeIds = [...this.strokeOrder];
        this.clearStrokes(strokeIds);

        strokes.forEach((stroke) => {
          this.addStroke({ width: stroke.width, d: stroke.d, stroke: stroke.stroke });
        });
      } else {
        this.saveToRedoStack();
        const chipStack = this.newUndoStack[this.activeFormulaId];
        let previousString = chipStack.pop();
        if (previousString === this.activeFormula.str.trim()) {
          previousString = chipStack.pop();
        }
        this.texString = previousString;
        this.updateEditorState('text', this.texString);
        this.canUndo = chipStack.length > 0;
      }
    },
    redoOperation() {
      const chipStack = this.newRedoStack[this.activeFormulaId];
      let previousString = chipStack.pop();
      if (previousString === this.activeFormula.str) {
        previousString = chipStack.pop();
      }
      this.texString = previousString;
      this.updateEditorState('text', this.texString);
      this.canRedo = chipStack.length > 0;
    },
  },
};

export default mixin;
