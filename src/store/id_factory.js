
// Generator for unique id generator
export default function GetIdFactory() {
  /**
   * When loading state from local storage,
   * idCounter may generate old ids so initialize with
   * number of ms elapsed since 1/1/1970
   * in the generated ids to preserve uniqueness
   *
   * Assumption: there will not be more ids generated
   * between save/load than there have been ms elapsed,
   * therefore the ids will never collide
   * (idCounter++ won't increment faster during use than Date.now(),
   * within the local storage save window (60 seconds)).
   * aka: there will be less than 60000 ids generated each min.
   */
  let idCounter = Date.now();
  return () => {
    idCounter += 1;
    return idCounter;
  };
}


export const groupIdOf = (id) => {
  const idStr = id?.toString() || '';
  const groupId = (idStr.length > 7)
    ? `group${id.toString().slice(-6, -2)}`
    : 'defaultGroup';
  return groupId;
};
