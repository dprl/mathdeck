import _ from 'lodash';
import Vue from 'vue';
import GetIdFactory from './id_factory';

const arrayAdd = (arr, values) => {
  values.forEach(value => arr.push(value));
};
const arrayDelete = (arr, values) => {
  // make a copy of values to delete in case values === arr
  const toDelete = [...values];
  toDelete.forEach((value) => {
    const index = arr.indexOf(value);
    if (index !== -1) arr.splice(index, 1);
  });
};

const ArrayOperations = [
  'get',
  'set',
  'addTo',
  'removeFrom',
  'empty',
];

const ObjectOperations = [
  'get',
  'set',
  'addTo',
  'removeFrom',
  'empty',
];


export default {
  casedNameFor(name) {
    return _.upperFirst(name);
  },
  xStringFor(x, name) {
    return `${x}${this.casedNameFor(name)}`;
  },
  getStringFor(name) {
    return this.xStringFor('get', name);
  },
  setStringFor(name) {
    return this.xStringFor('set', name);
  },
  createGetter(name) {
    return {
      [name]: state => state[name],
    };
  },
  getObjectOperationNames(name) {
    return ObjectOperations
      .map((op) => {
        let str = this.xStringFor(op, name);
        if (op in ['get', 'set']) {
          str = str.slice(0, -1);
        }
        return str;
      });
  },
  /**
   * Creates mutations to set the data values.
   * e.g. with name = 'formula': returns {
   *  setFormula(state, value) {...}
   * }
   *
   * @param {String} name
   * @param {Type} type
   */
  createMutations(nameValuePairs) {
    return _.merge({},
      ..._.map(nameValuePairs, (defaultValue, name) => {
        const setStr = this.setStringFor(name);
        const addToStr = this.xStringFor('addTo', name);
        const removeFromStr = this.xStringFor('removeFrom', name);
        const emptyStr = this.xStringFor('empty', name);
        // const popStr = this.xStringFor('pop', name);
        if (Array.isArray(defaultValue)) {
          return {
            [setStr](state, value) {
              // Vue for: state[name] = value;
              Vue.set(state, name, value);
            },
            [addToStr](state, values) {
              arrayAdd(state[name], values);
            },
            [removeFromStr](state, values) {
              arrayDelete(state[name], values);
            },
            [emptyStr](state) {
              Vue.set(state, name, []);
            },
            // [popStr](state, index) {
            //   state[name].splice(index, 1);
            // },
          };
        }
        if (defaultValue && typeof defaultValue === 'object') {
          return {
            [setStr](state, value) {
              // Object.assign(state[name][value.id], value);
              Vue.set(state[name], value.id, value);
            },
            [addToStr](state, values) {
              // Object.assign(state[name],
              //   Object.assign({}, ...values.map(v => ({ [v.id]: v }))));

              values.forEach((v) => { state[name][v.id] = v; });
              Vue.set(state, name, state[name]);
            },
            [removeFromStr](state, values) {
              values.forEach(key => delete state[name][key]);
              Vue.set(state, name, state[name]);
            },
            [emptyStr](state) {
              Vue.set(state, name, {});
            },
          };
        }
        return {
          [setStr](state, value) {
            Vue.set(state, name, value);
          },
        };
      }));
  },
  /**
   * Creates an action to set the data value.
   * Assumes the mutation is created.
   * e.g. with name = 'formula': returns {
   *  setFormula(context, value) {...}
   * }
   *
   * @param {String} name
   */
  createActions(nameValuePairs) {
    return _.merge({},
      ..._.map(nameValuePairs, (defaultValue, name) => {
        const setStr = this.setStringFor(name);

        if (Array.isArray(defaultValue)) {
          const arrayOperationNames = ArrayOperations
            .map(op => this.xStringFor(op, name));
          return _.merge({},
            ...arrayOperationNames.map(opName => ({
              [opName]:
                ({ commit }, ...args) => {
                  commit(opName, ...args);
                },
            })));
        }
        if (defaultValue && typeof defaultValue === 'object') {
          const idFactory = GetIdFactory();
          const objectOperationNames = this.getObjectOperationNames(name);
          const addToStr = this.xStringFor('addTo', name);
          return _.merge({},
            ...objectOperationNames.map(opName => ({
              [opName]:
                ({ commit }, ...args) => {
                  commit(opName, ...args);
                },
            })),
            {
              [addToStr]:
                ({ commit }, values) => {
                  const valuesWithIds = values.map(value => ({
                    ...value,
                    id: idFactory(),
                  }));
                  commit(addToStr, valuesWithIds);
                  return valuesWithIds.map(v => v.id);
                },
            });
        }
        return {
          [setStr]:
            ({ commit }, ...args) => {
              commit(setStr, ...args);
            },
        };
      }));
  },
  /**
   * Creates a get and set method for a collection.
   * Assumes the action is created.
   * e.g. with name = 'formula': returns {
   *  getFormula(id) {...},
   *  setFormula(id, value) {...}
   * }
   *
   * @param {String} name - data object name
   */
  createCollectionMethods(name) {
    const getStr = this.getStringFor(name)
      .slice(0, -1); // remove trailing s
    const setStr = this.setStringFor(name)
      .slice(0, -1); // remove trailing s
    const addToStr = this.xStringFor('addTo', name);
    const removeFromStr = this.xStringFor('removeFrom', name);
    return {
      [addToStr](values) {
        return this.$store.dispatch(addToStr, values);
      },
      [removeFromStr](values) {
        return this.$store.dispatch(removeFromStr, values);
      },
      [getStr](id) {
        return this.$store.state[name][id];
      },
      [setStr](id, value) {
        return this.$store.dispatch(setStr, {
          ...value,
          id,
        });
      },
    };
  },
  createGetterSetters(...names) {
    return _.merge({}, ..._.map(names, (name) => {
      const setStr = this.setStringFor(name);
      return {
        [name]: {
          get() {
            return this.$store.getters[name]
              || this.$store.state[name];
          },
          set(value) {
            if (value instanceof Object || this[name] !== value) {
              this.$store.dispatch(setStr, value);
            }
          },
        },
      };
    }));
  },
};
