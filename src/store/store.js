import Vue from 'vue';
import Vuex from 'vuex';

import mutations from './mutations';
import getters from './getters';
import actions from './actions';
import { initialState } from './state';

Vue.use(Vuex);

export const state = initialState();

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {},
  state,
  getters,
  mutations,
  actions,
});
