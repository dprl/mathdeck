import _ from 'lodash';
import Vue from 'vue';
import { SAVE_UNDO } from '@/store/mutation_types';

export const filterList = [
  'newUndoStack',
  'newRedoStack',
  'targetFormula',
  'undoStack',
  'redoStack',
  'recognizingCounter',
  'autoRecPercent',
  'histories',
  'errorMessages',
  'mode',
  'duplicateChipId',
  'activeHistory',
  'controlsActive',
  'entityActiveChip',
  'showSearch',
  'dialogIsOpen',
  'selectionActive',
  'activeChip',
  // 'activeFormulaId',
  'chipGroups',
  'cardsExpanded',
  // 'wikiCards',
  // 'focusedEntityType',
  // 'focusedCardId',
  'deckExpanded',
  'canvasWidth',
  'canvasHeight',
  'texCursorStart',
  'texCursorEnd',
  // 'texString',
  'texValid',
  'paletteSymbols',
  'selectingOpArg',
  'treeSelection',
];

function recordState(state) {
  const clone = _.cloneDeep(state);
  filterList.forEach(el => delete clone[el]);
  return clone;
}

export function difference(before, after) {
  // Compare the differences in object fields to find changes
  const beforeKeys = Object.keys(before);
  const afterKeys = Object.keys(after);
  const keys = _.union(beforeKeys, afterKeys);

  return _.merge({}, ...keys.map((key) => {
    // Compute basic necessary values
    const inBefore = beforeKeys.includes(key);
    const inAfter = afterKeys.includes(key);
    const beforeVal = before[key];
    const afterVal = after[key];

    if ((beforeVal && beforeVal.ignoreUndo)
      || (afterVal && afterVal.ignoreUndo)) {
      return undefined;
    }
    if (inAfter && inBefore) {
      if (!_.isEqual(beforeVal, afterVal)) {
        // Difference
        if (_.isObject(beforeVal) && _.isObject(afterVal)) {
        // Recurse to nested difference
          return { [key]: difference(beforeVal, afterVal) };
        }
        // Non-nested Update
        return { [key]: afterVal };
      }
      // Unchanged
      return undefined;
    }
    if (inBefore) { // implicit && !inAfter
      // Deletion
      return { [key]: undefined };
    }
    if (inAfter) { // implicit && !inBefore
      // Addition
      return { [key]: afterVal };
    }
    // This should be unreachable: key is in Union of aKeys and bKeys and
    // reaching this point corresponds to the key being in neither set
    return undefined;
  }));
}

/**
 * Route all undoable opeartions through the UndoManager
 * Operations will be grouped into a single undo unless there is a
 * lack of operations for the debounce time
 */
const UndoManager = {
  numProcessing: 0,
  finishing: null,
  before: null,
  finish: _.debounce(function finish({ commit, state }) {
    // If something else is currently processing, abort and
    // wait for it to finish the set
    if (this.numProcessing === 0) {
      // Note: assumes the check above and the
      // this.finishing promise assignment below are atomic
      // If another process begins in-between, it may not hit
      // the await this.finishing line after this.finishing is set
      // This could cause the recorded this.before state to be incorrect
      // including the subsequent diff computation

      // Set finishing promise after debounce; once set, all new
      // operations should wait for the promise to resolve before starting
      this.finishing = new Promise((resolve) => {
        const after = recordState(state);
        // Compute the reverse diff which undos the operation when applied
        const revDiff = difference(after, this.before);

        if (!_.isEmpty(revDiff)) {
        // Save the diff to undo the changes
          commit(SAVE_UNDO, revDiff);
        }

        // Reset the before state then resolve promise
        this.before = null;
        resolve();
      });
    }
  }, 500),

  async performOperation(context, op) {
    this.numProcessing += 1;
    // finish writing any previous 'finished' undo
    await this.finishing;

    // Only write to this.before if it is the first operation
    if (!this.before) {
      // Write starting state for first in set
      this.before = recordState(context.state);
    }

    // Actually perform the operation
    const result = await op();
    this.numProcessing -= 1;

    // If nothing is currently processing, attempt to finish the set
    // Otherwise let the last currently processing operation finish
    if (this.numProcessing === 0) {
      this.finish(context);
    }
    return result;
  },
};

export function undoableInProgress() {
  const v = !!UndoManager.before || UndoManager.numProcessing !== 0;
  if (v) { console.log(v); }
  return v;
}

/**
 * Make a function undo-able
 */
export function undoable(context, func) {
  return (...args) => UndoManager.performOperation(context, () => func(...args));
}

/**
 * Make an action undo-able
 */
export function undoableAct(action) {
  return (context, data) => undoable(context, action)(context, data);
}

export function applyDelta(state, delta) {
  // TODO: fix: array indicies/keys are 'change unstable'
  // e.g. deleting from an array at index i causes all keys: k > i
  // to become incorrect. Temporary fix is to reverse keys to delete
  // from the end of the array towards the front
  const keys = Object.keys(delta);

  keys.sort((a, b) => {
    // USE INTEGER SORTING IF POSSIBLE
    // otherwise it could mess up undo/redo w/ Array index deletions
    try {
      const intA = parseInt(a, 10);
      const intB = parseInt(b, 10);
      return intA - intB;
    } catch {
      if (a < b) return -1;
      if (a >= b) return 1;
      return 0;
    }
  }).reverse();

  keys.forEach((key) => {
    const sVal = state[key];
    const dVal = delta[key];
    if (!_.isEqual(sVal, dVal)) {
      if (_.isObject(delta[key]) && _.isObject(state[key])) {
        applyDelta(state[key], delta[key]);
      } else if (!_.isNil(dVal)) {
        // Check that it is not null or undefined since 0 is falsy
        Vue.set(state, key, dVal);
      } else {
        Vue.delete(state, key);
      }
    }
  });
}

function recordDiff(state, syncFunc) {
  const before = recordState(state);
  syncFunc(state);
  const after = recordState(state);

  // Compute the reverse diff which undos the operation when applied
  const revDiff = difference(after, before);

  return revDiff;
}

export function saveUndo(state, data) {
  // if (state.undoStack.length > 16) {
  //   state.undoStack.splice(0, 8);
  // }
  state.undoStack.push(data);
  // Clear the redo stack when a new undoable operation occurs
  Vue.set(state, 'redoStack', []);
}

export function applyUndo(state) {
  if (!(state.undoStack.length > 0)) {
    return;
  }
  const data = state.undoStack.pop();
  const diff = recordDiff(state, () => applyDelta(state, data));
  if (!_.isEmpty(diff)) {
    state.redoStack.push(diff);
  }
}

export function applyRedo(state) {
  if (!(state.redoStack.length > 0)) {
    return;
  }
  const data = state.redoStack.pop();
  const diff = recordDiff(state, () => applyDelta(state, data));
  if (!_.isEmpty(diff)) {
    state.undoStack.push(diff);
  }
}

export default {
  undoableAct,
  undoable,
  difference,
  undoableInProgress,
  filterList,
};
