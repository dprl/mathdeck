export const INITIALIZE_WITH_STATE = 'INITIALIZE_WITH_STATE';

export const SET_ACTIVE_FORMULA = 'SET_ACTIVE_FORMULA';
export const SET_ACTIVE_FORMULA_CANVAS_SVG_ID = 'SET_ACTIVE_FORMULA_CANVAS_SVG_ID';

export const EDIT_CHIP = 'EDIT_CHIP';
export const ADD_TO_CHIP_GROUPS = 'ADD_TO_CHIP_GROUPS';

export const ADD_SVGS = 'ADD_SVGS';
export const EDIT_SVG = 'EDIT_SVG';
export const DELETE_SVGS = 'DELETE_SVGS';


export const SET_FAVORITE = 'SET_FAVORITE';
export const ADD_TO_FAVORITE_CHIPS = 'ADD_TO_FAVORITE_CHIPS';
export const REMOVE_CHIP_OBSERVER = 'REMOVE_CHIP_OBSERVER';
export const REMOVE_FROM_FAVORITE_CHIPS = 'REMOVE_FROM_FAVORITE_CHIPS';

export const SAVE_UNDO = 'saveUndo';
export const APPLY_UNDO = 'applyUndo';
export const SAVE_REDO = 'saveRedo';
export const APPLY_REDO = 'applyRedo';

export const CREATE_STROKE = 'CREATE_STROKE';
export const ADD_STROKE = 'ADD_STROKE';
export const CLEAR_STROKES = 'CLEAR_STROKES';

// A Chip history is a collection of history chips ids
export const CREATE_CHIP_HISTORY = 'CREATE_CHIP_HISTORY';
export const CLEAR_CHIP_HISTORY = 'CLEAR_CHIP_HISTORY';

export const ADD_TO_CHIP_HISTORY = 'ADD_TO_CHIP_HISTORY';
export const DELETE_FROM_CHIP_HISTORY = 'DELETE_FROM_CHIP_HISTORY';
export const SET_MERGE_HISTORIES = 'SET_MERGE_HISTORIES';

export const QUEUE_ERROR_MESSAGE = 'QUEUE_ERROR_MESSAGE';
export const DEQUEUE_ERROR_MESSAGE = 'DEQUEUE_ERROR_MESSAGE';
