import Vue from 'vue';
import { initialState } from '@/store/state';
import * as mutation from './mutation_types';
import { applyUndo, applyRedo, saveUndo } from './undoable';
import Factory from './factory';

const isArray = (...args) => {
  // Verify each of the arguments provided is an Array
  args.forEach((arg) => {
    if (!Array.isArray(arg)) {
      throw new Error(`Invalid type of ${
        JSON.stringify(arg)}, expected Array`);
    }
  });
  return true;
};
const objectAdd = (obj, values) => {
  isArray(values);

  values.forEach((value) => {
    Vue.set(obj, value.id, value);
  });
};
const objectDelete = (obj, keys) => {
  isArray(keys);

  keys.forEach(key => Vue.delete(obj, key));
};
const arrayAdd = (arr, values) => {
  isArray(arr, values);

  values.forEach(value => arr.push(value));
};
const arrayDelete = (arr, values) => {
  isArray(arr, values);

  // make a copy of values to delete in case values === arr
  const toDelete = [...values];
  toDelete.forEach((value) => {
    const index = arr.indexOf(value);
    if (index !== -1) arr.splice(index, 1);
  });
};

// Utility function to create a state toggle function
// const toggle = name => state => Vue.set(state, name, !state[name]);
// Utility function to create a state setter function
// const set = name => (state, value) => Vue.set(state, name, value);

export default {
  [mutation.INITIALIZE_WITH_STATE](state, newState) {
    const initial = initialState();
    // Delete existing state
    // eslint-disable-next-line no-param-reassign
    [...Object.keys(state)].forEach(key => delete state[key]);
    // Initialize state
    Object.keys(initial).forEach(key => Vue.set(state, key, initial[key]));
    // Write newState
    Object.keys(newState).forEach(key => Vue.set(state, key, newState[key]));
  },
  [mutation.SAVE_UNDO]: saveUndo,
  [mutation.APPLY_UNDO]: applyUndo,
  [mutation.APPLY_REDO]: applyRedo,
  [mutation.CREATE_STROKE](state, { id, stroke }) {
    Vue.set(state.strokes, id, stroke);
  },
  [mutation.ADD_STROKE](state, id) {
    // Add the stroke to be rendered
    state.strokeOrder.push(id);
  },
  [mutation.CLEAR_STROKES](state, ids) {
    if (!ids) {
      // Clear the render strokes
      Vue.set(state, 'strokeOrder', []);
      Vue.set(state, 'strokes', {});
    } else {
      const filteredOrder = state.strokeOrder
        .filter(v => ids.indexOf(v) === -1);
      Vue.set(state, 'strokeOrder', filteredOrder);
      ids.forEach(id => Vue.delete(state.strokes, id));
    }
  },
  [mutation.EDIT_CHIP]: (state, data) => {
    // Updates chip with data
    if (data.id) {
      Vue.set(state.chips, data.id, data);
    }
  },
  addToChips(state, data) {
    // Object.assign(state.chips,
    //   Object.assign({}, ...data.map(d => ({ [d.id]: d }))));
    // Vue.set(state, 'chips', state.chips);

    // objectAdd(state.chips, data);
    state.chips = Object.assign(
      {},
      state.chips,
      ...data.map(c => ({ [c.id]: c })),
    );
  },
  removeFromChips(state, ids) {
    // ids.forEach(id => Vue.delete(state.chips, id));
    const keepIds = Object.keys(state.chips)
      .filter(key => ids.indexOf(key) === -1);
    state.chips = Object.assign(
      {},
      ...keepIds.map(id => ({ [id]: state.chips[id] })),
    );
    // ids.forEach(id => delete state.chips[id]);
    // Vue.set(state, 'chips', state.chips);

    // objectDelete(state.chips, ids);
  },
  [mutation.ADD_SVGS]: (state, svgs) => {
    // Stores a new svg with the provided id
    objectAdd(state.svgs, svgs);
  },
  [mutation.EDIT_SVG]: (state, data) => {
    // Updates svg with data
    if (data.id) {
      Vue.set(state.svgs, data.id, {
        ...state.svgs[data.id],
        ...data,
      });
    }
  },
  [mutation.DELETE_SVGS]: (state, ids) => {
    // Deletes the svgs with the provided ids
    objectDelete(state.svgs, ids);
  },
  [mutation.ADD_TO_FAVORITE_CHIPS]: (state, favoriteChipId) => {
    if (favoriteChipId) {
      arrayAdd(state.favoriteChips, [favoriteChipId]);
    }
  },
  [mutation.ADD_TO_CHIP_GROUPS]: (state, duplicateData) => {
    if (duplicateData) {
      const { chipGroups } = state;
      if (duplicateData.str in chipGroups) {
        // When editing, do not change the state of favorite
        if (!('favoriteId' in duplicateData)) return;
        Vue.set(state.chipGroups[duplicateData.str], 'favoriteId', duplicateData.favoriteId);
        Vue.set(state.chipGroups[duplicateData.str], 'newFavoriteId', duplicateData.newFavoriteId);
      } else {
        Vue.set(state.chipGroups, duplicateData.str, duplicateData);
      }
    }
  },
  [mutation.REMOVE_FROM_FAVORITE_CHIPS]: (state, favoriteChipId) => {
    // Only remove the chip if it is there
    if (favoriteChipId && (state.favoriteChips.includes(favoriteChipId))) {
      arrayDelete(state.favoriteChips, [favoriteChipId]);
    }
  },
  [mutation.SET_FAVORITE]: (state, { id, status }) => {
    Vue.set(state.chips[id], 'isFavorite', status);
  },
  [mutation.REMOVE_CHIP_OBSERVER]: (state, { id, observerId }) => {
    const chip = state.chips[id];
    if (chip && chip.observers) {
      arrayDelete(chip.observers, [observerId]);
    }
  },
  [mutation.CREATE_CHIP_HISTORY]: (state, { historyId }) => {
    Vue.set(state.histories, historyId, { history: [] });
  },
  [mutation.CLEAR_CHIP_HISTORY]: (state, historyId) => {
    Vue.set(state.histories, historyId, { history: [] });
  },
  [mutation.ADD_TO_CHIP_HISTORY]: (state, { historyId, historyChipId }) => {
    state.histories[historyId].history.unshift(historyChipId);
  },
  [mutation.DELETE_FROM_CHIP_HISTORY]: (state, { historyId, historyChipId }) => {
    arrayDelete(state.histories[historyId].history, [historyChipId]);
  },
  [mutation.QUEUE_ERROR_MESSAGE]: (state, message) => {
    arrayAdd(state.errorMessages, [message]);
  },
  [mutation.DEQUEUE_ERROR_MESSAGE]: (state) => {
    state.errorMessages.shift();
    Vue.set(state, 'errorMessages', state.errorMessages);
  },
  // CARDS mutations
  setCard(state, value) {
    Vue.set(state.cards, value.id, value);
  },
  addToCards(state, values) {
    state.cards = Object.assign(
      {},
      state.cards,
      ...values.map(v => ({ [v.id]: v })),
    );
  },
  removeFromCards(state, values) {
    const keepIds = Object.keys(state.cards)
      .filter(key => values.indexOf(key) === -1);
    state.cards = Object.assign(
      {},
      ...keepIds.map(id => ({ [id]: state.cards[id] })),
    );
  },
  emptyCards(state) {
    Vue.set(state, 'cards', {});
  },
  // SYMBOLS mutations
  setSymbol(state, value) {
    Vue.set(state.symbols, value.id, value);
  },
  addToSymbols(state, values) {
    state.symbols = Object.assign(
      {},
      state.symbols,
      ...values.map(v => ({ [v.id]: v })),
    );
  },
  removeFromSymbols(state, values) {
    const keepIds = Object.keys(state.symbols)
      .filter(key => values.indexOf(key) === -1);
    state.symbols = Object.assign(
      {},
      ...keepIds.map(id => ({ [id]: state.symbols[id] })),
    );
  },
  emptySymbols(state) {
    Vue.set(state, 'symbols', {});
  },
  addIndividualSymbolAlternatives(state, { symbol, alternatives }) {
    Vue.set(state.individualSymbolAlternatives, symbol, alternatives);
  },
  deleteIndividualSymbolAlternatives(state, symbol) {
    Vue.delete(state.individualSymbolAlternatives, symbol);
  },
  clearIndividualSymbolAlternatives(state) {
    Vue.set(state, 'individualSymbolAlternatives', {});
  },
  ...Factory.createMutations({
    facetedSearchMode: '',
    replacementOptions: [],
    recognizedFormula: '',
    alternateResults: [],
    recognitionSearchFilters: [],
    recognitionSearchOpen: false,
    canvasTransitionTrigger: false,
    showContext: false,
    insertionContext: '',
    individualSymbolAlternatives: {},
    activeRecognizedFormula: '',
    newUndoStack: {},
    newRedoStack: {},
    canUndo: false,
    canRedo: false,
    recognizerWindowOpen: false,
    activeTabs: [],
    recognizingCounter: 0,
    activeHistory: null,
    autoRecPercent: 100,
    mode: 'draw',
    controlsActive: false,
    entityActiveChip: null,
    showSearch: false,
    dialogIsOpen: false,
    selectionActive: false,
    activeChip: null,
    draggingChip: null,
    selection: null,
    treeSelection: null,
    selectingOpArg: false,
    searchEngine: 'Google',
    activeFormulaId: null,
    duplicateChipId: null,
    searchBarChipsOrder: [],
    favoriteChips: [],
    canvasChipsOrder: [],
    canvasSVGsOrder: [],
    wikiCards: [],
    myCards: [],
    selectedSymbols: [],
    cardsExpanded: true,
    focusedEntityType: null,
    focusedCardId: null,
    deckExpanded: 1,
    canvasWidth: 0,
    canvasHeight: 0,
    texCursorStart: -1,
    texCursorEnd: 0,
    texValid: true,
    texString: '',
    paletteSymbols: null,
    canvasChipData: null,
    editorStateUpdate: 0,
    targetFormula: null,
    editingMode: 'LaTeX + Visual',
    editingPanelExpanded: true,
    wikicardsAutoUpdate: true,
  }),
};
