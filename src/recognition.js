import _ from 'lodash';
import config from '@/client-config';

const $ = require('jquery');

// Convert stroke into expected recognition request format
function strokeToRecognitionReq(stroke, id) {
  const x = index => stroke.x[index];
  const y = index => stroke.y[index];

  // Safety check: at least length elements in x and y
  const length = Math.min(stroke.x.length, stroke.y.length);

  const ptsStr = _.range(length) // [0:length)
    .map(i => `${x(i)},${y(i)}`) // point format
    .join('|'); // point separator

  return [{
    type: 'pen_stroke',
    instanceID: id,
    points: ptsStr,
  }];
}

function buildRequest(strokes) {
  return {
    primitives: strokes.map(({ stroke }, id) => strokeToRecognitionReq(stroke, id)),
  };
}

function req(url, reqData, success, fail, starting) {
  console.log('Attempting recognition request');
  if (starting) starting();
  if (reqData.primitives.length) {
    $.post(url, JSON.stringify(reqData), success, 'json')
      .fail(fail);
  } else {
    fail('No primitives to recognize');
  }
}

export function cleanResult(data) {
  const result = data;
  // unwrap latex string since it is currently wrapped in escaped parens
  // e.g. `\(${latex}\)` => `${latex}`
  result.latex = result.latex
    .replace(/\r?\n */g, '') // remove newlines
    .substring(2, result.latex.length - 3);

  // there shouldn't be spaces around = in the 'xml:id = "1000: "' attributes
  result.mathml = result.mathml.replace(/xml:id = "/g, 'xml:id="')
    .replace(/ +/g, ' ') // remove excess spaces
    .replace(/ >/g, '>') // remove excess spaces
    .replace(/\r?\n */g, '') // remove newlines
    .replace(/\\infty</g, '&infin;<') // map infinity for mathml
    .replace(/\\ldots/g, '&hellip;') // map ldots for mathml
    .replace(/\\(sin|cos|tan|lim|log)</g, '$1<') // map sin, cos, tan, lim, log
    .replace(/\\(.*?)</g, '&$1;<'); // attempt to map latex name to mathml name
  return result;
}

export const debounceTime = 2000;
// const debouncedReq = _.debounce(
//   req, debounceTime, { leading: false, trailing: true },
// );

function doRequest(reqData, starting) {
  const url = config.handwritingURL;

  return new Promise((resolve, reject) => {
    function fail() {
      reject(Error('Recognition request failed.'));
    }
    function success(data) {
      if (data.error) {
        fail();
      } else {
        const result = cleanResult(data);

        const fracs = result.mathml.match(/mfrac +xml:id="(\d+:?)+"/g);
        const fracStrokes = fracs
          ? _.flatten(fracs.map(s => s.match(/\d+/g)))
          : [];

        result.classificationResults.objects
          .forEach((o) => {
            if (o.symbol === 'COMMA') {
            // map all 'COMMA' objects to ,
            // eslint-disable-next-line no-param-reassign
              o.symbol = ',';
            } else if (o.symbol === '-' && o.strokes
              .every(stroke => fracStrokes.indexOf(stroke) === -1)) {
            // map all '-' objects which are not fracs to \\minus
            // eslint-disable-next-line no-param-reassign
              o.symbol = '\\minus';
            }
          });

        // console.log(result);
        resolve(result);
      }
    }
    // Disable debounce for now
    // debouncedReq(url, reqData, success, fail, starting);
    req(url, reqData, success, fail, starting);
  });
}

function recognize(strokes, starting) {
  return doRequest(buildRequest(strokes), starting);
}

export async function makeSVGRequest(reqData) {
  return new Promise((resolve, reject) => {
    const url = config.svgURL;
    const failure = () => {
      reject(Error('Recognition failed'));
    };
    const success = (response) => {
      // console.log(response);
      if (response.error) {
        failure();
      } else {
        resolve(cleanResult(response));
      }
    };
    $.post(url, JSON.stringify(reqData), success, 'json')
      .fail(failure);
  });
}

export default {
  recognize,
  debounceTime,
  cleanResult,
};
