import Vue from 'vue';
import Pep from 'pepjs';
import 'drag-drop-touch';
import VueClosable from 'vue-closable';
import App from './App.vue';
import store from './store/store';
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css';

Vue.use(Pep);
Vue.use(VueClosable);

Vue.config.productionTip = false;

new Vue({
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
