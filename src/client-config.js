export default {
  handwritingURL: 'https://mathdeck.cs.rit.edu/recognize-handwriting',
  recognizerURL: 'https://northbay.cs.rit.edu/qdgga/predict/',
  imageURL: 'https://northbay.cs.rit.edu/qdgga/predict/',
  pdfURL: 'https://northbay.cs.rit.edu/qdgga/predict/',
  svgURL: 'https://mathdeck.cs.rit.edu/recognize-structure',
  // Use devSession to avoid deletion of non-dev sessions
  // dev-relase branch => use devSession
  saveStateURL: 'https://mathdeck.cs.rit.edu/devSession',
  entityCards: 'https://mathdeck.cs.rit.edu/devEntityCards',
  entityCardsText: 'https://mathdeck.cs.rit.edu/entityCardsName',
  remoteSave: true,
};
