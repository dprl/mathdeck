import piexif from 'piexifjs';
import makeImageRequest from '@/image_recognition';

export function uploadImageFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = async function onload(e) {
      const encodedImage = e.target.result;
      const prefixKeyEnd = 'base64,';
      const prefixEnd = encodedImage.indexOf(prefixKeyEnd) + prefixKeyEnd.length;
      const slicedEncodedImage = encodedImage.slice(prefixEnd, encodedImage.length);
      const prefix = encodedImage.slice(0, prefixEnd);
      const contentType = encodedImage.slice('data:'.length, encodedImage.indexOf(';base64'));
      const isPDF = (prefix === 'data:application/pdf;base64,');
      const exifObj = contentType === 'image/jpeg' ? piexif.load(encodedImage) : {};

      if ('0th' in exifObj && Object.prototype.hasOwnProperty.call(exifObj['0th'], 270)) {
        resolve(
          JSON.parse(exifObj['0th'][270]),
        );

        return;
      }

      makeImageRequest(slicedEncodedImage, isPDF)
        .then(resolve)
        .catch(reject);
    };
    reader.readAsDataURL(file);
  });
}

export function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}

function handleFiles(files) {
  return Promise.all(([...files]).map(uploadImageFile));
}


export function handleDrop(e) {
  preventDefaults(e);
  const dt = e.dataTransfer;
  const { files } = dt;

  if (files) {
    return handleFiles(files);
  }
  return Promise.all([]);
}

export default {
  uploadImageFile,
  preventDefaults,
  handleDrop,
};
