
import _MathML from '@/../vfs/src/MathML.bs.js';
import _Node from '@/../vfs/src/Node.bs.js';
import _Bbox from '@/../vfs/src/Bbox.bs.js';
import _Controls from '@/../vfs/src/Controls.bs.js';
import _Selection from '@/../vfs/src/Selection.bs.js';

export const MathML = _MathML;
export const Node = _Node;
export const Bbox = _Bbox;
export const Controls = _Controls;
export const Selection = _Selection;

let awaitingOp = null;
const callbacks = [];
export const OpManager = {
  getOp() {
    return awaitingOp;
  },
  setOp(v) {
    awaitingOp = v;
    try {
      callbacks.map(cb => cb(awaitingOp));
    } catch (e) {
      console.log('OpMangager subscriber callback failed');
      console.warn(e);
    }
  },
  subscribe(cb) {
    callbacks.push(cb);
  },
};

export default {
  MathML: _MathML,
  Node: _Node,
  Bbox: _Bbox,
  Controls: _Controls,
  Selection: _Selection,
};
