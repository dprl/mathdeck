export function saveJSON(jsonObject, filename = 'jsonData.json') {
  const a = document.createElement('a');
  const jsonStr = JSON.stringify(jsonObject, null, 2); // indent 2 spaces
  const file = new Blob([jsonStr], { type: 'text/plain' });
  a.href = URL.createObjectURL(file);
  a.download = filename;
  a.click();
}

export default saveJSON;
