
import config from '@/client-config';
import { SvgOps } from '@/svg-operations';

const $ = require('jquery');

function isNonEmptyArray(value) {
  return value
    && Array.isArray(value)
    && value.length > 0;
}

export async function parseEntityResponse(result, textsearch = false) {
  const entityIds = [];
  const unique = result.filter((e) => {
    if (entityIds.indexOf(e.id) === -1) {
      entityIds.push(e.id);
      return true;
    }
    return false;
  });
    // this.entities = unique;
  return Promise.all(unique.map(async (cardData) => {
    const formula = cardData.formula[0];
    let { symbols } = formula;
    if (symbols && typeof symbols === 'string') {
      try {
        const parsed = JSON.parse(symbols);
        if (isNonEmptyArray(parsed)) {
          symbols = parsed;
        }
      } catch (e) {
        console.log('Failed to parse pre-rendered symbols');
        return null;
      }
    } else {
      const symbolsObject = await SvgOps.getMathSVGSymbols(`[imath]${formula.latex}[/imath]`, 'black');
      // eslint-disable-next-line prefer-destructuring
      symbols = symbolsObject.symbols;
    }
    const desc = {
      description: null,
      sourceLink: null,
      sourceName: null,
    };
    if (isNonEmptyArray(cardData.descriptions)) {
      const d = cardData.descriptions[0];
      desc.description = d.description;
      desc.sourceLink = d.source_link;
      desc.sourceName = d.source_name;
    }
    return {
      title: cardData.title,
      confidence: textsearch ? 100 : cardData.confidence,
      ...desc,
      formula: {
        str: formula.latex,
        symbols,
      },
    };
  })).then(cards => cards.filter(card => card));
}

export async function fetchEntityCards(latex) {
  if (!navigator.onLine) return null;
  try {
    const result = await new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        url: config.entityCards,
        data: JSON.stringify({
          formula: latex,
        }),
        success: resolve,
        error: reject,
        contentType: 'application/json',
      });
    });

    if (result) {
      const cards = parseEntityResponse(result);
      return cards;
    }
  } catch {
    return null;
  }
  return null;
}

export async function fetchEntityCardsWithText(text) {
  if (!navigator.onLine) return null;
  try {
    const result = await new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        url: config.entityCardsText,
        data: JSON.stringify({
          textQuery: text,
        }),
        success: resolve,
        error: reject,
        contentType: 'application/json',
      });
    });

    if (result) {
      const cards = parseEntityResponse(result, true);
      return cards;
    }
  } catch {
    return null;
  }
  return null;
}

export default {
  fetchEntityCards,
  fetchEntityCardsWithText,
  parseEntityResponse,
};
