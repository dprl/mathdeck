// import { cleanResult } from '@/recognition';
import config from '@/client-config';

const $ = require('jquery');

function buildRequest(image) {
  // Format Request Time
  const now = new Date();
  now.setMilliseconds(0);
  const isoNow = new Date(now.getTime() - now.getTimezoneOffset() * 60000).toISOString();
  return {
    request_time: isoNow.replace(/T/, ' ').replace(/.000Z/, ''),
    file_id: 'mathdeck-image-request',
    input_type: 'image',
    input_image: {
      base64_img: image,
      dpi: 600,
    },
    annotation: 'string',
  };
}

function cleanResult(data) {
  const result = data;
  result.latex = result.latex.substring(2, result.latex.length - 2);
  return result;
}

export default function makeImageRequest(encodedImage, isPDF = false) {
  return new Promise((resolve, reject) => {
    const url = isPDF ? config.pdfURL : config.imageURL;
    const requestData = buildRequest(encodedImage);
    const failure = () => {
      reject(Error('Recognition failed'));
    };
    const success = (response) => {
      if (response.error) {
        failure();
      } else {
        resolve(cleanResult(response));
      }
    };
    $.post(url, JSON.stringify(requestData), success, 'json')
      .fail(failure);
  });
}
