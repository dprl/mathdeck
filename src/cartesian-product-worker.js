function cartesianProduct(...arrays) {
  return [...arrays].reduce((a, b) => a.map(x => b.map(y => x.concat(y)))
    .reduce((c, d) => c.concat(d), []), [[]]);
}

onmessage = (e) => {
  const arrays = JSON.parse(e.data);
  const combinations = cartesianProduct(...arrays);
  postMessage(combinations);
};
