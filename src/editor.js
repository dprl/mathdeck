import _ from 'lodash';

const $ = require('jquery');

export const ScriptEditor = {
// A collection of useful functions related to the script editor
  BCRCache: null,

  get id() {
    return 'script-editor';
  },
  get selector() {
    return `#${this.id}`;
  },
  get element() {
    return $(this.selector)[0];
  },
  getRect: _.throttle(function rect() {
    // Update the cached value
    this.BCRCache = this.element?.getBoundingClientRect();
    return this.BCRCache;
  }, 3000),
  get rect() {
    return this.getRect();
  },
  get selection() {
    return $('#selected-symbols')[0];
  },
  get selectionRect() {
    return this.selection.getBoundingClientRect();
  },
  get svgContainer() {
    return $('#svg-containter')[0];
  },
  get selectedElements() {
    return $('.selected').get();
  },
  get chipPadding() {
    return {
      left: 19,
      top: 11,
      innerLeft: 15,
      innerTop: 7,
    };
  },
  get templateChipPadding() {
    return {
      left: 17,
      top: 9,
      innerLeft: 13,
      innerTop: 5,
    };
  },
  get selectionChipPadding() {
    return {
      left: 17,
      top: 0,
      innerLeft: 13,
      innerTop: 5,
    };
  },
  getChipSVG(chipId) {
    return $(`#chip-id-${chipId} svg`)[0];
  },
};

export const EditorModes = {
  draw: 'draw',
  select: 'select',
  text: 'text',
};

export default {
  ScriptEditor,
  EditorModes,
};
