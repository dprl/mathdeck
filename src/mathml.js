import { MathML } from './vst';


export default {
  fromElement(mathEl) {
    /**
     * Constructs an object from a svg element
     */
    // Create the object structure
    const object = {
      tagName: mathEl.tagName,
      attributes: {},
    };
    // Add all element attributes as object attributes
    const attrs = mathEl.attributes;
    for (let i = 0; i < attrs.length; i += 1) {
      const key = attrs[i].name;
      const { value } = attrs[i];
      object.attributes[key] = value;
    }
    // If leaf record textcontent as value
    if (mathEl.children.length === 0) {
      object.value = mathEl.textContent;
    }

    // Recursively construct and add children to object
    object.children = [];
    const childEls = mathEl.children;
    for (let i = 0; i < childEls.length; i += 1) {
      object.children.push(this.fromElement(childEls[i]));
    }
    return object;
  },
  toSLT(mathEl) {
    const obj = this.fromElement(mathEl);

    // console.log(obj);
    return MathML.buildTree(obj);
  },
  idFromColor(cStr) {
    const i = cStr.indexOf('rgb(');
    if (i !== -1) {
      return cStr.slice(i + 4, -1);
    }
    return cStr;
  },
  // objectToSLT(math, tRef) {
  //   let id = this.idFromColor(math.attributes.mathcolor);

  //   const up = (f, args) => {
  //     // eslint-disable-next-line no-param-reassign
  //     tRef.t = f(...args, tRef.t);
  //   };
  //   // Leaf
  //   if (!math.children.length) {
  //     const val = {
  //       type: math.tagName,
  //       value: math.value,
  //       color: math.attributes.mathcolor,
  //     };
  //     up(Tree.makeNode, [id, val]);
  //     return id;
  //   }
  //   const ids = math.children
  //     .map(c => this.objectToSLT(c, tRef));

  //   const sltType = math.tagName;

  //   const cVal = v => ({
  //     type: sltType,
  //     value: v,
  //     color: math.attributes.mathcolor,
  //   });

  //   if (['math', 'mrow'].indexOf(sltType) !== -1) {
  //     if (sltType === 'mrow' && ids.length !== 1) {
  //       up(Tree.makeNode, [id, cVal('')]); // container
  //       up(Tree.insert, [id, Tree.relations.contains, ids[0]]);
  //     } else {
  //       [id] = ids;
  //     }
  //     for (let i = 1; i < ids.length; i += 1) {
  //       up(Tree.insert, [ids[i - 1], Tree.relations.right, ids[i]]);
  //     }
  //   } else if (sltType === 'msupsub'
  //     && ids.length >= 3) {
  //     up(Tree.makeNode, [id, cVal('')]); // container
  //     up(Tree.insert, [id, Tree.relations.contains, ids[0]]);
  //     up(Tree.insert, [ids[0], Tree.relations.super, ids[1]]);
  //     up(Tree.insert, [ids[0], Tree.relations.sub, ids[2]]);
  //   } else if (sltType === 'msubsup'
  //     && ids.length >= 3) {
  //     up(Tree.makeNode, [id, cVal('')]); // container
  //     up(Tree.insert, [id, Tree.relations.contains, ids[0]]);
  //     up(Tree.insert, [ids[0], Tree.relations.sub, ids[1]]);
  //     up(Tree.insert, [ids[0], Tree.relations.super, ids[2]]);
  //   } else if (sltType === 'msup'
  //     && ids.length >= 2) {
  //     up(Tree.makeNode, [id, cVal('')]); // container
  //     up(Tree.insert, [id, Tree.relations.contains, ids[0]]);
  //     up(Tree.insert, [ids[0], Tree.relations.super, ids[1]]);
  //   } else if (sltType === 'msub'
  //     && ids.length >= 2) {
  //     up(Tree.makeNode, [id, cVal('')]); // container
  //     up(Tree.insert, [id, Tree.relations.contains, ids[0]]);
  //     up(Tree.insert, [ids[0], Tree.relations.sub, ids[1]]);
  //   } else if (sltType === 'mover'
  //     && ids.length >= 2) {
  //     up(Tree.makeNode, [id, cVal('\\overset')]); // container
  //     up(Tree.relate, [id, Tree.relations.contains, ids[1]]);
  //     up(Tree.insert, [ids[0], Tree.relations.above, id]);
  //     [id] = ids;
  //   } else if (sltType === 'munder'
  //     && ids.length >= 2) {
  //     up(Tree.makeNode, [id, cVal('\\underset')]); // container
  //     up(Tree.relate, [id, Tree.relations.contains, ids[1]]);
  //     up(Tree.insert, [ids[0], Tree.relations.below, id]);
  //     [id] = ids;
  //   }

  //   return id;
  // },

  // stringOfSLT({ t }) {
  //   return Tree.toString(x => x.value, t);
  // },
};
