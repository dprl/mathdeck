
export function findMultipart(ucArr, getUC) {
  return (el, index, arr) => el
    && arr.length >= index + ucArr.length
    && ucArr.reduce(
      (last, cur, curInd) => last
        && cur === getUC(arr[index + curInd]),
      true,
    );
}

export function mergeMultiparts(
  multiPartCandidates,
  sequence,
  getValue,
) {
  const multiParts = [];
  multiPartCandidates.forEach((sym) => {
    let loc;
    // eslint-disable-next-line no-cond-assign
    while (
      (
        loc = sequence.findIndex(findMultipart(sym, getValue))
      ) !== -1
    ) {
      multiParts.push(sequence.splice(loc, sym.length));
    }
  });
  return multiParts;
}
