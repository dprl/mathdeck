import _ from 'lodash';
import {
  experimentURL,
} from './experiment-config';

const eventAPIURL = `${experimentURL}/event/log`;
// const getKeyURL = 'http://localhost:5000/get-id';

// let key;
// setTimeout(() => {
//   fetch(getKeyURL)
//     .then(async (response) => {
//       if (response) {
//         key = await response.text();
//         console.log(key);
//       }
//     })
//     .catch(console.warn);
// }, 0);

export default {
  queueA: [],
  queueB: [],
  liveIsA: true,
  get queue() {
    if (this.liveIsA) {
      return this.queueA;
    }
    return this.queueB;
  },
  pushEvent(data) {
    if (this.liveIsA) {
      this.queueA.push(data);
    } else {
      this.queueB.push(data);
    }
  },
  flushQueue() {
    const { queue } = this;
    this.liveIsA = !this.liveIsA;
    if (this.liveIsA) {
      this.queueB = [];
    } else {
      this.queueA = [];
    }
    return queue;
  },
  logNow() {
    this.debounceFlush.flush();
  },
  debounceFlush: _.debounce(function debounceFlush() {
    const { experimentInfo } = window;
    if (!experimentInfo || experimentInfo.editingMode === 'Practice') return;
    const queue = this.flushQueue();
    const data = {
      participantId: experimentInfo.participantId,
      taskId: experimentInfo.taskId,
      events: queue,
    };
    try {
      const url = new URL(eventAPIURL);
      // console.log(url);
      // url.pathname = experimentInfo.participantId + url.pathname;
      // const params = new URLSearchParams();
      // params.append('token', key);
      // url.search = params.toString();
      // console.log(url);
      fetch(url.toString(), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
    } catch (err) {
      console.error(err);
      if (this.liveIsA) {
        this.queueB = queue;
      } else {
        this.queueA = queue;
      }
    }
  }, 100),
  log(...args) {
    console.log(...args);
  },
  makeEvent(type, data) {
    return {
      time: Date.now(),
      type,
      data,
    };
  },
  logEvent(type, data) {
    this.pushEvent(
      this.makeEvent(type, data),
    );
    this.debounceFlush();
    // console.log(type);
  },
};
