export default {
  items: [
    {
      title: 'Pythagorean Theorem',
      formula: 'a^2 + b^2 = c^2',
      descriptions: [
        {
          text: 'desc 1',
          sourceName: 'Wiki A',
          sourceURL: 'http://localhost',
        },
        {
          text: 'desc 2',
          sourceName: 'Wiki B',
          sourceURL: 'http://localhost',
        },
      ],
      related: [
        {
          sourceName: 'Related A',
          sourceURL: 'http://localhost',
        },
        {
          sourceName: 'Related B',
          sourceURL: 'http://localhost',
        },
      ],
    },
    {
      title: 'Theorem B',
      formula: 'x^2 + y^2 = z^2',
      descriptions: [
        {
          text: 'desc 1',
          sourceName: 'Wiki A',
          sourceURL: 'http://localhost',
        },
        {
          text: 'desc 2',
          sourceName: 'Wiki B',
          sourceURL: 'http://localhost',
        },
      ],
      related: [
        {
          sourceName: 'Related A',
          sourceURL: 'http://localhost',
        },
        {
          sourceName: 'Related B',
          sourceURL: 'http://localhost',
        },
      ],
    },
    {
      title: 'Theorem C',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem D',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem E',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem F',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem G',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem H',
      formula: 'x^2 + y^2 = z^2',
    },
    {
      title: 'Theorem I',
      formula: 'x^y',
    },
  ],
};
