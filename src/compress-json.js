import _ from 'lodash';

export function buildMap(obj, counts = new Map()) {
  const isObject = _.isPlainObject(obj);
  Object.keys(obj).forEach((key) => {
    if (isObject) {
      // Only record keys for Objects
      if (counts.has(key)) {
        // eslint-disable-next-line no-param-reassign
        counts.get(key).counter += 1;
      } else {
        // eslint-disable-next-line no-param-reassign
        counts.set(key, {
          value: key,
          counter: 1,
        });
      }
    }
    const val = obj[key];
    if (_.isObjectLike(val)) {
      // Recurse if object or array
      buildMap(val, counts);
    } else if (counts.has(val)) {
      // eslint-disable-next-line no-param-reassign
      counts.get(val).counter += 1;
    } else {
      // eslint-disable-next-line no-param-reassign
      counts.set(val, {
        value: val,
        counter: 1,
      });
    }
  });
}


export const Encoder = {
  getEncoding(counts) {
    const keyCodes = Array.from(counts.keys())
    // sort by expected space usage
      .sort((a, b) => (
        (JSON.stringify(b).length * counts.get(b).counter)
        - (JSON.stringify(a).length * counts.get(a).counter)
      ))
      .map(key => counts.get(key).value);
    return keyCodes;
  },

  buildKeyMap(keyCodes, invert = false) {
    const map = new Map();
    keyCodes.forEach((key, i) => (
      invert
        ? map.set(i, key)
        : map.set(key, i)
    ));
    return map;
  },

  encodeArray(arr, keyMap, fromString = false) {
    return arr.map((val) => {
      if (_.isPlainObject(val)) {
        return Encoder.encodeObject(val, keyMap, fromString);
      }
      if (_.isArray(val)) {
        return Encoder.encodeArray(val, keyMap, fromString);
      }
      return keyMap.get(val);
    });
  },

  encodeObject(obj, keyMap, fromString = false) {
    const encoded = {};
    Object.keys(obj).forEach((keyStr) => {
      const value = obj[keyStr];
      let key = '';
      if (fromString) {
        try {
          key = JSON.parse(keyStr);
        } catch {
          // failed to parse as JSON
          key = keyStr;
        }
      } else {
        key = keyStr;
      }
      if (_.isPlainObject(value)) {
        encoded[keyMap.get(key)] = Encoder.encodeObject(value, keyMap, fromString);
      } else if (_.isArray(value)) {
        encoded[keyMap.get(key)] = Encoder.encodeArray(value, keyMap, fromString);
      } else {
        encoded[keyMap.get(key)] = keyMap.get(value);
      }
    });
    return encoded;
  },

  decode(obj) {
    if (!obj && typeof obj === 'object') return undefined;
    const toDecode = { ...obj };
    const { keyCodes } = toDecode;
    if (!keyCodes) return undefined;
    delete toDecode.keyCodes;
    const decodeKeyMap = Encoder.buildKeyMap(keyCodes, true);
    return Encoder.encodeObject(toDecode, decodeKeyMap, true);
  },
};

export function compress(obj) {
  try {
    const counts = new Map();
    buildMap(obj, counts);
    const keyCodes = Encoder.getEncoding(counts);
    const keyMap = Encoder.buildKeyMap(keyCodes);
    const compressed = Encoder.encodeObject(obj, keyMap);
    compressed.keyCodes = keyCodes;

    // const sizeBefore = JSON.stringify(obj).length;
    // const sizeAfter = JSON.stringify(compressed).length;
    // const spaceSavings = 1 - sizeAfter / sizeBefore;
    // console.log(`Space saved: ${100 * spaceSavings}%`);

    // // Test decoding
    // const decoded = Encoder.decode(JSON.parse(JSON.stringify(compressed)));
    // const sizeDecoded = JSON.stringify(decoded).length;
    // console.log(sizeBefore);
    // console.log(sizeAfter);
    // console.log(sizeDecoded);
    // console.log(`Obj isDeepEqual with decoded?: ${_.isEqual(obj, decoded)}`);

    return compressed;
  } catch (e) {
    console.error(e);
  }
  return undefined;
}

export function decompress(obj) {
  try {
    return Encoder.decode(obj);
  } catch (e) {
    console.error(e);
  }
  return undefined;
}
