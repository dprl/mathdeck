import unicodeMath from 'unicode-math';

export function charToUnicode(char) {
  return char.charCodeAt(0);
}

export function charFromUnicode(uc) {
  return String.fromCharCode(uc);
}

export function toHex(ucChar) {
  return ucChar.toString(16).toUpperCase();
}

export function fromHex(hexStr) {
  return parseInt(hexStr, 16);
}

export function charToUnicodeHex(char) {
  return toHex(charToUnicode(char));
}

export function charFromUnicodeHex(ucHex) {
  return charFromUnicode(fromHex(ucHex));
}

export function getUseElementUnicodeHex(el) {
  // MathJax use hrefs end in "-{unicode char in hex}"
  return el.getAttribute('href').split('-')[1];
}

export function getPathElementUnicodeHex(el) {
  return el.getAttribute('unicodehex');
}

export const multiPartSymbolStrings = [
  'sin',
  'cos',
  'tan',
  'log',
  'lim',
];

export const multiPartSymbolUnicodes = [
  ...multiPartSymbolStrings.map(
    str => str.split('').map(charToUnicodeHex),
  ),
  ['221A', '2212'], // sqrt + vbar
];

export const multiPartUnicodeSymbols = Object.assign(
  {},
  ...multiPartSymbolStrings.map(str => ({
    [str]: {
      latex: str,
      unicodeHex: str.split('').map(charToUnicodeHex),
    },
  })),
  {
    sqrt: {
      latex: '√',
      unicodeHex: ['221A', '2212'], // sqrt + vbar
    },
  },
);

// export function arrayToTrie(arr) {
//   if (!arr || arr.length === 0) {
//     return {
//       END: true,
//     };
//   }
//   const first = arr.shift();
//   return {
//     [first]: arrayToTrie(arr),
//   };
// }

// export function arrayInTrie(arr, trie) {
//   let context = trie;
//   for (let i = 0; i < arr.length; i += 1) {
//     const cur = arr[i];
//     context = context[cur];
//     if (context === undefined) {
//       return false;
//     }
//   }
//   return !!context.END;
// }

// export const multiPartSymbolUnicodes = _.merge(
//   {},
//   ...multiPartSymbolStrings.map(
//     str => arrayToTrie(
//       Array.prototype.map.call(str, charToUnicodeHex),
//     ),
//   ),
//   arrayToTrie(
//     ['221A', '2212'], // sqrt + vbar
//   ),
// );

// function testTrie() {
//   console.log(multiPartSymbolUnicodes);
//   console.log(arrayInTrie(
//     Array.prototype.map.call('sin', charToUnicodeHex),
//     multiPartSymbolUnicodes,
//   ));
// }
// testTrie();

export function latexToUnicodeHex(str) {
  // Don't map multi-part symbol strings
  if (multiPartSymbolStrings.map(s => `\\${s}`).includes(str)) {
    return str;
  }
  let entry = unicodeMath[str];
  // if no entry try latex command with 'up' prefix (\beta -> \upbeta)
  entry = !entry ? unicodeMath[str.replace(/\\/, '\\up')] : entry;
  const unicode = !entry ? charToUnicode(str) : entry.codePoint;
  return unicode ? toHex(unicode) : null;
}
