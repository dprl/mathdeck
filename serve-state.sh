#!/bin/bash
nohup python3 -u serve-state.py --sessions-limit=10000 > mathseer-state-service.log &
nohup python3 -u serve-state.py --port=8082 --data-dir="DEVDATA" > mathseer-state-service-dev.log &
