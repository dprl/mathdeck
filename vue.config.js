// vue.config.js - https://cli.vuejs.org/config/
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? './' : '/', // Build production version with relative paths
  productionSourceMap: false,
};
