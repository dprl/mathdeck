#!/usr/bin/env python3
import http.server
import socketserver
import os
import json
import time
from random import SystemRandom

class MyException(Exception):
    pass

class StateServer(http.server.SimpleHTTPRequestHandler):
    # default values
    states_dir = "states"
    # How many sessions to support / save
    max_sessions = 1000

    last_gc = time.time()
    rand = None
    index = "".join([
        "zyxwvutsrqponmlkjihgfedcba",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "-1234567890_"])

    def translate_path(self, path):
        # hack to prevent serving files in current directory
        return ''

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, content-type")
        # self.send_header('content-type', 'application/json')
        self.end_headers()


    def validate_session_id(self, session_id):
        if len(session_id) != 44:
            return False
        for c in session_id:
            if not c in self.index:
                return False
        return True


    def check_session_exists(self, session_id):
        return os.path.isfile(os.path.join(self.states_dir, session_id))


    def do_POST(self):
        print("DO POST",end='')
        response = {}
        try:
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            request_map = json.loads(post_data.decode('utf-8'))

            start_time = time.time()
            request_type = request_map["request"]
            request_session_id = ''


            if request_type == "GET_SESSION_ID":
                print("::GET_SESSION_ID",end='')
                # Generate a new session_id
                new_session_id = self.get_rand_path()
                # Create an empty file for the new session_id
                self.save_state("", new_session_id)
                # Send the session_id in the response
                response["sessionId"] = new_session_id
            else:
                request_session_id = request_map["sessionId"]

                if not self.validate_session_id(request_session_id) \
                or not self.check_session_exists(request_session_id):
                    raise MyException("Invalid sessionId provided")

                elif request_type == "GET_STATE":
                    print("::GET_STATE",end='')
                    # Respond with the state stored for the requested session
                    response["state"] = self.read_state(request_session_id)
                    response["sessionId"] = request_session_id

                elif request_type == "SET_STATE":
                    print("::SET_STATE",end='')
                    # Respond with the state stored for the requested session
                    request_session_id = request_map["sessionId"]
                    state_str = request_map["state"]
                    # Save the provided state value for the requested session
                    self.save_state(str(state_str), request_session_id)
                    response["message"] = "Successfully saved state"

                else:
                    raise MyException('Invalid request type')

            elapsed_time = time.time() - start_time
            print("::Time:"+str(elapsed_time))

        except MyException as e:
            print("Failed to process request.. ")
            print(e)
            response["error"] = True
            response["message"] = str(e)
        except Exception as e:
            print("Failed to process request.. ")
            print(e)
            response["error"] = True
            response["message"] = "Failed to process request"
        except:
            print("Failed to process request.. ")
            response["error"] = True
            response["message"] = "Failed to process request"

        finally:
            json_response = json.dumps(response, indent=4)
            self.send_response(http.client.OK, 'OK')
            self.send_header("Content-Type", "application/json")
            self.send_header("Content-length", len(json_response))
            self.send_header("Access-Control-Allow-Origin", "*")
            self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
            self.end_headers()
            response_bytes = bytes(json_response,"ascii")
            self.wfile.write(response_bytes)

            # 'Garbage collect' max once per min
            if time.time() - StateServer.last_gc > 60:
                try:
                    StateServer.last_gc = time.time()
                    # Cleanup old files if necessary
                    files = os.listdir(self.states_dir)
                    num_to_del = len(files) - self.max_sessions
                    if num_to_del > 0:
                        get_path = lambda f: os.path.join(self.states_dir, f)
                        old_files = sorted(map(get_path, files), key=os.path.getmtime)[0:num_to_del]
                        print(f"Deleting {len(old_files)} oldest files above limit of {self.max_sessions} files")

                        # Delete the files with the oldest modification time
                        for f in map(os.path.abspath, old_files):
                            os.remove(f)

                except Exception as e:
                    print("Auto deletion of old files failed")
                    print(e)
                except:
                    print("Auto deletion of old files failed")


    def get_rand_path(self, length = 44):
        i_size = len(self.index)
        return "".join([ self.index[self.rand.randrange(i_size)] for i in range(length) ])


    def save_state(self, state_str, path):
        with open(os.path.join(self.states_dir, path), 'w') as f:
            f.write(state_str)


    def read_state(self, path):
        with open(os.path.join(self.states_dir, path), 'r') as f:
            data = f.read()
        return data


def serve(host, port, data_dir, states_dir, max_sessions):
    working_dir = os.path.join(os.path.dirname(__file__), data_dir)

    StateServer.rand = SystemRandom()
    StateServer.states_dir = states_dir
    StateServer.max_sessions = max_sessions

    try:
        if not os.path.isdir(working_dir):
            os.mkdir(working_dir)
        os.chdir(working_dir)
        if not os.path.isdir(states_dir):
            os.mkdir(states_dir)

        server = socketserver.TCPServer((host, port), StateServer)
        print("Running states service at port:", port)
    except Exception as e:
        print(e)
        return

    server.serve_forever()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run the server for saving session state.")
    parser.add_argument("--host", default="localhost", help="hostname (default: localhost)")
    parser.add_argument("--port", default=8081, type=int, help="port (default: 8081)")
    parser.add_argument("--data-dir", default="APPDATA", help="data directory name (default: 'APPDATA')")
    parser.add_argument("--states-dir", default="states", help="states subdirectory name (default: 'states')")
    parser.add_argument("--sessions-limit", default=1000, type=int, help="limit to the number of sessions saved (default: 1000; oldest deleted first)")

    args = parser.parse_args()
    print(args)

    serve(args.host, args.port, args.data_dir, args.states_dir, args.sessions_limit)

