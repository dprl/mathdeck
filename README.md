# MathSeer Frontend

## Project setup

Install nodejs 10.x LTS

```sh
npm ci
# don't use npm install it breaks package-lock dependencies
```

### Compiles and hot-reloads for development

```sh
npm run serve
```

### Compiles and minifies for production

```sh
npm run build
```

### Run your tests

```sh
npm run test
```

### Lints and fixes files

```sh
npm run lint
```

### Run your end-to-end tests

```sh
npm run test:e2e
```

### Run your unit tests

```sh
npm run test:unit
```

## Technologies

### Vue

JavaScript Framework

[https://vuejs.org](https://vuejs.org)

### Vuetify

Material Design component framework

[https://vuetifyjs.com/](https://vuetifyjs.com/)

[https://vuetifyjs.com/en/components/api-explorer](https://vuetifyjs.com/en/components/api-explorer)

### Vuex

State management pattern + library

[https://vuex.vuejs.org](https://vuex.vuejs.org)

### Mocha

JavaScript test framework

[https://mochajs.org](https://mochajs.org)

### Chai

Behavior/Test driven development assertion library

[https://www.chaijs.com](https://www.chaijs.com)

### Eslint

JavaScript linting utility

[https://eslint.org](https://eslint.org)

### Nightwatch

Browser automated End-to-End testing framework

[http://nightwatchjs.org](http://nightwatchjs.org)

### Less

CSS language extension

[http://lesscss.org](http://lesscss.org)

### MathJax

JavaScript engine for mathematics

[https://www.mathjax.org](https://www.mathjax.org)

### MyScriptJS

Graphical JavaScript library to integrate handwriting recognition

[https://myscript.github.io/MyScriptJS/examples/](https://myscript.github.io/MyScriptJS/examples/)

### Selenium

Browser Test Automation

[https://www.seleniumhq.org](https://www.seleniumhq.org)

### Node.js

JavaScript runtime built on Chrome's V8 JavaScript engine

[https://nodejs.org/en](https://nodejs.org/en)

### PEP.js

Pointer Events Polyfill - cross platform pointer events

[https://github.com/jquery/PEP](https://github.com/jquery/PEP)
