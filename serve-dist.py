#!/usr/bin/env python3
import http.server
import socketserver
import base64
import os

class Server(http.server.SimpleHTTPRequestHandler):
    KEY= ""

    # def do_HEAD(self):
    #     self.send_response(200)
    #     self.send_header('Content-type', 'text/html')
    #     self.end_headers()

    def do_AUTH(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Interface Dev Access\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        auth = self.headers['Authorization']
        if not auth is None and auth == 'Basic ' + self.KEY:
            # check the request path
            path = self.translate_path(self.path)

            # default to index.html for non-existent paths
            if not os.path.exists(path):
                self.path = 'index.html'

            # handle the request as usual
            http.server.SimpleHTTPRequestHandler.do_GET(self)
        else:
            self.do_AUTH()
            self.wfile.write(b'Not authenticated')


def serve(host, port, username, password):
    working_dir = os.path.join(os.path.dirname(__file__), 'dist')
    os.chdir(working_dir)

    try:
        # Temporary authentication setup (not very secure!)
        # Username is everything before ':'
        # Password is everything after ':'
        Server.KEY = base64.b64encode(b''
            +bytes(username, 'ascii')+b':'
            +bytes(password, 'ascii')).decode('ascii')
        server = socketserver.TCPServer((host, port), Server)
        print("Serving dist dir:", working_dir, "at port:", port)
    except Exception as e:
        print(e)
        return

    server.serve_forever()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run the server for serving dev app.")
    parser.add_argument("--host", default="localhost", help="hostname (default: localhost)")
    parser.add_argument("--port", default=8078, type=int, help="port (default: 8078)")
    parser.add_argument("--username", default="devdeck", help="username")
    parser.add_argument("--password", default="pythagorean a^2+b^2=c^2", help="password")

    args = parser.parse_args()

    serve(args.host, args.port, args.username, args.password)
